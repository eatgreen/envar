#include "Ensemble.hpp"

namespace EnVar
{

    Ensemble::Ensemble(){}

    Ensemble::~Ensemble(){}

    Ensemble::Ensemble(Parameter& par_obj){
        nens_ = par_obj.nens();
        codENS_ = par_obj.codENS();
        Ensemble_type_ = par_obj.Ensemble_type();
        EnsType_suffix_ = par_obj.EnsType_suffix();
        EnsembleInflation_flag_ = par_obj.EnsembleInflation_flag();
        EnsInf_suffix_ = par_obj.EnsInf_suffix();
    }

    void Ensemble::EnsembleInitial(DataAssim& das_obj, Model& mod_obj, MpiDom& mpi_obj){
        MatrixXd  xens0;
        VectorXd  xinit = das_obj.sb(),dxens;
        MatrixXd  noise,noise_loc;
    
        if (mpi_obj.myrank()==mpi_obj.root()){
            cout << "Ensemble::make the noise" << endl;
            noise.resize(mpi_obj.nSize_glo(),nens_);
            //use pseudo2dM
            pseudo2dM(noise,mpi_obj.ny_glo(),mpi_obj.nx_glo(),nens_,codENS_,codENS_,1,1,mpi_obj.ny_glo(),mpi_obj.nx_glo(),0);

#ifdef DEBUG
            NetCDFIO noise_h_file("ensemble_noise", Parameter::Global, mpi_obj);
            noise_h_file.Create_File();
            std::string var_name;

            for (std::size_t i=0; i<(unsigned int)nens_; i++){
                Eigen::Map<MatrixXd> noise_h(noise.col(i).data(),mpi_obj.nx_glo(),mpi_obj.ny_glo());
                
                if (i==0)
                    noise_h_file.Add_Matrix_Dim(noise_h);
                var_name="h"+std::to_string(i);
                noise_h_file.Write_Matrix(noise_h,var_name);
            }
#endif
        }

        auto t_deb=Clock::now();

#if defined(MPI) || defined(MPI_simple)
#ifdef DEBUG
        NetCDFIO noise_h_loc_i_file("ensemble_noise_loc", Parameter::Local, mpi_obj);
        noise_h_loc_i_file.Create_File();
        std::string var_name_loc;
#endif
        MatrixXd    noise_h,noise_h_loc_i;
        VectorXd    noise_loc_i;

        //non-contiguous data send to each process
        //A derived type need to be defined, for each process, it needs to be filled with
        //the exact right domain size, then mpi_scatter can be used to distribute such type
        noise_loc.resize(mpi_obj.nx_loc()*mpi_obj.ny_loc(), nens_);
        noise_loc_i.resize(mpi_obj.nx_loc()*mpi_obj.ny_loc());
        noise_h_loc_i.resize(mpi_obj.nx_loc(), mpi_obj.ny_loc());

        int sendcounts[ mpi_obj.mysize() ];
        int senddispls[ mpi_obj.mysize() ];
        MPI_Datatype sendtypes[ mpi_obj.mysize() ];
        int recvcounts[ mpi_obj.mysize() ];
        int recvdispls[ mpi_obj.mysize() ];
        MPI_Datatype recvtypes[ mpi_obj.mysize() ];

        for (int proc=0; proc<mpi_obj.mysize(); proc++){
            recvcounts[proc] = 0;
            recvdispls[proc] = 0;
            recvtypes[proc]  = MPI_DOUBLE;

            sendcounts[proc] = 0;
            senddispls[proc] = 0;
            sendtypes[proc]  = MPI_DOUBLE;
        }

        recvcounts[mpi_obj.root()] = mpi_obj.nx_loc()*mpi_obj.ny_loc();
        recvdispls[mpi_obj.root()] = 0;

        MPI_Datatype local_field_type[mpi_obj.mysize()];

        int bigsizes[2] = {mpi_obj.nx_glo(), mpi_obj.ny_glo()};

        if (mpi_obj.myrank()==mpi_obj.root()){

            int subsizes[2];
            int starts[2]   = {0, 0};

            double displs_idx;

            for (int i=0; i<mpi_obj.proc_ni(); i++){
                for (int j=0; j<mpi_obj.proc_nj(); j++){
                    subsizes[0]=mpi_obj.nx_loc_vec().at( i*mpi_obj.proc_nj()+j );
                    subsizes[1]=mpi_obj.ny_loc_vec().at( i*mpi_obj.proc_nj()+j );
                    MPI_Type_create_subarray(2, bigsizes, subsizes, starts,
                                            MPI_ORDER_FORTRAN, MPI_DOUBLE, &local_field_type[ i*mpi_obj.proc_nj()+j ]);
                    MPI_Type_commit(&local_field_type[ i*mpi_obj.proc_nj()+j ]);

                    sendcounts[i*mpi_obj.proc_nj()+j]=1;

                    displs_idx=mpi_obj.iy_loc_vec().at(i*mpi_obj.proc_nj()+j)*mpi_obj.nx_glo()
                                + mpi_obj.ix_loc_vec().at(i*mpi_obj.proc_nj()+j); 

                    senddispls[i*mpi_obj.proc_nj()+j]=displs_idx*sizeof(MPI_DOUBLE);
                    
                    sendtypes[i*mpi_obj.proc_nj()+j]=local_field_type[i*mpi_obj.proc_nj()+j];
                }
            }
        }

        for (std::size_t i=0; i<(unsigned int)nens_; i++){
            if (mpi_obj.myrank()==mpi_obj.root())
                noise_h=Eigen::Map<MatrixXd>(noise.col(i).data(), mpi_obj.nx_glo(), mpi_obj.ny_glo());

            MPI_Alltoallw(noise_h.data(), sendcounts, senddispls, sendtypes, 
                          noise_h_loc_i.data(), recvcounts, recvdispls, recvtypes, 
                          MPI_COMM_WORLD);

#ifdef DEBUG
            if (i==0)
                noise_h_loc_i_file.Add_Matrix_Dim(noise_h_loc_i);
            
            var_name_loc="h"+std::to_string(i);
            noise_h_loc_i_file.Write_Matrix(noise_h_loc_i,var_name_loc);
#endif

            noise_loc_i=Eigen::Map<VectorXd>(noise_h_loc_i.data(), noise_h_loc_i.rows()*noise_h_loc_i.cols());
            noise_loc.col(i)=noise_loc_i;
        }
#else
        noise_loc=noise;
#endif

        auto t_com=Clock::now();

        if (mpi_obj.myrank()==mpi_obj.root())
            cout<<"time elapsed in ensembleinitial MPI_Alltoallw: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_com-t_deb).count()<<" ms"<<endl;

        if (mpi_obj.myrank()==mpi_obj.root())
            cout << "Ensemble::add noise to initial state" << endl;

        xens0.resize(mod_obj.Imp().nSize()*mod_obj.Imp().nState(),nens_);
        dxens.setZero(mod_obj.Imp().nSize()*mod_obj.Imp().nState());

        for(std::size_t i = 0; i<(unsigned int)nens_; i++){
            dxens.head(mod_obj.Imp().nSize()) = das_obj.Err_beta()*das_obj.Err_hb()*noise_loc.col(i);
            xens0.col(i) = xinit+dxens;
        }

        if (mpi_obj.myrank()==mpi_obj.root())
            cout << "Ensemble::make the ensemble initial state" << endl;

        if (das_obj.t_offset() == 0)
            xensf_=xens0;
        else
        {
            xensf_.resize(mod_obj.Imp().nSize()*mod_obj.Imp().nState(),nens_);
            for(std::size_t i = 0; i<(unsigned int)nens_; i++)
                xensf_.col(i) = runmodel_final(xens0.col(i),0,das_obj.t_offset(),mod_obj,mpi_obj,i,nens_); 
        }

        auto t_fin=Clock::now();

        if (mpi_obj.myrank()==mpi_obj.root())
            cout<<"my rank: "<<mpi_obj.myrank()<<" time elapsed in ensembleinitial runmodel_final: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_com).count()<<" ms"<<endl;

    }

    void Ensemble::EnsembleForecast(DataAssim& das_obj, Model& mod_obj, MpiDom& mpi_obj)
    {
        auto t_deb=Clock::now();

        if (mpi_obj.myrank()==mpi_obj.root())
            cout << "Ensemble::make the ensemble forecast state trajectory" << endl;

#ifdef _OMP
#pragma omp parallel 
{
#endif
        double td,tf;
        Data2D Xens_member;
        std::vector<VectorXd>  Xens_member_temp;

        // cout << "Thread rank " << omp_get_thread_num() << endl;

        // #pragma omp for ordered
        for(std::size_t i = 0; i<(unsigned int)nens_; i++)
        {
            Xens_member_temp.push_back(xensf_.col(i));
            for(std::size_t j = 0; j<das_obj.itobs_number()-1; j++)
            {
                td = das_obj.t_assim_deb().at(das_obj.CycleIndex())+j*das_obj.dtobs();
                tf = td+das_obj.dtobs();
                Xens_member_temp.push_back(runmodel_final(Xens_member_temp.at(j),td,tf,mod_obj,mpi_obj,i,nens_)); 
            }
            Xens_member.setData(Xens_member_temp);

            // #pragma omp ordered
            Xens_.setDataSlice(Xens_member);

            Xens_member_temp.clear();
        }

        auto t_fin=Clock::now();

        if (mpi_obj.myrank()==mpi_obj.root())
            cout<<"time elapsed in ensembleforecast runmodel_final: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;

#ifdef _OMP
}
#endif

        Data2D Xens_final_member;
        Xens_final_member = TimeDownSample2D(das_obj.Xf(),das_obj.t_ratio());
        Xens_.setDataSlice(Xens_final_member);
        
        Data2D      Xens_sum;

        if (mpi_obj.myrank()==mpi_obj.root())
            cout << "Ensemble::make the ensemble mean" << endl;

        Xens_sum.setZero(Xens_.size()[1],Xens_.size()[2]);

#ifdef ALGO
        std::accumulate(Xens_.begin(), Xens_.end()-1, Xens_sum); 
#else
        for(auto N_iter = Xens_.begin(); N_iter != Xens_.end()-1; ++N_iter)
            Xens_sum += *N_iter;
#endif
        Xens_mean_ = Xens_sum/nens_;
    }

    void Ensemble::EnsembleAnomaly(DataAssim& das_obj, Observation& obs_obj, MpiDom& mpi_obj){
        if (mpi_obj.myrank()==mpi_obj.root())
            cout << "Ensemble::make the ensemble anomaly matrix" << endl;
        
#ifdef ALGO
        std::for_each(Xens_.begin(), Xens_.end()-1, [&HMSb_, &Xens_mean_, &nens_](Data2D N_member){ return HMSb_.setDataSlice((N_member-Xens_mean_)/std::sqrt(nens_-1));});
#else
        for(auto N_iter = Xens_.begin(); N_iter!=Xens_.end()-1; ++N_iter)
            MSb_.setDataSlice((*N_iter-Xens_mean_)/std::sqrt(nens_-1));
#endif

        for(auto M_iter = MSb_.begin(); M_iter!=MSb_.end(); ++M_iter)
            HMSb_.setDataSlice( obs_obj.ObservationSpace_operator(*M_iter, das_obj.Assimilation_type()) );
    }

    void Ensemble::EnsembleInnovation(DataAssim& das_obj, const Data3D& XobsENS, MpiDom& mpi_obj){
        if (mpi_obj.myrank()==mpi_obj.root())
            cout << "Ensemble::make the ensemble innovation matrix" << endl;
        
        Data3D XobsENS_current_cycle;
        Data2D XobsENS_current_cycle_member;
        std::vector<VectorXd> XobsENS_current_cycle_member_temp(das_obj.itobs_number());

        for(auto n_iter = XobsENS.begin(); n_iter!=XobsENS.end(); ++n_iter)
        {
            std::copy(n_iter->begin()+das_obj.CycleIndex()*das_obj.itobs_number(), 
                      n_iter->begin()+(das_obj.CycleIndex()+1)*das_obj.itobs_number(), 
                      XobsENS_current_cycle_member_temp.begin());
            XobsENS_current_cycle_member.setData(XobsENS_current_cycle_member_temp);
            XobsENS_current_cycle.setDataSlice(XobsENS_current_cycle_member);
        }

#ifdef DEBUG
        if (mpi_obj.myrank()==mpi_obj.root()){
            cout<<"Ensemble::EnsembleInnovation Xens_ size info:"<<endl;
            Xens_.info();
        }
#endif
        Data2D Xens_member;
        std::vector<VectorXd>  Xens_member_temp;
        Data3D Xens_partial;
        for(auto e_iter=Xens_.begin(); e_iter!=Xens_.end(); ++e_iter){
            for(auto t_iter=e_iter->begin(); t_iter!=e_iter->end(); ++t_iter){

                switch(das_obj.Assimilation_type()){
                    case Parameter::All:
                        Xens_member_temp.push_back( *t_iter );
                        break;
                    case Parameter::Hei:
                        Xens_member_temp.push_back( t_iter->head( mpi_obj.nSize_loc()   ) );
                        break;
                    case Parameter::Vel:
                        Xens_member_temp.push_back( t_iter->tail( mpi_obj.nSize_loc()*2 ) );
                        break;
                }
            }

            Xens_member.setData(Xens_member_temp);
            Xens_member_temp.clear();
            Xens_partial.setDataSlice(Xens_member);
        }

#ifdef DEBUG
        if (mpi_obj.myrank()==mpi_obj.root()){
            cout<<"Ensemble::EnsembleInnovation Xens_partial size info:"<<endl;
            Xens_partial.info();
            cout<<"Ensemble::EnsembleInnovation XobsENS_current_cycle size info:"<<endl;
            XobsENS_current_cycle.info();
        }
#endif

        V_InnoENS_ = XobsENS_current_cycle - Xens_partial;

// #ifdef DEBUG
// #if defined(MPI) || defined(MPI_simple)
//             Parameter::IO_Domain_Type Domain_flag=Parameter::Local;
// #else
//             Parameter::IO_Domain_Type Domain_flag=Parameter::Global;
// #endif
//         NetCDFIO VInnoENS_file("VInnoENS", Domain_flag, mpi_obj);
//         VInnoENS_file.Create_File();
//         std::vector<std::string> var_name;
        
//         for (std::size_t i=0; i<=(unsigned int)nens_; i++){
//             if (i==0)
//                 VInnoENS_file.Add_StateTrajectory_Dim(V_InnoENS_.at(i));
//             var_name.assign({"h"+std::to_string(i),"u"+std::to_string(i),"v"+std::to_string(i)});
//             VInnoENS_file.Write_StateTrajectory(V_InnoENS_.at(i),var_name);
//         }
// #endif
    }

    Data3D Ensemble::EnsembleAnomalyLocal(const Data3D& Xensmn, const Data2D& Xensmn_mean, MpiDom& mpi_obj){
        Data3D      HMSbmn;

        if (mpi_obj.myrank()==mpi_obj.root())
            cout << "Ensemble::make the local ensemble anomaly matrix" << endl;

#ifdef ALGO
        std::for_each(Xensmn.begin(), Xensmn.end()-1, [&HMSbmn, &Xensmn_mean, &nens_](Data2D Nmn_member){ return HMSbmn.setDataSlice((Nmn_member-Xensmn_mean)/std::sqrt(nens_-1));});
#else
        for(auto N_iter = Xensmn.begin(); N_iter!=Xensmn.end()-1; ++N_iter)
            HMSbmn.setDataSlice((*N_iter-Xensmn_mean)/std::sqrt(nens_-1));
#endif
        return HMSbmn;
    }

    Data2D Ensemble::EnsembleInnovationLocal(DataAssim& das_obj, const Data2D& Xobsmn, const Data2D& Xfmn, MpiDom& mpi_obj){
        if (mpi_obj.myrank()==mpi_obj.root())
            cout << "Ensemble::make the local ensemble innovation matrix" << endl;
        
        Data2D V_Innomn;

        V_Innomn = Xobsmn - Xfmn;

        return V_Innomn;
    }

    void Ensemble::UpdateEnsembleInitial(const MatrixXd& delta_xENS)
    {
        xensa_ = xensf_+delta_xENS.leftCols(nens_);
    }

    void Ensemble::EnsembleIntegration(DataAssim& das_obj, Model& mod_obj, MpiDom& mpi_obj)
    {
        if (das_obj.CycleIndex()!=das_obj.Assimilation_cycle())
            cout << "Ensemble::make the ensemble initial state for next cycle" << endl;

        for(std::size_t i = 0; i<(unsigned int)nens_; i++)
            xensf_.col(i) = runmodel_final(xensa_.col(i),0,das_obj.tobs_assim(),mod_obj,mpi_obj,i,nens_);
    }

}