#ifndef NORMALRANDOM
#define NORMALRANDOM

#include <iostream>
#include <Eigen/Core>
#include "boost/random.hpp"
#include "boost/random/normal_distribution.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

namespace EnVar
{

	inline double StandardNormalRandomNumber(double dummy)
	{
	  static boost::mt19937 rng;
	  static boost::normal_distribution<> nd(0,1);
	  // static boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > gen(rng, nd);    // Variate generator
	  return nd(rng);
	}

//TODO, parallel random number generation
	inline MatrixXd NormalRandomMatrix(double rows, double cols, double mean, double std, int myrank)
	{
	  MatrixXd target;
	  target=MatrixXd::Zero(rows,cols).unaryExpr(std::ptr_fun(StandardNormalRandomNumber))*std+MatrixXd::Ones(rows,cols)*mean;
	  return target;
	}

	inline MatrixXd NormalRandomMatrix(double rows, double cols, double mean, MatrixXd& std, int myrank)
	{
	  MatrixXd target;
	  target=MatrixXd::Zero(rows,cols).unaryExpr(std::ptr_fun(StandardNormalRandomNumber))*std+MatrixXd::Ones(rows,cols)*mean;
	  return target;
	}

}
#endif