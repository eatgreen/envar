#ifndef PARAMETER
#define PARAMETER

#include <Eigen/Core>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <cstdlib>
#include "pseudo2dM.hpp"
#include "Data.hpp"
#include "../model/ModelPara.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

namespace EnVar
{
    class Parameter
    {
    public:
        enum Model_Type               {ShallowWater};

        enum Err_Type                 {GaussianErr,SlopeErr};
        enum Assimilation_Type        {All,Hei,Vel};
        enum Parameter_Type           {Tensor,Scalar};
        enum ParameterPert_Type       {TensorSD,ScalarSD};
        enum BackgroundState_Type     {EnsembleMean,TrueSolution,Observation,Theoritical};
        enum Algo_ControlVector_Type  {Ensemble,State};
        enum Algo_Precondition_Type   {SquareRootB,FullB,FullHessian};
        enum Algo_Localization_Type   {NoneLocal,Localize_Covariance,Local_Analysis};
        enum Algo_EnsembleUpdate_Type {Ensemble_Analysis,Ensemble_Transform};
        enum Ensemble_Type            {Gaussian,Timediff,Parameterian};
        enum Disp_Type                {Notify,Iter,NoneDisp};
        enum IO_Domain_Type           {Global,Local};

    private:
        std::unordered_map<std::string, std::string> para_map_;
        int                          Obs_type_;

        Model_Type                   Model_type_;
        ModelPara                    DA_modelpara_;
        ModelPara                    Synthobs_modelpara_;
        DimPara                      Realobs_dimpara_;

        DimPara::Dim_Type            Dim_type_;
        int                          nx_;
        int                          ny_;
        int                          nState_;

        bool                         obsreal_flag_;
        std::string                  obsreal_path_;  
        int                          nxrealobs_;
        int                          nyrealobs_;
        int                          nxsynthobs_;
        int                          nysynthobs_;
        double                       dtsynthobs_;

        double                       dtobs_;
        int                          t_ratio_;
        int                          t_ratiosynthobs_;
        Err_Type                     Err_type_;
        double                       Err_hb_;
        double                       Err_ub_;
        double                       Err_vb_;
        double                       Err_ho_;
        double                       Err_uo_;
        double                       Err_vo_;
        double                       Err_beta_;
        double                       Inc_beta_;
        int                          Assimilation_cycle_;
        int                          nobs_;
        double                       t_init_;
        double                       t_offset_;
        double                       tobs_assim_;
        double                       tmax_;
        std::vector<double>          t_assim_deb_;
        std::vector<double>          t_assim_fin_;
        double                       itobs_sum_;
        double                       itobs_number_;
        double                       ittru_sum_;
        std::vector<int>             itobs_deb_;
        std::vector<int>             itobs_fin_;
        std::vector<int>             ittru_;
        std::vector<int>             ittru_assim_deb_;
        std::vector<int>             ittru_assim_fin_;
        std::vector<int>             ittru_number_;
        int                          itmax_;
        int                          itinit_;
        Assimilation_Type            Assimilation_type_;
        int                          nobsState_;
        int                          VarRangeDeb_;
        int                          VarRangeFin_;
        int                          augState_;
        std::string                  Assimflag_suffix_;
        bool                         ParameterEstimation_flag_;
        Parameter_Type               Parameter_type_;
        ParameterPert_Type           ParameterPert_type_;
        BackgroundState_Type         BackgroundState_type_;
        Algo_ControlVector_Type      Algo_ControlVector_type_;
        Algo_Precondition_Type       Algo_Precondition_type_;
        Algo_Localization_Type       Algo_Localization_type_;
        Algo_EnsembleUpdate_Type     Algo_EnsembleUpdate_type_;
        double                       noise_const_;
        bool                         Write_Res_Each_Cycle_flag_;
        Algo_Localization_Type       Localization_type_;
        double                       cod_;  
        double                       cod_factor_;          
        int                          r_;               
        int                          LocalSizex_;     
        int                          LocalSizey_;      
        int                          nens_;
        double                       codENS_;
        double                       codENS_factor_;
        Ensemble_Type                Ensemble_type_;
        std::string                  EnsType_suffix_;
        bool                         EnsembleInflation_flag_; 
        std::string                  EnsInf_suffix_;
        int                          MaxIter_Outer_;
        double                       epsdxx_;
        int                          MaxIter_Inner_;
        double                       epsg_;
        double                       epsx_;
        Disp_Type                    Disp_type_;
        std::string                  parameter_exp_path_;

        int                          proc_ni_;
        int                          proc_nj_;

    public:
        /*** Constructors and destructor ***/

        ~Parameter(void){};

        /*** Methods ***/

        Parameter(int ifile, int proc_ni, int proc_nj):DA_modelpara_(1)
        {
            std::string   src_path(std::getenv("ENV_PATH"));

            //read paramter_config
            std::ifstream parafile_config(src_path+"DAParameter_config");
            std::string   line,name,value;

            if(parafile_config.is_open())
            {
                while(std::getline(parafile_config,line)){
                    std::string::size_type nc = line.find("//");
                    if(nc != std::string::npos)
                        line.erase(nc);

                    std::istringstream is(line);
                    if(is >> name >> value){
                        para_map_.insert({name, value});
                    }
                }
            }
            else
                std::cerr<<"Error opening file parameter_config"<<endl;

            switch(ifile)
            {
                default:
                {                
                    parameter_exp_path_=src_path+"DAParameter_exp";
                    break;
                }
            }
        
            //read parameter_exp, substitute corresponding parameters
            std::ifstream parafile_exp(parameter_exp_path_);

            if(parafile_exp.is_open())
            {
                while(std::getline(parafile_exp,line)){
                    std::string::size_type ne = line.find("//");
                    if(ne != std::string::npos)
                        line.erase(ne);

                    std::istringstream is(line);
                    if(is >> name >> value){
                        para_map_[name]=value;
                    }
                }
            }
            else
                std::cerr<<"Error opening file parameter_exp"<<endl;

            Obs_type_=std::stoi(para_map_["Obs_type_"]);
            Model_type_=static_cast<Model_Type>(std::stoi(para_map_["Model_type_"]));
            //model overwrite
            Dim_type_=static_cast<DimPara::Dim_Type>(std::stoi(para_map_["Dim_type_"]));
            nx_=std::stoi(para_map_["nx_"]);
            ny_=std::stoi(para_map_["ny_"]);
            DA_modelpara_.dimpara().setDim_type(Dim_type_);
            DA_modelpara_.dimpara().setnx(nx_);
            DA_modelpara_.dimpara().setny(ny_);
            nState_=DA_modelpara_.dimpara().nState();
            //mpi
            proc_ni_=proc_ni;
            proc_nj_=proc_nj;
            DA_modelpara_.dimpara().setproc_ni(proc_ni_);
            DA_modelpara_.dimpara().setproc_nj(proc_nj_);    
            //reset model dimension para
            DA_modelpara_.dimpara().setDimPara();
            //observation
            obsreal_flag_=stob(para_map_["obsreal_flag_"]);
            obsreal_path_=para_map_["obsreal_path_"];
            nxrealobs_=std::stod(para_map_["nxrealobs_"]);  
            nyrealobs_=std::stod(para_map_["nyrealobs_"]);   
            nxsynthobs_=std::stod(para_map_["nxsynthobs_"]);  
            nysynthobs_=std::stod(para_map_["nysynthobs_"]);    
            dtsynthobs_=std::stod(para_map_["dtsynthobs_"]); 
            //set ModelPara when synthetic obs
            if (obsreal_flag_){
                Realobs_dimpara_=DimPara( DA_modelpara_.dimpara() );//copy constructor
                Realobs_dimpara_.setnx(nxrealobs_);
                Realobs_dimpara_.setnx(nyrealobs_);
                Realobs_dimpara_.setDimPara();
            }
            else{
                Synthobs_modelpara_=ModelPara( DA_modelpara_ ); //copy constructor
                Synthobs_modelpara_.dimpara().setnx(nxsynthobs_);
                Synthobs_modelpara_.dimpara().setny(nysynthobs_);
                Synthobs_modelpara_.setdt(dtsynthobs_);
                //reset model dimension para
                Synthobs_modelpara_.dimpara().setDimPara();
                Synthobs_modelpara_.setSynthobs_ModelPara();
            }
            //assimilation
            Err_type_=static_cast<Err_Type>(std::stoi(para_map_["Err_type_"]));    
            Err_beta_=std::stod(para_map_["Err_beta_"]); 
            Err_hb_=std::stod(para_map_["Err_hb_"]); 
            Err_ub_=std::stod(para_map_["Err_ub_"]); 
            Err_vb_=std::stod(para_map_["Err_vb_"]); 
            Err_ho_=std::stod(para_map_["Err_ho_"]); 
            Err_uo_=std::stod(para_map_["Err_uo_"]); 
            Err_vo_=std::stod(para_map_["Err_vo_"]); 
            Inc_beta_=std::stod(para_map_["Inc_beta_"]);
            Assimilation_cycle_=std::stoi(para_map_["Assimilation_cycle_"]); 
            dtobs_=std::stod(para_map_["dtobs_"]);
            nobs_=std::stoi(para_map_["nobs_"]);
            t_init_=std::stod(para_map_["t_init_"]);
            t_offset_=std::stod(para_map_["t_offset_"]);   
            Assimilation_type_=static_cast<Assimilation_Type>(stoi(para_map_["Assimilation_type_"]));
            ParameterEstimation_flag_=stob(para_map_["ParameterEstimation_flag_"]);
            ParameterPert_type_=static_cast<ParameterPert_Type>(std::stoi(para_map_["ParameterPert_type_"]));
            Parameter_type_=static_cast<Parameter_Type>(std::stoi(para_map_["Parameter_type_"]));
            BackgroundState_type_=static_cast<BackgroundState_Type>(std::stoi(para_map_["BackgroundState_type_"]));
            Algo_ControlVector_type_=static_cast<Algo_ControlVector_Type>(std::stoi(para_map_["Algo_ControlVector_type_"]));
            Algo_Precondition_type_=static_cast<Algo_Precondition_Type>(std::stoi(para_map_["Algo_Precondition_type_"]));
            Algo_Localization_type_=static_cast<Algo_Localization_Type>(std::stoi(para_map_["Algo_Localization_type_"]));
            Algo_EnsembleUpdate_type_=static_cast<Algo_EnsembleUpdate_Type>(std::stoi(para_map_["Algo_EnsembleUpdate_type_"]));
            noise_const_=std::stod(para_map_["noise_const_"]);   
            Write_Res_Each_Cycle_flag_=stob(para_map_["Write_Res_Each_Cycle_flag_"]);
            //localization
            Localization_type_=Algo_Localization_type_;
            cod_factor_=std::stod(para_map_["cod_factor_"]);          
            r_=std::stoi(para_map_["r_"]);              
            LocalSizex_=std::stoi(para_map_["LocalSizex_"]);     
            LocalSizey_=std::stoi(para_map_["LocalSizey_"]);  
            //ensemble 
            nens_=std::stoi(para_map_["nens_"]);
            codENS_factor_=std::stod(para_map_["codENS_factor_"]);
            Ensemble_type_=static_cast<Ensemble_Type>(std::stoi(para_map_["Ensemble_type_"]));
            EnsType_suffix_=para_map_["EnsType_suffix_"];
            EnsembleInflation_flag_=stob(para_map_["EnsembleInflation_flag_"]);
            //optimization
            MaxIter_Outer_=std::stoi(para_map_["MaxIter_Outer_"]);
            epsdxx_=std::stod(para_map_["epsdxx_"]);
            MaxIter_Inner_=std::stoi(para_map_["MaxIter_Inner_"]);
            epsg_=std::stod(para_map_["epsg_"]);
            epsx_=std::stod(para_map_["epsx_"]);
            Disp_type_=static_cast<Disp_Type>(std::stoi(para_map_["Disp_type_"]));    

            //Derivative parameters
            t_ratio_=std::round(dtobs_/DA_modelpara_.dt());
            if(!obsreal_flag_)
                t_ratiosynthobs_=std::round(dtobs_/dtsynthobs_);

            tobs_assim_=dtobs_*nobs_;
            tmax_=t_offset_+tobs_assim_*Assimilation_cycle_;

            for(std::size_t i=0;i<(unsigned int)Assimilation_cycle_;i++)
            {   
                t_assim_deb_.push_back(t_init_+t_offset_+tobs_assim_*i);
                t_assim_fin_.push_back(t_assim_deb_.at(i)+tobs_assim_);
            }

            itobs_sum_=std::floor((tmax_-t_offset_)/dtobs_);

            ittru_sum_=std::floor((tmax_-t_offset_)/DA_modelpara_.dt());

            itobs_number_=nobs_;

            for(std::size_t i=0;i<(unsigned int)Assimilation_cycle_;i++)
            {   
                itobs_deb_.push_back(nobs_*i);
                itobs_fin_.push_back(itobs_deb_.at(i)+nobs_);
            }

            for(std::size_t i=0;i<(unsigned int)nobs_;i++)
                ittru_.push_back(t_ratio_*i);

            for(std::size_t i=0;i<(unsigned int)Assimilation_cycle_;i++)
            {   
                ittru_assim_deb_.push_back(round(t_assim_deb_.at(i)/DA_modelpara_.dt()));
                ittru_number_.push_back(round(tobs_assim_/DA_modelpara_.dt()));
                ittru_assim_fin_.push_back(ittru_assim_deb_.at(i)+ittru_number_.at(i));
            }

            itmax_=round(tmax_/DA_modelpara_.dt());
            itinit_=round(t_init_/DA_modelpara_.dt());;

            switch(Assimilation_type_)
            {
                case Parameter::All:
                {
                    nobsState_=3;
                    VarRangeDeb_=1;
                    VarRangeFin_=3;
                    Assimflag_suffix_="All";
                    break;
                }
                case Parameter::Hei:
                {
                    nobsState_=1;
                    VarRangeDeb_=1;
                    VarRangeFin_=1;
                    Assimflag_suffix_="Hei";
                    break;
                }
                case Parameter::Vel:
                {
                    nobsState_=2;
                    VarRangeDeb_=2;
                    VarRangeFin_=3;
                    Assimflag_suffix_="Vel";
                    break;
                }
            }

            if (ParameterPert_type_==Parameter::ScalarSD)
            {
                Parameter_type_=Parameter::Scalar;
                DA_modelpara_.setLocUncert_combo(5);
            }

            if (ParameterEstimation_flag_==false)
                augState_=nState_;
            else
                augState_=nState_+2;

            if (Algo_Localization_type_==Parameter::Local_Analysis)
                Algo_EnsembleUpdate_type_=Parameter::Ensemble_Transform;
            else
                Algo_EnsembleUpdate_type_=Parameter::Ensemble_Analysis;

            Localization_type_=Algo_Localization_type_;

            if (EnsembleInflation_flag_==false)
                EnsInf_suffix_="";
            else
                EnsInf_suffix_="_Inf";

            codENS_=std::floor(codENS_factor_*static_cast<double>(ny_));
            cod_=std::floor(cod_factor_*static_cast<double>(ny_));
        }

        void info(){
            if(Model_type_==Parameter::ShallowWater)
                cout<<"Dynamic model type     : Shallow water model"<<endl;
            else{
                std::cerr<<"undefined model type"<<endl;
                std::abort();
            }
            cout<<"Test        run on     : "<<proc_ni_<<" * "<<proc_nj_<<" processors"<<endl;
            cout<<"Forecast    run on     : "<<DA_modelpara_.dimpara().nx()<<" * "<<DA_modelpara_.dimpara().ny()<<endl;
            cout<<"with resolution        : "<<DA_modelpara_.dimpara().dx()<<"   "<<DA_modelpara_.dimpara().dy()<<endl;
            if(!obsreal_flag_)
                cout<<"Observation run on     : "<<nxsynthobs_<<" * "<<nysynthobs_<<endl;
            cout<<"Assimilate state(s)    : "<<Assimflag_suffix_<<endl;
            cout<<"Estimate parameters    : "<<ParameterEstimation_flag_<<endl;
            cout<<"Ensemble number        : "<<nens_<<endl;
            if(Algo_Localization_type_==Parameter::NoneLocal){
                cout<<"Localization scheme    : No Localization scheme"<<endl;
                cout<<"Control vector size    : "<<nens_<<endl;
            }
            else if(Algo_Localization_type_==Parameter::Localize_Covariance){
                cout<<"Localization scheme    : Localize_Covariance"<<endl;
                cout<<"Control vector size    : "<<nens_*r_<<endl;
            }
            else if(Algo_Localization_type_==Parameter::Local_Analysis){
                cout<<"Localization scheme    : Local_Analysis"<<endl;
                cout<<"Control vector size    : "<<nens_<<endl;
            }
            else
                std::cerr<<"No valid localization option is posed"<<endl;
            if(Algo_EnsembleUpdate_type_==Parameter::Ensemble_Analysis)
                cout<<"Ensemble update scheme : Ensemble_Analysis"<<endl;
            else if(Algo_EnsembleUpdate_type_==Parameter::Ensemble_Transform)
                cout<<"Ensemble update scheme : Ensemble_Transform"<<endl;
            else
                std::cerr<<"No valid ensemble update option is posed"<<endl;
            if (ParameterEstimation_flag_)
                cout<<"Parameter Pert_Type    : "<<ParameterPert_type_<<endl;
            cout<<"Assimilation_cycle     : "<<Assimilation_cycle_<<endl;
            cout<<"window starts at       : "<<"t_offset     = "<<t_offset_<<endl;
            cout<<"window length at       : "<<"nobs * dtobs = "<<nobs_<<" * "<<dtobs_<<endl;
        }
        const int&         Obs_type(void) const { return Obs_type_; }
        const Model_Type&  Model_type(void) const { return Model_type_; }

              ModelPara&   DA_modelpara(void)  { return DA_modelpara_; }
              ModelPara&   Synthobs_modelpara(void)  { return Synthobs_modelpara_; }
              DimPara&     Realobs_dimpara(void)  { return Realobs_dimpara_; }

        const DimPara::Dim_Type&    Dim_type(void) const { return Dim_type_; }
        const int&         nx(void) const { return nx_; }
        const int&         ny(void) const { return ny_; }
        const int&         nState(void) const{ return nState_;}

        const bool&        obsreal_flag(void) const { return obsreal_flag_; }
        const std::string& obsreal_path(void) const { return obsreal_path_; }
        const int&         nxrealobs(void) const { return nxrealobs_; }
        const int&         nyrealobs(void) const { return nyrealobs_; }

        const int&         nxsynthobs(void) const { return nxsynthobs_; }
        const int&         nysynthobs(void) const { return nysynthobs_; }
        const double&      dtsynthobs(void) const { return dtsynthobs_; }

        const double&      dtobs(void) const { return dtobs_; }
        const int&         t_ratio(void) const { return t_ratio_; }
        const int&         t_ratiosynthobs(void) const { return t_ratiosynthobs_; }

        const Err_Type& Err_type(void) const { return Err_type_;}
        const double&   Err_hb(void) const { return Err_hb_; }
        const double&   Err_ub(void) const { return Err_ub_; }
        const double&   Err_vb(void) const { return Err_vb_; }
        const double&   Err_ho(void) const { return Err_ho_; }
        const double&   Err_uo(void) const { return Err_uo_; }
        const double&   Err_vo(void) const { return Err_vo_; }
        const double&   Err_beta(void) const { return Err_beta_; }
        const double&   Inc_beta(void) const { return Inc_beta_; }
        const int&      Assimilation_cycle(void) const{ return Assimilation_cycle_;}
        const int&      nobs(void) const{ return nobs_;}
        const double&   t_init(void) const { return t_init_; }
        const double&   t_offset(void) const { return t_offset_; }
        const double&   tobs_assim(void) const { return tobs_assim_; }
        const double&   tmax(void) const { return tmax_; }
        const std::vector<double>&      t_assim_deb(void) const { return t_assim_deb_; }
        const std::vector<double>&      t_assim_fin(void) const { return t_assim_fin_; }
        const double&                   itobs_sum(void) const { return itobs_sum_; }
        const double&                   itobs_number(void) const { return itobs_number_; }
        const double&                   ittru_sum(void) const { return ittru_sum_; }
        const std::vector<int> &        itobs_deb(void) const { return itobs_deb_; }
        const std::vector<int> &        itobs_fin(void) const { return itobs_fin_; }
        const std::vector<int> &        ittru(void) const { return ittru_; }
        const std::vector<int> &        ittru_assim_deb(void) const { return ittru_assim_deb_; }
        const std::vector<int> &        ittru_assim_fin(void) const { return ittru_assim_fin_; }
        const std::vector<int> &        ittru_number(void) const { return ittru_number_; }
        const int&                      itmax(void) const { return itmax_; }
        const int&                      itinit(void) const { return itinit_; }
        const Assimilation_Type&        Assimilation_type(void) const { return Assimilation_type_; }
        const int&                      nobsState(void) const { return nobsState_; }
        const int&                      VarRangeDeb(void) const { return VarRangeDeb_; }
        const int&                      VarRangeFin(void) const { return VarRangeFin_; }
        const int&                      augState(void) const { return augState_;}
        const std::string&              Assimflag_suffix(void) const { return Assimflag_suffix_; }
        const bool&                     ParameterEstimation_flag(void) const { return ParameterEstimation_flag_; }
        const Parameter_Type&           Parameter_type(void) const { return Parameter_type_; }
        const ParameterPert_Type&       ParameterPert_type(void) const { return ParameterPert_type_; }
        const BackgroundState_Type&     BackgroundState_type(void) const { return BackgroundState_type_; }
        const Algo_ControlVector_Type&  Algo_ControlVector_type(void) const { return Algo_ControlVector_type_; }
        const Algo_Precondition_Type&   Algo_Precondition_type(void) const { return Algo_Precondition_type_; }
        const Algo_Localization_Type&   Algo_Localization_type(void) const { return Algo_Localization_type_; }
        const Algo_EnsembleUpdate_Type& Algo_EnsembleUpdate_type(void) const { return Algo_EnsembleUpdate_type_; }
        const bool&                     Write_Res_Each_Cycle_flag(void) const { return Write_Res_Each_Cycle_flag_; }
        const Algo_Localization_Type&   Localization_type(void) const { return Localization_type_; }
        const double&        cod(void) const { return cod_; }
        const int&           r(void) const { return r_; }
        const int&           LocalSizex(void) const { return LocalSizex_; }
        const int&           LocalSizey(void) const { return LocalSizey_; }
        const int&           nens(void) const { return nens_; }
        const double&        codENS(void) const { return codENS_; }
        const Ensemble_Type& Ensemble_type(void) const { return Ensemble_type_; }
        const std::string&   EnsType_suffix(void) const { return EnsType_suffix_; }
        const bool&          EnsembleInflation_flag(void) const { return EnsembleInflation_flag_; }
        const std::string&   EnsInf_suffix(void) const { return EnsInf_suffix_; }
        const int&           MaxIter_Outer(void) const { return MaxIter_Outer_; }
        const int&           MaxIter_Inner(void) const { return MaxIter_Inner_; }
        const double&        epsdxx(void) const { return epsdxx_; }
        const double&        epsg(void) const { return epsg_; }
        const double&        epsx(void) const { return epsx_; }
        const Disp_Type&     Disp_type(void) const { return Disp_type_; }

    private:

        bool stob(std::string str){
            std::istringstream is(str);
            bool b;
            is>>std::boolalpha>>b;
            return b;
        }

    };

}
#endif