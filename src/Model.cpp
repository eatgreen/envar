#include "Model.hpp"

namespace EnVar
{
	#ifdef TIME 
	        static std::chrono::duration<float> set_BC_time=std::chrono::duration<float>::zero();
	        static std::chrono::duration<float> RoeFlux_time=std::chrono::duration<float>::zero();
	#endif

	Model::Model(void){
	}

	Model::~Model(void){
	}

	Model::Model(ModelPara& modpar_obj, MpiDom& mpi_obj):swmodel_(modpar_obj,mpi_obj){
	}

	Data2D runmodel_frame(const VectorXd& x0, double t0, double tf, Model& mod_obj, MpiDom& mpi_obj)
	{
	    Data2D Xt;
	    Xt.setDataSlice(x0);

	    int    it=0;
	    double t=t0;
	    bool   print_time_final_flag;
	    while (t<tf)
	        {
	            print_time_final_flag=0;
	            if ( t==t0 || abs(t-tf)<=mod_obj.Imp().dt() ) 
	                print_time_final_flag = 1;
	            Xt.setDataSlice(mod_obj.Imp().TimeIntegration2D(Xt.data().at(it).data(), Xt.data().at(it).size(), print_time_final_flag, mpi_obj));
	            it++;
	            t=t+mod_obj.Imp().dt();
	        }
	    return Xt;
	}

	VectorXd runmodel_final(const VectorXd& x0, double t0, double tf, Model& mod_obj, MpiDom& mpi_obj, int ens_member, int nens)
	{
	    VectorXd Xt;
	    Xt=x0;

	    int    it=0;
	    double t=t0;
	    bool   print_time_final_flag;
	    while (t<tf)
	        {
	            print_time_final_flag=0;
	            if ( (t==t0 && ens_member==0) || (abs(t-tf)<=mod_obj.Imp().dt() && ens_member==nens-1) )
	                print_time_final_flag = 1;
	            Xt=mod_obj.Imp().TimeIntegration2D(Xt.data(), Xt.size(), print_time_final_flag, mpi_obj);
	            it++;
	            t=t+mod_obj.Imp().dt();
	        }
	    return Xt;
	}
}