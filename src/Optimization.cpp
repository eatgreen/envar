/********************************************************************

    Optimization.cpp

*********************************************************************/

/**
    \file Optimization.cpp
    \brief Optimization structure implementation
*/

#include"Optimization.hpp"

namespace EnVar{

    static int              apply_Rinv_times_=0;
    static int              apply_sqB_times_=0;
    static int              apply_sqBT_times_=0;

	Optimization::Optimization(){}

	Optimization::~Optimization(){}

	Optimization::Optimization(Parameter& par_obj)
	{
		if(par_obj.Algo_Localization_type()==Parameter::NoneLocal)
			nCtlVec_=par_obj.nens();
		else
			nCtlVec_=par_obj.nens()*par_obj.r();
		itobs_number_=par_obj.itobs_number();
		nens_=par_obj.nens();
		nobsState_=par_obj.nobsState();
		MaxIter_Outer_=par_obj.MaxIter_Outer();
		epsdxx_=par_obj.epsdxx();
		MaxIter_Inner_=par_obj.MaxIter_Inner();
		epsg_=par_obj.epsg();
		epsx_=par_obj.epsx();
		Disp_type_=par_obj.Disp_type();
	}

	void Optimization::OptInner(Ensemble& ens_obj, Observation& obs_obj, Localization& loc_obj, MpiDom& mpi_obj)
	{
        Data3D                  V_InnoENS;
        Data3D                  sqrtBens, sqrtBensState;
        VectorXd                Rinv;

		V_InnoENS=ens_obj.V_InnoENS();
		Rinv=obs_obj.Rinv();
		sqrtBens=ens_obj.HMSb();
		sqrtBensState=ens_obj.MSb();

#ifdef DEBUG
        if (mpi_obj.myrank()==mpi_obj.root()){
			cout<<"Optimization::OptPrepare: V_InnoENS_ size info:"<<endl;
			V_InnoENS.info();

			cout<<"Optimization::OptPrepare: sqrtBens_ size info:"<<endl;
			sqrtBens.info();
		}
#endif

		OptSingle(V_InnoENS, sqrtBens, Rinv, loc_obj, sqrtBensState, mpi_obj);

		// OptEnsemble(V_InnoENS, sqrtBens, Rinv, loc_obj, mpi_obj);
	}

	void Optimization::OptSingle(Data3D& V_InnoENS, Data3D& sqrtBens, VectorXd& Rinv, Localization& loc_obj, Data3D& sqrtBensState, MpiDom& mpi_obj)
	{

		cout<<"Run Opt Single"<<endl;

		Data2D   V_Inno;

		double 	 J0=0.0,J0_glo=0.0;
		std::vector<double> Jv;
		VectorXd b,b_glo;
		b.setZero(nCtlVec_);
		b_glo.setZero(nCtlVec_);

		delta_z_.setZero(nCtlVec_);

		V_Inno=V_InnoENS.back();

#ifdef DEBUG
        if (mpi_obj.myrank()==mpi_obj.root()){
			cout<<"V_Inno size info:"<<endl;
			V_Inno.info();
		}

#if defined(MPI) || defined(MPI_simple)
		Parameter::IO_Domain_Type Domain_flag=Parameter::Local;
#else
		Parameter::IO_Domain_Type Domain_flag=Parameter::Global;
#endif
		MatrixXd u,v;
		u=get_u(V_Inno.at(0),mpi_obj.nx_loc(),mpi_obj.ny_loc());
		v=get_v(V_Inno.at(0),mpi_obj.nx_loc(),mpi_obj.ny_loc());

    	NetCDFIO u_Inno("u_Inno", Domain_flag, mpi_obj);
    	u_Inno.Create_File();    
	    u_Inno.Add_Matrix_Dim(u);
        u_Inno.Write_Matrix(u,"u");

    	NetCDFIO v_Inno("v_Inno", Domain_flag, mpi_obj);
    	v_Inno.Create_File();    
	    v_Inno.Add_Matrix_Dim(v);
        v_Inno.Write_Matrix(v,"v");
#endif

		//Compute cost function
#ifdef ALGO
		std::transform((V_Inno.begin(), V_Inno.end(), Jv.begin(), [Rinv](VectorXd i_col){return 0.5*i_col.transpose()*Rinv.asDiagonal()*i_col;} );
		J0=std::accumulate(Jv.begin(), Jv.end(), 0);
#else

		int i=0;
		for(auto i_iter=V_Inno.begin(); i_iter!=V_Inno.end(); ++i_iter){
			J0+=0.5*(*i_iter).transpose()*Rinv.asDiagonal()*(*i_iter);
			i++;
			cout<<std::setprecision(8)<<"rank: "<<mpi_obj.myrank()<<" iter "<<i<<" J0: "<<0.5*(*i_iter).transpose()*Rinv.asDiagonal()*(*i_iter)<<endl;
		}
#endif

#if defined(MPI) || defined(MPI_simple)
		cout<<std::setprecision(8)<<"rank: "<<mpi_obj.myrank()<<" before MPI_Reduce J0: "<<J0<<endl;
		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Reduce(&J0, &J0_glo, 1, MPI_DOUBLE, MPI_SUM, mpi_obj.root(), MPI_COMM_WORLD);
#else
		J0_glo=J0;
#endif

        if (mpi_obj.myrank()==mpi_obj.root())
			cout<<std::setprecision(8)<<"rank: "<<mpi_obj.myrank()<<" after  MPI_Reduce J0: "<<J0_glo<<endl;

		cout<<"b size: "<<b.size()<<endl;

		//Compute gradient 
		if ( loc_obj.Localization_type() == Parameter::NoneLocal )
			b=apply_sqBTRinv(sqrtBens, V_Inno, Rinv, nCtlVec_, itobs_number_, mpi_obj);
		else if ( loc_obj.Localization_type() == Parameter::Localize_Covariance )
			b=apply_sqBTRinv(sqrtBens, V_Inno, Rinv, nens_, nCtlVec_, itobs_number_, loc_obj, mpi_obj);
		else
			std::cerr<<"unspecified localize type for sqBTRinv"<<endl;

#if defined(MPI) || defined(MPI_simple)
		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Reduce(b.data(), b_glo.data(), b.size(), MPI_DOUBLE, MPI_SUM, mpi_obj.root(), MPI_COMM_WORLD);
#else
		b_glo=b;
#endif

        if (mpi_obj.myrank()==mpi_obj.root())
			cout<<"b0 first 10 elements:\n"<<b_glo.head(nens_)<<endl;

		delta_z_=Inner_cg(J0_glo, b_glo, sqrtBens, Rinv, loc_obj, mpi_obj);

		if ( loc_obj.Localization_type() == Parameter::NoneLocal )
			delta_x_=apply_sqB0_state(delta_z_, sqrtBensState, mpi_obj);
		else if ( loc_obj.Localization_type() == Parameter::Localize_Covariance )
			delta_x_=apply_sqB0_state(delta_z_, sqrtBensState, nens_, loc_obj, mpi_obj);
		else
			std::cerr<<"unspecified localize type for sqB0"<<endl;
	}

	void Optimization::OptEnsemble(Data3D& V_InnoENS, Data3D& sqrtBens, VectorXd& Rinv, Localization& loc_obj, Data3D& sqrtBensState, MpiDom& mpi_obj)
	{
		cout<<"Run Opt Ensemble"<<endl;

		delta_zENS_.setZero(nCtlVec_,nens_);

// #pragma omp parallel
// {				
		Data2D V_Inno;

		double 	 J0,J0_glo;
		VectorXd b,b_glo;

		delta_zENS_.setZero(nCtlVec_,nens_);
		delta_xENS_.setZero(mpi_obj.nx_loc()*mpi_obj.ny_loc()*mpi_obj.nState(),nens_);

		for(std::size_t n=0; n<(unsigned int)nens_; n++)
		{

	        if (mpi_obj.myrank()==mpi_obj.root())
				cout<<"ensemble member "<<n<<endl;

			J0=0.0;
			J0_glo=0.0;
			b.setZero(nCtlVec_);
			b_glo.setZero(nCtlVec_);

			V_Inno=V_InnoENS.at(n);

			//Compute cost function
			for(auto i_iter=V_Inno.begin(); i_iter!=V_Inno.end(); ++i_iter)
				J0+=0.5*(*i_iter).transpose()*Rinv.asDiagonal()*(*i_iter);

#if defined(MPI) || defined(MPI_simple)
			cout<<"rank: "<<mpi_obj.myrank()<<" before MPI_Reduce J0: "<<J0<<endl;
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Reduce(&J0, &J0_glo, 1, MPI_DOUBLE, MPI_SUM, mpi_obj.root(), MPI_COMM_WORLD);
#else
			J0_glo=J0;
#endif

	       if (mpi_obj.myrank()==mpi_obj.root())
				cout<<"rank: "<<mpi_obj.myrank()<<" after  MPI_Reduce J0: "<<J0_glo<<endl;

			cout<<"b size: "<<b.size()<<endl;

			//Compute gradient
			if ( loc_obj.Localization_type() == Parameter::NoneLocal )
				b=apply_sqBTRinv(sqrtBens, V_Inno, Rinv, nCtlVec_, itobs_number_, mpi_obj);
			else if ( loc_obj.Localization_type() == Parameter::Localize_Covariance )
				b=apply_sqBTRinv(sqrtBens, V_Inno, Rinv, nens_, nCtlVec_, itobs_number_, loc_obj, mpi_obj);
			else
				std::cerr<<"unspecified localize type for sqBTRinv"<<endl;

#if defined(MPI) || defined(MPI_simple)
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Reduce(b.data(), b_glo.data(), b.size(), MPI_DOUBLE, MPI_SUM, mpi_obj.root(), MPI_COMM_WORLD);
#else
			b_glo=b;
#endif

        	if (mpi_obj.myrank()==mpi_obj.root())
				cout<<"b0 first 10 elements:\n"<<b_glo.head(nens_)<<endl;

			delta_zENS_.col(n)=Inner_cg(J0, b, sqrtBens, Rinv, loc_obj, mpi_obj);

			if ( loc_obj.Localization_type() == Parameter::NoneLocal )
				delta_xENS_.col(n)=apply_sqB0_state(delta_zENS_.col(n), sqrtBensState, mpi_obj);
			else if ( loc_obj.Localization_type() == Parameter::Localize_Covariance )
				delta_xENS_.col(n)=apply_sqB0_state(delta_zENS_.col(n), sqrtBensState, nens_, loc_obj, mpi_obj);
			else
				std::cerr<<"unspecified localize type for sqB0"<<endl;
		}
// }
	}

	void Optimization::OptLocalInner(Ensemble& ens_obj, Observation& obs_obj, Localization& loc_obj, DataAssim& das_obj, MpiDom& mpi_obj)
	{
		int lnx,lny,llocalsizex,llocalsizey;

		lnx=mpi_obj.nx_loc();
		lny=mpi_obj.ny_loc();
		llocalsizex=loc_obj.LocalSizex();
		llocalsizey=loc_obj.LocalSizey();

		VectorXd 	lRinv=obs_obj.Rinv();
		Data2D	 	lXobs=obs_obj.Xobs();	
		VectorXd 	lsf=das_obj.sf();
		Data2D   	lXf=das_obj.Xf();

		VectorXd 	sfmn,Rinvmn,samn;
		Data2D   	Xfmn,Xobsmn,Xensmn_mean,VInnomn,Samn;
		Data3D   	Xensmn,HMSbmn;
        MatrixXd    xensamn,OptRes,xensa;

        delta_x_.setZero(mpi_obj.nx_glo()*mpi_obj.ny_glo()*mpi_obj.nState());
		xensa.setZero(mpi_obj.nx_glo()*mpi_obj.ny_glo()*mpi_obj.nState(),nens_);

		for(std::size_t LocalIndex=0; LocalIndex<(unsigned int)lnx*lny; LocalIndex++)
		{
			int absiIndex=LocalIndex%lnx;
			if(absiIndex==0)
				absiIndex=lnx-1;

			int absjIndex=(LocalIndex-absiIndex)/lnx;

            int reliIndex=std::min(llocalsizex+1,absiIndex);
            int reljIndex=std::min(llocalsizey+1,absjIndex);

            int lx,ly;

            if(absiIndex<llocalsizex)
            	lx=std::min(absiIndex+llocalsizex,lnx);
            else if( (absiIndex>lnx-llocalsizex) && (absiIndex>=llocalsizex) )
            	lx=lnx-absiIndex+llocalsizex;
        	else
        		lx=2*llocalsizex;

            if(absjIndex<llocalsizey)
            	ly=std::min(absjIndex+llocalsizey,lnx);
            else if( (absjIndex>lnx-llocalsizey) && (absjIndex>=llocalsizey) )
            	ly=lnx-absjIndex+llocalsizey;
        	else
        		ly=2*llocalsizey;

            //local analysis transformation
            sfmn   =loc_obj.LocalAnalysisOperator(lsf,absiIndex,absjIndex,lx,ly,mpi_obj);
            Xfmn   =loc_obj.LocalAnalysisOperator(lXf,absiIndex,absjIndex,lx,ly,mpi_obj);
            Xensmn =loc_obj.LocalAnalysisOperator(ens_obj.Xens(),absiIndex,absjIndex,lx,ly,mpi_obj);
            Xensmn_mean =loc_obj.LocalAnalysisOperator(ens_obj.Xens_mean(),absiIndex,absjIndex,lx,ly,mpi_obj);

            HMSbmn =ens_obj.EnsembleAnomalyLocal(Xensmn,Xensmn_mean,mpi_obj);

            Xobsmn =loc_obj.LocalAnalysisOperator(lXobs,absiIndex,absjIndex,lx,ly,mpi_obj);
            Rinvmn =loc_obj.LocalAnalysisOperator(lRinv,absiIndex,absjIndex,lx,ly,mpi_obj);

            VInnomn=ens_obj.EnsembleInnovationLocal(das_obj,Xobsmn,Xfmn,mpi_obj);
            
            OptRes.setZero(nCtlVec_,nCtlVec_+1);
            OptRes=OptLocalSingle(VInnomn,HMSbmn,Rinvmn,loc_obj,mpi_obj);
            samn = sfmn + OptRes.col(0);

	        // [W,S,V]=svd(Hessian);
	        // TMmn=W*diag(diag(S).^(-0.5))*V;   
	        // SaRmn=HMSbRmn(:,:,1)*TMmn*orth(randn(ens_obj.nens));

			Samn=HMSbmn.front();

	        xensamn.resize(lx*ly,ens_obj.nens());

	        for(std::size_t k=0; k<(unsigned int)ens_obj.nens(); k++)
	        {
	        	xensamn.col(k)=samn + std::sqrt(ens_obj.nens()-1)*Samn.at(k);
	        }

	        delta_x_(LocalIndex)=OptRes(reliIndex*reljIndex,0);

			xensa(LocalIndex)=xensamn(reliIndex*reljIndex);
        }
	}

	MatrixXd Optimization::OptLocalSingle(Data2D& VInnomn, Data3D& HMSbmn, VectorXd& Rinvmn, Localization& loc_obj, MpiDom& mpi_obj)
	{
		double 	 J0=0.0;
		VectorXd b, delta_xmn;
		MatrixXd res;

		b.setZero(nCtlVec_);

		delta_z_.setZero(nCtlVec_);

        if (mpi_obj.myrank()==mpi_obj.root()){
			cout<<"V_Innomn size info:"<<endl;
			VInnomn.info();
		}	

		for(auto i_iter=VInnomn.begin(); i_iter!=VInnomn.end(); ++i_iter)
			J0+=0.5*(*i_iter).transpose()*Rinvmn.asDiagonal()*(*i_iter);

        if (mpi_obj.myrank()==mpi_obj.root())
			cout<<"J0:\n"<<J0<<endl;

		int k=0;
		for(auto k_iter=HMSbmn.begin(); k_iter!=HMSbmn.end(); ++k_iter){
			for(std::size_t i=0; i<k_iter->size()[0]; i++)
				b(k)=b(k)-k_iter->at(i).transpose()*Rinvmn.asDiagonal()*VInnomn.at(i);
			k++;
		}

        if (mpi_obj.myrank()==mpi_obj.root())
			cout<<"b0:\n"<<b<<endl;

		delta_z_=Inner_cg(J0, b, HMSbmn, Rinvmn, loc_obj, mpi_obj);

		delta_xmn.setZero(nCtlVec_);

		for(std::size_t m=0; m<(unsigned int)nCtlVec_; m++)
			delta_xmn+=HMSbmn.at(m,0)*delta_z_(m);

		res.setZero(nCtlVec_,nCtlVec_+1);

		res.col(0)=delta_xmn;

		return res;
	}

	VectorXd Optimization::Inner_cg(double J0, VectorXd& b, Data3D& sqrtBens, VectorXd& Rinv_t, Localization& loc_obj, MpiDom& mpi_obj)
	{
		/*
		J=1/2 dx^T B^-1 dx + 1/2 (HM dx - V_Inno)^T R^-1 (HM dx - V_Inno)
		using conjugate gradient with or without B^1/2 preconditioning
		=> J'=1/2 dz^T dz + 1/2 (HMAb dz - V_Inno)^T R^-1 (HMAb dz - V_Inno)
		Minimizing the above function corresponds to solving linear equations
		=> 					   	A'dz = b
		=> (I + HMAb^T R^-1 HMAb) dz = HMAb^T R^-1 V_Inno
		size note: HMAb n*N, V_Inno n*T	 
		*/

        auto t_deb=Clock::now();
		
#ifdef TIME	
		static std::chrono::duration<float> sgl_chip_1_time=std::chrono::duration<float>::zero();
		static std::chrono::duration<float> sgl_chip_2_time=std::chrono::duration<float>::zero();
		static std::chrono::duration<float> MPI_Bast_time=std::chrono::duration<float>::zero();
		static std::chrono::duration<float> Apply_Hessian_time=std::chrono::duration<float>::zero();
		static std::chrono::duration<float> MPI_Reduce_time=std::chrono::duration<float>::zero();
#endif

		double   J,alpha,beta;
		VectorXd r0;
		int	 break_flag=0;

		VectorXd r_k,q_k;
		double   rho=0.0,rho_old=0.0,r_knorm,r0norm=0.0;

		VectorXd p_k,q_k_loc;
		VectorXd x;

		if (mpi_obj.myrank()==mpi_obj.root()){
			r0=-b;
			r0norm=std::sqrt(r0.dot(r0));
		}

// #if defined(MPI) || defined(MPI_simple)
// 		MPI_Bcast(&r0norm, 1, MPI_DOUBLE, mpi_obj.root(), MPI_COMM_WORLD);
// #endif

		p_k.setZero(nCtlVec_);
		q_k.setZero(nCtlVec_);
		x.setZero(nCtlVec_);

		for(std::size_t k=0; k<(unsigned int)MaxIter_Inner_; k++)
		{
#if defined(MPI) || defined(MPI_simple)
			if (mpi_obj.myrank()==mpi_obj.root()){
#endif
#ifdef TIME	
		        auto t_1=Clock::now();
#endif
				if(k==0)
				{
					r_k= r0;
					p_k=-r0;
					rho=r_k.dot(r_k);
				}
				else
					p_k=-r_k+beta*p_k;

				rho_old=rho;
#ifdef TIME	
		        auto t_2=Clock::now();
		        sgl_chip_1_time+=t_2-t_1;
		        if ( k==(unsigned int)(MaxIter_Inner_-1) )
            		cout<<"time elapsed in Optimization Inner_cg single chip 1: "<<std::chrono::duration_cast<std::chrono::microseconds>(sgl_chip_1_time).count()<<" mius"<<endl;
#endif
#if defined(MPI) || defined(MPI_simple)
			}//end root
#ifdef TIME	
	        auto t_3=Clock::now();
#endif
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Bcast(p_k.data(), nCtlVec_, MPI_DOUBLE, mpi_obj.root(), MPI_COMM_WORLD);
#ifdef TIME	
	        auto t_4=Clock::now();
			if (mpi_obj.myrank()==mpi_obj.root()){
				MPI_Bast_time+=t_4-t_3;
		        if ( k==(unsigned int)(MaxIter_Inner_-1) )
        			cout<<"time elapsed in Optimization Inner_cg MPI_Bcast loop: "<<std::chrono::duration_cast<std::chrono::microseconds>(MPI_Bast_time).count()<<" mius"<<endl;
        	}
#endif
#endif
#ifdef TIME	
	        auto t_5=Clock::now();
#endif
			if(loc_obj.Localization_type()==Parameter::NoneLocal || loc_obj.Localization_type()==Parameter::Local_Analysis)
				q_k_loc=apply_sqBTRinvsqB(p_k, sqrtBens, Rinv_t, nCtlVec_, itobs_number_, nobsState_, mpi_obj);
			else if (loc_obj.Localization_type()==Parameter::Localize_Covariance)
				q_k_loc=apply_sqBTRinvsqB(p_k, sqrtBens, Rinv_t, nens_, nCtlVec_, itobs_number_, nobsState_, loc_obj, mpi_obj);
			else
				std::cerr<<"No viable localization scheme given!"<<endl;
#ifdef TIME	
	        auto t_6=Clock::now();
        	if (mpi_obj.myrank()==mpi_obj.root()){
        		Apply_Hessian_time+=t_6-t_5;
		        if ( k==(unsigned int)(MaxIter_Inner_-1) )
        			cout<<"my rank: "<<mpi_obj.myrank()<<" time elapsed in Optimization Inner_cg Apply Hessian: "<<std::chrono::duration_cast<std::chrono::milliseconds>(Apply_Hessian_time).count()<<" ms"<<endl;
        	}

	        auto t_7=Clock::now();
#endif

#if defined(MPI) || defined(MPI_simple)
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Reduce(q_k_loc.data(), q_k.data(), nCtlVec_, MPI_DOUBLE, MPI_SUM, mpi_obj.root(), MPI_COMM_WORLD);
#else
			q_k=q_k_loc;
#endif

#ifdef TIME	
	        auto t_8=Clock::now();
        	if (mpi_obj.myrank()==mpi_obj.root()){
        		MPI_Reduce_time+=t_8-t_7;
		        if ( k==(unsigned int)(MaxIter_Inner_-1) )
        			cout<<"time elapsed in Optimization Inner_cg MPI_Reduce: "<<std::chrono::duration_cast<std::chrono::milliseconds>(MPI_Reduce_time).count()<<" ms"<<endl;
        	}
#endif

#if defined(MPI) || defined(MPI_simple)
			if (mpi_obj.myrank()==mpi_obj.root()){
#endif
#ifdef TIME	
	        	auto t_9=Clock::now();
#endif
				q_k += p_k;

				alpha = rho/(p_k.dot(q_k));

				r_k = r_k+alpha*q_k;

				rho = r_k.dot(r_k);

				r_knorm = std::sqrt(rho);

				x = x+alpha*p_k;

				beta = rho/rho_old;

				J = J0+0.5*r0.dot(x);

				if(k%10==0) 
					cout<<"Iter"<<std::setw(8)<<"J"<<std::setw(15)<<"rho_old"<<std::setw(15)<<"alpha"<<std::setw(15)<<"rho"<<std::setw(15)<<"r_knorm"<<std::setw(15)<<"beta"<<endl;
			
				cout<<std::setw(2)<<k<<std::setw(10)<<J<<std::setw(15)<<rho_old<<std::setw(15)<<alpha<<std::setw(15)<<rho<<std::setw(15)<<r_knorm<<std::setw(15)<<beta<<endl;

#ifdef TIME	
	        	auto t_10=Clock::now();
    			sgl_chip_2_time+=t_10-t_9;
		        if ( k==(unsigned int)(MaxIter_Inner_-1) )
    				cout<<"time elapsed in Optimization Inner_cg single chip 2: "<<std::chrono::duration_cast<std::chrono::microseconds>(sgl_chip_2_time).count()<<" mius"<<endl;
#endif   

				if(r_knorm<epsg_*r0norm)
				{
					cout<<"converge attained"<<endl;
					break_flag=1;
				}
#if defined(MPI) || defined(MPI_simple)
			}//end root
			// MPI_Bcast(&r_knorm, 1, MPI_DOUBLE, mpi_obj.root(), MPI_COMM_WORLD);
			MPI_Bcast(&break_flag, 1, MPI_INT, mpi_obj.root(), MPI_COMM_WORLD);
#endif        		
			if(break_flag){
				break;
			}
		}//end Iter_Inner loop

#ifdef TIME	
    	auto t_11=Clock::now();
#endif

#if defined(MPI) || defined(MPI_simple)
		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Bcast(x.data(), nCtlVec_, MPI_DOUBLE, mpi_obj.root(), MPI_COMM_WORLD);
#endif

#ifdef TIME	
    	auto t_12=Clock::now();
		if (mpi_obj.myrank()==mpi_obj.root())
			cout<<"time elapsed in Optimization Inner_cg Final MPI_Bcast call: "<<std::chrono::duration_cast<std::chrono::microseconds>(t_12-t_11).count()<<" mius"<<endl;
#endif

	    auto t_fin=Clock::now();
        if (mpi_obj.myrank()==mpi_obj.root())
            cout<<"time elapsed in optimization Inner_cg: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;

		return x;
	}

//! A member to apply the inverse of R

	Data2D apply_Rinv(const Data2D& Gamma, const VectorXd& Rinv_t, const int& itobs_number)
	{
#ifdef DEBUG
		cout<<"apply Rinv times "<<++apply_Rinv_times_<<endl;
#endif
		Data2D GammaRinv;

		for(std::size_t t=0; t<(unsigned int)itobs_number; t++){
			GammaRinv.setDataSlice( Rinv_t.array()*Gamma.at(t).array() );
		}

		return GammaRinv;
	}

//! A member to apply the sqrt of UNLOCALIZED ensemble B and its evolution in time

	VectorXd apply_sqBTRinvsqB(const VectorXd& p, const Data3D& sqrtBens, const VectorXd& Rinv_t, const int& nCtlVec, const int& itobs_number, const int& nobsState, MpiDom& mpi_obj)
	{
		Data2D Gamma,GammaRinv;
		Gamma=apply_sqB(p, sqrtBens, itobs_number, nobsState, mpi_obj);

		GammaRinv=apply_Rinv(Gamma, Rinv_t, itobs_number);

		VectorXd Ap;
		Ap=apply_sqBT(GammaRinv, sqrtBens, nCtlVec, itobs_number, mpi_obj);

		return Ap;
	}

	VectorXd apply_sqBTRinv(const Data3D& sqrtBens, const Data2D& Gamma, const VectorXd& Rinv_t, const int& nCtlVec, const int& itobs_number, MpiDom& mpi_obj)
	{
#ifdef DEBUG
		cout<<"apply sqBTRinv"<<endl;
#endif
		Data2D GammaRinv;
		GammaRinv=apply_Rinv(Gamma, Rinv_t, itobs_number);

		VectorXd Ap;
		Ap=apply_sqBT(GammaRinv, sqrtBens, nCtlVec, itobs_number, mpi_obj);

		return Ap;
	}

	VectorXd apply_sqB0_state(const VectorXd& p, const Data3D& sqrtBens, MpiDom& mpi_obj)
	{
		VectorXd x_nstate;
		x_nstate.setZero(mpi_obj.nSize_loc()*mpi_obj.nState());

		int i=0;
		for(auto i_iter=sqrtBens.begin(); i_iter!=sqrtBens.end(); ++i_iter)
		{
			x_nstate+=i_iter->at(0)*p(i);
			i++;
		}
		return x_nstate;
	}

	Data2D apply_sqB(const VectorXd& p, const Data3D& sqrtBens, const int& itobs_number, const int& nobsState, MpiDom& mpi_obj)
	{
#ifdef DEBUG
		cout<<"apply sqB  times "<<++apply_sqB_times_<<endl;
#endif
		Data2D Gamma;
		Gamma.setZero(itobs_number, mpi_obj.nSize_loc()*nobsState);

		int i=0;
		for(auto i_iter=sqrtBens.begin(); i_iter!=sqrtBens.end(); ++i_iter)
		{
			Gamma+=*i_iter*p(i);
			i++;
		}
		return Gamma;
	}

	VectorXd apply_sqBT(const Data2D& Gamma, const Data3D& sqrtBens, const int& nCtlVec, const int& itobs_number, MpiDom& mpi_obj)
	{
#ifdef DEBUG
		cout<<"apply sqBT times "<<++apply_sqBT_times_<<endl;
#endif
		VectorXd Ap;
		Ap.setZero(nCtlVec);	

		int k=0;
		for(auto k_iter=sqrtBens.begin(); k_iter!=sqrtBens.end(); ++k_iter)
		{
			for(std::size_t t=0; t<(unsigned int)itobs_number; t++)
				Ap(k)=Ap(k)+k_iter->at(t).transpose()*Gamma.at(t);
			k++;
		}
		return Ap;
	}

//! A member to apply the sqrt of LOCALIZED ensemble B and its evolution in time

	VectorXd apply_sqBTRinvsqB(const VectorXd& p, const Data3D& sqrtBens, const VectorXd& Rinv_t, const int& nens, const int& nCtlVec, const int& itobs_number, const int& nobsState, Localization& loc_obj, MpiDom& mpi_obj)
	{
		Data2D Gamma,GammaRinv;
		Gamma=apply_sqB(p, sqrtBens, nens, itobs_number, nobsState, loc_obj, mpi_obj);

		GammaRinv=apply_Rinv(Gamma, Rinv_t, itobs_number);

		VectorXd Ap;
		Ap=apply_sqBT(GammaRinv, sqrtBens, nens, nCtlVec, itobs_number, loc_obj, mpi_obj);

		return Ap;
	}

	VectorXd apply_sqBTRinv(const Data3D& sqrtBens, const Data2D& Gamma, const VectorXd& Rinv_t, const int& nens, const int& nCtlVec, const int& itobs_number, Localization& loc_obj, MpiDom& mpi_obj)
	{
		Data2D GammaRinv;
		GammaRinv=apply_Rinv(Gamma, Rinv_t, itobs_number);

		VectorXd Ap;
		Ap=apply_sqBT(GammaRinv, sqrtBens, nens, nCtlVec, itobs_number, loc_obj, mpi_obj);

		return Ap;
	}

	VectorXd apply_sqB0_state(const VectorXd& p, const Data3D& sqrtBens, const int& nens, Localization& loc_obj, MpiDom& mpi_obj)
	{
		VectorXd x_nstate;
		x_nstate.setZero(mpi_obj.nSize_loc()*mpi_obj.nState());

		VectorXd XbC;
		XbC.setZero(mpi_obj.nSize_loc()*mpi_obj.nState());

		int i=0;
		for(auto i_iter=sqrtBens.begin(); i_iter!=sqrtBens.end(); ++i_iter)
		{
			for(std::size_t j=0; j<(unsigned int)loc_obj.r(); j++)
			{				
				XbC=i_iter->at(0).array()*loc_obj.sqrt_Cor_mat().col(j).array();

				x_nstate += XbC * p( j*nens + i );
			}
			i++;
		}

		return x_nstate;
	}

	Data2D apply_sqB(const VectorXd& p, const Data3D& sqrtBens, const int& nens, const int& itobs_number, const int& nobsState, Localization& loc_obj, MpiDom& mpi_obj)
	{
#ifdef DEBUG
		cout<<"apply sqB  times "<<++apply_sqB_times_<<endl;
#endif
		Data2D Gamma;

		MatrixXd temp;
		temp.setZero(mpi_obj.nSize_loc()*nobsState, itobs_number);

		int i=0;
		for(auto i_iter=sqrtBens.begin(); i_iter!=sqrtBens.end(); ++i_iter)
		{
			for(std::size_t t=0; t<(unsigned int)itobs_number; t++)
				for(std::size_t j=0; j<(unsigned int)loc_obj.r(); j++)
					temp.col(t) += i_iter->at(t).cwiseProduct( loc_obj.sqrt_Cor_mat().col(j) ) * p( j*nens+i );
			i++;
		}

		Gamma.setDataMatrixXd( temp );

		return Gamma;
	}

	VectorXd apply_sqBT(const Data2D& Gamma, const Data3D& sqrtBens, const int& nens, const int& nCtlVec, const int& itobs_number, Localization& loc_obj, MpiDom& mpi_obj)
	{
#ifdef DEBUG
		cout<<"apply sqBT times "<<++apply_sqBT_times_<<endl;
#endif
		VectorXd Ap;
		Ap.setZero(nCtlVec);

		MatrixXd Q;
		Q.resize(nCtlVec, itobs_number);

		int i=0;
		for(auto i_iter=sqrtBens.begin(); i_iter!=sqrtBens.end(); ++i_iter)
		{
			for(std::size_t t=0; t<(unsigned int)itobs_number; t++)
				for(std::size_t j=0; j<(unsigned int)loc_obj.r(); j++)
					Q( j*nens+i, t ) = i_iter->at(t).cwiseProduct( loc_obj.sqrt_Cor_mat().col(j) ).dot( Gamma.at(t) );
			i++;
		}

		for(std::size_t k=0; k<(unsigned int)itobs_number; k++)
			Ap=Ap+Q.col(k);

		return Ap;
	}
	
	// int Optimization::OptInner_lbfgs()
	// {
	// 	/*
	// 	J=1/2 dx^T B^-1 dx + 1/2 (HM dx - V_Inno)^T R^-1 (HM dx - V_Inno)
	// 	using conjugate gradient with or without B^1/2 preconditioning
	// 	=> J'=1/2 dz^T dz + 1/2 (HMAb dz - V_Inno)^T R^-1 (HMAb dz - V_Inno)
	// 	Minimizing the above function corresponds to solving linear equations
	// 	*/
 //        lbfgsfloatval_t fx;
 //        lbfgsfloatval_t *m_x = lbfgs_malloc(nCtlVec_);
 //    	lbfgs_parameter_t param;

 //    	for(std::size_t i=0;i<(unsigned int)nCtlVec_;i++)
 //    	{
 //    		*(m_x+i)=0;
 //    	}

 //        lbfgs_parameter_init(&param);

 //        int ret = lbfgs(nCtlVec_, m_x, &fx, evaluate, progress, NULL, &param);

 //        cout<<"L-BFGS optimization terminated with status code = "<<ret<<endl;
        
 //        lbfgs_free(m_x);

 //        return ret;
	// }

    // lbfgsfloatval_t Optimization::evaluate(
	   //  void *instance,
    //     const lbfgsfloatval_t *x,
    //     lbfgsfloatval_t *g,
    //     const int n,
    //     const lbfgsfloatval_t step
    //     )
    // {
    //     return reinterpret_cast<Optimization*>(instance)->_evaluate(x, g, n, step);
    // }

  //   lbfgsfloatval_t Optimization::_evaluate(
	 //    // void *instance,
  //       const lbfgsfloatval_t *x,
  //       lbfgsfloatval_t *g,
  //       const int n,
  //       const lbfgsfloatval_t step
  //       )
  //   {
		// lbfgsfloatval_t fx = 0.0;

		// lbfgsfloatval_t Jb=0.0, Jo=0.0;
		// VectorXd		G,Gb,Go;

		// Go.setZero(nCtlVec_);

		// const VectorXd eigen_x=Eigen::Map<const VectorXd>(x, nCtlVec_);

		// Jb=1/2*eigen_x.dot(eigen_x);
		// Gb=eigen_x;

		// Data2D sqrtBens_times_x,sqrtBens_times_x_minus_V_Inno_;
		// int i=0;
		// sqrtBens_times_x.setZero(sqrtBens_.size()[1],sqrtBens_.size()[2]);

		// for(auto i_iter=sqrtBens_.begin(); i_iter!=sqrtBens_.end(); ++i_iter)
		// {
		// 	sqrtBens_times_x+=*i_iter*eigen_x(i);
		// 	i++;
		// }

		// sqrtBens_times_x_minus_V_Inno_=sqrtBens_times_x-V_InnoENS_.back();

		// int k=0;
		// for(auto k_iter=sqrtBens_.begin(); k_iter!=sqrtBens_.end(); ++k_iter)
		// {
	// 		for(std::size_t i=0; i<k_iter->size()[0]; i++)
	// 		{
	// 			Go(k)=Go(k)+k_iter->at(i).transpose()*Rinv_.asDiagonal()*sqrtBens_times_x_minus_V_Inno_.at(i);
	// 		}
	// 		k++;
	// 	}

	// 	for(auto j_iter=sqrtBens_times_x_minus_V_Inno_.begin(); 
	// 		j_iter!=sqrtBens_times_x_minus_V_Inno_.end(); 
	// 		++j_iter)
	// 	{
	// 		Jo+=(*j_iter).transpose()*Rinv_.asDiagonal()*(*j_iter);		
	// 	}

	// 	fx=Jb+Jo;
	// 	G=Gb+Go;

	// 	Eigen::Map<VectorXd>(g, nCtlVec_)=G;

	// 	return fx;
	// }

    // int Optimization::progress(
	   //  void  *instance,
    //     const lbfgsfloatval_t *x,
    //     const lbfgsfloatval_t *g,
    //     const lbfgsfloatval_t fx,
    //     const lbfgsfloatval_t xnorm,
    //     const lbfgsfloatval_t gnorm,
    //     const lbfgsfloatval_t step,
    //     int n,
    //     int k,
    //     int ls
    //     )
    // {
    //     return reinterpret_cast<Optimization*>(instance)->_progress(x, g, fx, xnorm, gnorm, step, n, k, ls);
    // }

 //    int Optimization::_progress(
	//     // void *instance,
 //        const lbfgsfloatval_t *x,
 //        const lbfgsfloatval_t *g,
 //        const lbfgsfloatval_t fx,
 //        const lbfgsfloatval_t xnorm,
 //        const lbfgsfloatval_t gnorm,
 //        const lbfgsfloatval_t step,
 //        int n,
 //        int k,
 //        int ls
 //        )
 //    {
	// 	cout<<"Iteration "<<k<<endl;
	//     cout<<"fx = "<<fx<<endl;
	//     cout<<"xnorm = "<<xnorm<<" gnorm = "<<gnorm<<" step = "<<step<<endl;
	//     return 0;
	// }

	// Optimization::Optimization(Parameter& par_obj, int test_case)
	// {
	// 	if(test_case==1 || test_case==3)
	// 	nCtlVec_=par_obj.nens();
	// 	else if(test_case==2)
	// 	nCtlVec_=par_obj.nens()*par_obj.r();

	// 	MaxIter_Outer_=par_obj.MaxIter_Outer();
	// 	epsdxx_=par_obj.epsdxx();
	// 	MaxIter_Inner_=par_obj.MaxIter_Inner();
	// 	epsg_=par_obj.epsg();
	// 	epsx_=par_obj.epsx();
	// 	Disp_type_=par_obj.Disp_type();

	// 	//read V_Inno, sqrt_Bens_, Rinv_ from file.
	// 	Data3D      sqBens;
	// 	Data2D 		V_Inno;
	// 	VectorXd	Rinv_;
	// 	Data2D 		sqBensUnit;
	// 	std::vector<VectorXd> sqBensUnit_temp;

	// 	std::string dirpath, Rinvpath, V_Innopath, sqBenspath;

	// 	dirpath="unit_tests/unit_tests_data/opt_test"+std::to_string(test_case);

	// 	Rinvpath=dirpath+"/Rinv";
	// 	V_Innopath=dirpath+"/VInno";
	// 	sqBenspath=dirpath+"/sqBens";

	// 	Rinv=unit_test_readfile_singlecolumn(Rinvpath);
	// 	Rinv_=Rinv;

	// 	V_Inno=unit_test_readfile_threecolumns(V_Innopath);
	// 	V_InnoENS_.setDataSlice(V_Inno);

	// 	sqBens=unit_test_readfile_threecolumns(sqBenspath, par_obj.nSize()*par_obj.nState(), nCtlVec_);
	// 	sqrt_Bens_=sqBens;

	// }
}