#ifndef DATAASSIM
#define DATAASSIM

#include <Eigen/Core>
#include <iostream>
#include <fstream>
#include <vector>
#include "Parameter.hpp"
#include "Model.hpp"
#include "Data.hpp"
#include "Output.hpp"
#include "../Model/MpiDom.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

// using Data2D=EnVar::DataBase<VectorXd>;
// using Data3D=EnVar::DataBase<Data2D>;

namespace EnVar
{

    class DataAssim
    {
    private:   
        int                                     Obs_type_;
        int                                     CycleIndex_;
        int                                     Assimilation_cycle_;
        int                                     nobs_;
        double                                  dtobs_;
        int                                     t_ratio_;
        double                                  t_init_;
        double                                  t_offset_;
        double                                  tobs_assim_;
        double                                  tmax_;
        std::vector<double>                     t_assim_deb_;
        std::vector<double>                     t_assim_fin_;
        double                                  itobs_sum_;
        double                                  itobs_number_;
        double                                  ittru_sum_;
        std::vector<int>                        itobs_deb_;
        std::vector<int>                        itobs_fin_;
        std::vector<int>                        ittru_;
        std::vector<int>                        ittru_assim_deb_;
        std::vector<int>                        ittru_assim_fin_;
        std::vector<int>                        ittru_number_;
        int                                     itmax_;
        int                                     itinit_;
        Parameter::Assimilation_Type            Assimilation_type_;
        int                                     VarRangeDeb_;
        int                                     VarRangeFin_;
        int                                     augState_;
        std::string                             Assimflag_suffix_;
        bool                                    ParameterEstimation_flag_;
        Parameter::Parameter_Type               Parameter_type_;
        Parameter::ParameterPert_Type           ParameterPert_type_;
        Parameter::BackgroundState_Type         BackgroundState_type_;
        Parameter::Algo_ControlVector_Type      Algo_ControlVector_type_;
        Parameter::Algo_Precondition_Type       Algo_Precondition_type_;
        Parameter::Algo_Localization_Type       Algo_Localization_type_;
        Parameter::Algo_EnsembleUpdate_Type     Algo_EnsembleUpdate_type_;
        bool                                    Write_Res_Each_Cycle_flag_;
        MatrixXd                                hb_;
        MatrixXd                                ub_;
        MatrixXd                                vb_;
        VectorXd                                sb_;
        VectorXd                                sf_;
        VectorXd                                sa_;
        Data2D                                  Xobs_cycle_;
        Data2D                                  Xb_;
        Data2D                                  Xf_;
        Data2D                                  Xa_;    
        MatrixXd 		                        RMSXobsXt_;
        MatrixXd 		                        RMSXbXt_;
        MatrixXd 		                        RMSXaXt_;
        Parameter::Err_Type                     Err_type_;
        double                                  Err_hb_;
        double                                  Err_ub_;
        double                                  Err_vb_;
        double                                  Err_ho_;
        double                                  Err_uo_;
        double                                  Err_vo_;
        double                                  Err_beta_;
        double                                  Inc_beta_;
        std::vector<Output>                     Output_cycle_;

    public:
        DataAssim(void);
        ~DataAssim(void);

        DataAssim(Parameter&, ModelPara&, MpiDom&);

        void BackgroundState(Model&, MpiDom&);
        void InitialState(Model&, std::vector<Output>&, MpiDom&);
        void ForecastState(Model&, MpiDom&);
        void AnalysisState(Model&, MpiDom&);
        void UpdateInitialState(const VectorXd&, MpiDom&);
        void CalculateRMS(const Data2D&, const Data2D&, MpiDom&);
        void SaveResult();

        //getters
        const int& Obs_type(void) const{ return Obs_type_;}
        const int& Assimilation_cycle(void) const{ return Assimilation_cycle_;}
        const int& nobs(void) const{ return nobs_;}
        const double&  dtobs(void) const { return dtobs_; }
        const int& t_ratio(void) const { return t_ratio_; }
        const double& t_init(void) const { return t_init_; }
        const double& t_offset(void) const { return t_offset_; }
        const double& tobs_assim(void) const { return tobs_assim_; }
        const double& tmax(void) const { return tmax_; }
        const int& CycleIndex(void) const { return CycleIndex_; }
        const std::vector<double> & t_assim_deb(void) const { return t_assim_deb_; }
        const std::vector<double> & t_assim_fin(void) const{ return t_assim_fin_;}
        const double& itobs_sum(void) const { return itobs_sum_; }
        const double& itobs_number(void) const { return itobs_number_; }
        const double& ittru_sum(void) const { return ittru_sum_; }
        const std::vector<int> & itobs_deb(void) const{ return itobs_deb_;}
        const std::vector<int> & itobs_fin(void) const{ return itobs_fin_;}
        const std::vector<int> & ittru(void) const{ return ittru_;}
        const std::vector<int> & ittru_assim_deb(void) const{ return ittru_assim_deb_;}
        const std::vector<int> & ittru_assim_fin(void) const{ return ittru_assim_fin_;}
        const std::vector<int> & ittru_number(void) const{ return ittru_number_;}
        const int& itmax(void) const{ return itmax_;}
        const int& itinit(void) const{ return itinit_;}
        const Parameter::Assimilation_Type& Assimilation_type(void) const{ return Assimilation_type_;}
        const int& VarRangeDeb(void) const{ return VarRangeDeb_;}
        const int& VarRangeFin(void) const{ return VarRangeFin_;}
        const int& augState(void) const{ return augState_;}
        const std::string& Assimflag_suffix(void) const{ return Assimflag_suffix_;}
        const bool&  ParameterEstimation_flag(void) const { return ParameterEstimation_flag_; }
        const Parameter::Parameter_Type& Parameter_type(void) const{ return Parameter_type_;}
        const Parameter::ParameterPert_Type& ParameterPert_type(void) const{ return ParameterPert_type_;}
        const Parameter::BackgroundState_Type& BackgroundState_type(void) const{ return BackgroundState_type_;}
        const Parameter::Algo_ControlVector_Type& Algo_ControlVector_type(void) const{ return Algo_ControlVector_type_;}
        const Parameter::Algo_Precondition_Type& Algo_Precondition_type(void) const{ return Algo_Precondition_type_;}
        const Parameter::Algo_Localization_Type& Algo_Localization_type(void) const{ return Algo_Localization_type_;}
        const Parameter::Algo_EnsembleUpdate_Type& Algo_EnsembleUpdate_type(void) const{ return Algo_EnsembleUpdate_type_;}
        const bool&  Write_Res_Each_Cycle_flag(void) const { return Write_Res_Each_Cycle_flag_; }
        const VectorXd& sb(void) const{ return sb_;}
        const MatrixXd& hb(void) const{ return hb_;}
        const MatrixXd& ub(void) const{ return ub_;}
        const MatrixXd& vb(void) const{ return vb_;}
        const VectorXd& sf(void) const{ return sf_;}
        const VectorXd& sa(void) const{ return sa_;}
        const Data2D& Xobs_cycle(void) const{ return Xobs_cycle_;}
        const Data2D& Xb(void) const{ return Xb_;}
        const Data2D& Xf(void) const{ return Xf_;}
        const Data2D& Xa(void) const{ return Xa_;}
        const Parameter::Err_Type& Err_type(void) const{ return Err_type_;}
        const double& Err_hb(void) const { return Err_hb_; }
        const double& Err_ub(void) const { return Err_ub_; }
        const double& Err_vb(void) const { return Err_vb_; }
        const double& Err_ho(void) const { return Err_ho_; }
        const double& Err_uo(void) const { return Err_uo_; }
        const double& Err_vo(void) const { return Err_vo_; }
        const double& Err_beta(void) const { return Err_beta_; }
        const double& Inc_beta(void) const { return Inc_beta_; }
        const std::vector<Output> Output_cycle(void) const { return Output_cycle_; }

        //setters
        void setCycleIndex(int CI){ CycleIndex_=CI; };
        void setsa(VectorXd& sanal){ sa_=sanal; };

    };

    //non-member non-friend function
    Data2D TimeDownSample2D(const Data2D&, const int&);

}
#endif