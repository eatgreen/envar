#include"Localization.hpp"

namespace EnVar{
		
	Localization::Localization(){}

	Localization::~Localization(){}

	Localization::Localization(Parameter& par_obj){
		Localization_type_=par_obj.Localization_type();
		cod_=par_obj.cod();
		r_=par_obj.r();
		LocalSizex_=par_obj.LocalSizex();
		LocalSizey_=par_obj.LocalSizey();
		dx_=par_obj.DA_modelpara().dimpara().dx();
		dy_=par_obj.DA_modelpara().dimpara().dy();
	}

	void Localization::Construct5thOrderCorrelationMatrix(MpiDom& mpi_obj)
	{
        auto t_deb=Clock::now();

        if (mpi_obj.myrank()==mpi_obj.root()){
			double 	 s,z;
			int      kmin,kmax,lmin,lmax;
			cor_mat_glo_.setZero( mpi_obj.nSize_glo(), mpi_obj.nSize_glo() );

            cout << "Localization::make the correlation matrix" << endl;

			s=double(cod_)/2.0*dx_;

			for(std::size_t i=0; i<(unsigned int)mpi_obj.nx_glo(); i++)
			{
				for(std::size_t j=0; j<(unsigned int)mpi_obj.ny_glo(); j++)
				{
					kmin=std::max(0,int(i-cod_));
					kmax=std::min(int(i+cod_),mpi_obj.nx_glo());
					lmin=std::max(0,int(j-cod_));
					lmax=std::min(int(j+cod_),mpi_obj.ny_glo());

					for(std::size_t k=kmin; k<(unsigned int)kmax; k++)
					{
						for(std::size_t l=lmin; l<(unsigned int)lmax; l++)
						{
							// cout<<"i "<<i<<" j "<<j<<" k "<<k<<" l "<<l<<endl;
							z=std::sqrt(std::pow(mpi_obj.xx_glo()(i,j)-mpi_obj.xx_glo()(k,l),2)+std::pow(mpi_obj.yy_glo()(i,j)-mpi_obj.yy_glo()(k,l),2));
							if(z<=s)
								cor_mat_glo_(j+i*mpi_obj.ny_glo(),l+k*mpi_obj.ny_glo())=-std::pow(z/s,5)/4+std::pow(z/s,4)/2+5*std::pow(z/s,3)/8-5*std::pow(z/s,2)/3+1;
							else if(z>s && z<=2*s) 
			                    cor_mat_glo_(j+i*mpi_obj.ny_glo(),l+k*mpi_obj.ny_glo())=std::pow(z/s,5)/12-std::pow(z/s,4)/2+5*std::pow(z/s,3)/8+5*std::pow(z/s,2)/3-5*(z/s)+4-2/(3*z/s);
			                // else
			                // 	cor_mat_glo_(j+i*ny_,l+k*ny_)=0;
						}
					}
				}
			}
		}

        auto t_fin=Clock::now();

        if (mpi_obj.myrank()==mpi_obj.root())
        	cout<<"time elapsed in Localization Construct5thOrderCorrelationMatrix: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
	}

	class Localization::Apply_Cor_mat
	{
	private:
		int 		rows_;
		int 		cols_;
		int 		nx_;
		int 	 	ny_;
		MatrixXd 	cor_mat_glo_;

	public:
		explicit Apply_Cor_mat(Localization& loc_obj, MpiDom& mpi_obj)
			: nx_(mpi_obj.nx_glo()), ny_(mpi_obj.ny_glo()), cor_mat_glo_(loc_obj.cor_mat_glo_)
		{
			rows_=mpi_obj.nSize_glo()*mpi_obj.nState();
			cols_=mpi_obj.nSize_glo()*mpi_obj.nState();
		}

		int rows() const { return rows_; }
		int cols() const { return cols_; }
		
		void perform_op(const double *x_in, double *y_out) const
		{
			Eigen::Map<const VectorXd> x(x_in,  cols_);
			Eigen::Map<VectorXd>       y(y_out, rows_);

			VectorXd y0;

			y0=cor_mat_glo_*x.head(nx_*ny_)+cor_mat_glo_*x.segment(nx_*ny_,nx_*ny_)+cor_mat_glo_*x.tail(nx_*ny_);

			y.head(nx_*ny_)=y0;
			y.segment(nx_*ny_,nx_*ny_)=y0;
			y.tail(nx_*ny_)=y0;
		}
	};

	void Localization::SpectralDecompositionOfCorrelationMatrix(MpiDom& mpi_obj)
	{
        auto t_deb=Clock::now();

        if (mpi_obj.myrank()==mpi_obj.root()){
			Apply_Cor_mat op(*this, mpi_obj);
			SymEigsSolver< double, LARGEST_ALGE, Apply_Cor_mat > eigs(&op, r_, 2*r_);

			eigs.init();

			int nconv=eigs.compute();

			VectorXd r_eigenvalues=eigs.eigenvalues();
			MatrixXd r_eigenvectors=eigs.eigenvectors(r_);

			// cout<<"r_eigenvalues  size: "<<r_eigenvalues.size()<<endl;
			// cout<<"r_eigenvectors rows: "<<r_eigenvectors.rows()<<endl;
			// cout<<"r_eigenvectors cols: "<<r_eigenvectors.cols()<<endl;

			VectorXd sqrt_lambda_r=r_eigenvalues.cwiseSqrt();

			sqrt_Cor_mat_glo_=r_eigenvectors*sqrt_lambda_r.asDiagonal();
		}

        auto t_fin=Clock::now();

        if (mpi_obj.myrank()==mpi_obj.root())
        	cout<<"time elapsed in Localization SpectralDecompositionOfCorrelationMatrix: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
	
	}

	void Localization::CorrelationMatrix(MpiDom& mpi_obj)
	{
        if (mpi_obj.myrank()==mpi_obj.root()){
			double 	 s,z;
			int      kmin,kmax,lmin,lmax;
			cor_mat_glo_.setZero( mpi_obj.nSize_glo(), mpi_obj.nSize_glo() );

            cout << "Localization::make the correlation matrix" << endl;

			s=double(cod_)/2.0*dx_;

			for(std::size_t i=0; i<(unsigned int)mpi_obj.nx_glo(); i++)
			{
				for(std::size_t j=0; j<(unsigned int)mpi_obj.ny_glo(); j++)
				{
					kmin=std::max(0,int(i-cod_));
					kmax=std::min(int(i+cod_),mpi_obj.nx_glo());
					lmin=std::max(0,int(j-cod_));
					lmax=std::min(int(j+cod_),mpi_obj.ny_glo());

					// cout<<"kmin "<<kmin<<" kmax "<<kmax<<" lmin "<<lmin<<" lmax "<<lmax<<endl;

					for(std::size_t k=kmin; k<(unsigned int)kmax; k++)
					{
						for(std::size_t l=lmin; l<(unsigned int)lmax; l++)
						{
							// cout<<"i "<<i<<" j "<<j<<" k "<<k<<" l "<<l<<endl;
							z=std::sqrt(std::pow(mpi_obj.xx_glo()(i,j)-mpi_obj.xx_glo()(k,l),2)+std::pow(mpi_obj.yy_glo()(i,j)-mpi_obj.yy_glo()(k,l),2));
							if(z<=s)
								cor_mat_glo_(j+i*mpi_obj.ny_glo(),l+k*mpi_obj.ny_glo())=-std::pow(z/s,5)/4+std::pow(z/s,4)/2+5*std::pow(z/s,3)/8-5*std::pow(z/s,2)/3+1;
							else if(z>s && z<=2*s) 
			                    cor_mat_glo_(j+i*mpi_obj.ny_glo(),l+k*mpi_obj.ny_glo())=std::pow(z/s,5)/12-std::pow(z/s,4)/2+5*std::pow(z/s,3)/8+5*std::pow(z/s,2)/3-5*(z/s)+4-2/(3*z/s);
			                // else
			                // 	cor_mat_glo_(j+i*mpi_obj.ny_glo(),l+k*mpi_obj.ny_glo())=0;
						}
					}
				}
			}

			Cor_mat_glo_.resize(mpi_obj.nSize_glo()*mpi_obj.nState(),mpi_obj.nSize_glo()*mpi_obj.nState());

			Cor_mat_glo_=cor_mat_glo_.replicate(mpi_obj.nState(),mpi_obj.nState());

			// Eigen::SelfAdjointEigenSolver<MatrixXd> es(Cor_mat_glo_);
			
			// VectorXd sqrt_lambda_r=es.eigenvalues().tail(r_).cwiseSqrt();
			// MatrixXd Ev_r=es.eigenvectors().rightCols(r_);

			// sqrt_Cor_mat_glo_=Ev_r*sqrt_lambda_r.asDiagonal();

			DenseSymMatProd<double> op(Cor_mat_glo_);

			SymEigsSolver< double, LARGEST_ALGE, DenseSymMatProd<double> > eigs(&op, r_, 2*r_);

			eigs.init();

			int nconv=eigs.compute();

			VectorXd r_eigenvalues=eigs.eigenvalues();
			MatrixXd r_eigenvectors=eigs.eigenvectors(r_);

			cout<<"r_eigenvalues  size: "<<r_eigenvalues.size()<<endl;
			cout<<"r_eigenvectors rows: "<<r_eigenvectors.rows()<<endl;
			cout<<"r_eigenvectors cols: "<<r_eigenvectors.cols()<<endl;

			VectorXd sqrt_lambda_r=r_eigenvalues.cwiseSqrt();

			sqrt_Cor_mat_glo_=r_eigenvectors*sqrt_lambda_r.asDiagonal();

			// cout<<"sqrt_Cor_mat_glo rows: "<<sqrt_Cor_mat_glo_.rows()<<endl;
			// cout<<"sqrt_Cor_mat_glo cols: "<<sqrt_Cor_mat_glo_.cols()<<endl;

            //Save global correlation matrix for debug purposes
      		// NetCDFIO sqrt_Cor_mat_glo_h_file("sqrt_Cor_mat_glo_h", Parameter::Global, mpi_obj);
    		// sqrt_Cor_mat_glo_h_file.Create_File();
    		// std::string var_sqrt_Cor_mat_glo;
    		// MatrixXd sqrt_Cor_mat_glo_h_j_temp;

      	//      for (std::size_t j=0; j<(unsigned int)r_; j++){
      	//         sqrt_Cor_mat_glo_h_j_temp=Eigen::Map<MatrixXd>(sqrt_Cor_mat_glo_.col(j).head(mpi_obj.nSize_glo()).data(), mpi_obj.nx(), mpi_obj.ny());
            
	    //    	if(j==0)
	    //    		sqrt_Cor_mat_glo_h_file.Add_Matrix_Dim(sqrt_Cor_mat_glo_h_j_temp);
	        	
	    //    	var_sqrt_Cor_mat_glo="sqrt_Cor_glo"+std::to_string(j);
	    //    	sqrt_Cor_mat_glo_h_file.Write_Matrix(sqrt_Cor_mat_glo_h_j_temp,var_sqrt_Cor_mat_glo);
      	//      }
            //Save global correlation matrix for debug purposes
		}
	}


	void Localization::ScatterCorrelationMatrix(MpiDom& mpi_obj){
        auto t_deb=Clock::now();
		//mpi_scatter sqrt_Cor_mat_ to child processes
		//sqrt_Cor_mat_ (n*r) send to each process
#if defined(MPI) || defined(MPI_simple)
		MatrixXd sqrt_Cor_mat_glo_h_j,sqrt_Cor_mat_glo_u_j,sqrt_Cor_mat_glo_v_j;
		MatrixXd sqrt_Cor_mat_loc_h_j,sqrt_Cor_mat_loc_u_j,sqrt_Cor_mat_loc_v_j;
		VectorXd sqrt_Cor_mat_loc_j;

		sqrt_Cor_mat_.resize(mpi_obj.nx_loc()*mpi_obj.ny_loc()*mpi_obj.nState(), r_);
		sqrt_Cor_mat_loc_j.resize(mpi_obj.nx_loc()*mpi_obj.ny_loc()*mpi_obj.nState());
		sqrt_Cor_mat_loc_h_j.resize(mpi_obj.nx_loc(),mpi_obj.ny_loc());
		sqrt_Cor_mat_loc_u_j.resize(mpi_obj.nx_loc(),mpi_obj.ny_loc());
		sqrt_Cor_mat_loc_v_j.resize(mpi_obj.nx_loc(),mpi_obj.ny_loc());

        int sendcounts[ mpi_obj.mysize() ];
        int senddispls[ mpi_obj.mysize() ];
        MPI_Datatype sendtypes[ mpi_obj.mysize() ];
        int recvcounts[ mpi_obj.mysize() ];
        int recvdispls[ mpi_obj.mysize() ];
        MPI_Datatype recvtypes[ mpi_obj.mysize() ];

        for (int proc=0; proc<mpi_obj.mysize(); proc++){
            recvcounts[proc] = 0;
            recvdispls[proc] = 0;
            recvtypes[proc]  = MPI_DOUBLE;

            sendcounts[proc] = 0;
            senddispls[proc] = 0;
            sendtypes[proc]  = MPI_DOUBLE;
        }

        recvcounts[mpi_obj.root()] = mpi_obj.nx_loc()*mpi_obj.ny_loc();
        recvdispls[mpi_obj.root()] = 0;

        MPI_Datatype local_field_type[mpi_obj.mysize()];

        int bigsizes[2] = {mpi_obj.nx_glo(), mpi_obj.ny_glo()};

        if (mpi_obj.myrank()==mpi_obj.root()){

            int subsizes[2];
            int starts[2]   = {0, 0};

            double displs_idx;

            for (int i=0; i<mpi_obj.proc_ni(); i++){
                for (int j=0; j<mpi_obj.proc_nj(); j++){
                    subsizes[0]=mpi_obj.nx_loc_vec().at( i*mpi_obj.proc_nj()+j );
                    subsizes[1]=mpi_obj.ny_loc_vec().at( i*mpi_obj.proc_nj()+j );
                    MPI_Type_create_subarray(2, bigsizes, subsizes, starts,
                                             MPI_ORDER_FORTRAN, MPI_DOUBLE, &local_field_type[ i*mpi_obj.proc_nj()+j ]);

                    MPI_Type_commit(&local_field_type[ i*mpi_obj.proc_nj()+j ]);

                    sendcounts[ i*mpi_obj.proc_nj()+j ]=1;

                    displs_idx=mpi_obj.iy_loc_vec().at(i*mpi_obj.proc_nj()+j)*mpi_obj.nx_glo()
                                + mpi_obj.ix_loc_vec().at(i*mpi_obj.proc_nj()+j); 

                    senddispls[ i*mpi_obj.proc_nj()+j ]=displs_idx*sizeof(MPI_DOUBLE);
                    
                    sendtypes[ i*mpi_obj.proc_nj()+j ]=local_field_type[ i*mpi_obj.proc_nj()+j ];
                }
            }
        }

        // NetCDFIO sqrt_Cor_mat_loc_h_file("sqrt_Cor_mat_h", Parameter::Local, mpi_obj);
        // sqrt_Cor_mat_loc_h_file.Create_File();
        // std::string var_sqrt_Cor_mat;

        for (std::size_t j=0; j<(unsigned int)r_; j++){
            if (mpi_obj.myrank()==mpi_obj.root()){
                sqrt_Cor_mat_glo_h_j=Eigen::Map<MatrixXd>(sqrt_Cor_mat_glo_.col(j).head(mpi_obj.nSize_glo()).data(), mpi_obj.nx_glo(), mpi_obj.ny_glo());
                sqrt_Cor_mat_glo_u_j=Eigen::Map<MatrixXd>(sqrt_Cor_mat_glo_.col(j).segment(mpi_obj.nSize_glo(),mpi_obj.nSize_glo()).data(), mpi_obj.nx_glo(), mpi_obj.ny_glo());
                sqrt_Cor_mat_glo_v_j=Eigen::Map<MatrixXd>(sqrt_Cor_mat_glo_.col(j).tail(mpi_obj.nSize_glo()).data(), mpi_obj.nx_glo(), mpi_obj.ny_glo());
            }

            MPI_Alltoallw(sqrt_Cor_mat_glo_h_j.data(), sendcounts, senddispls, sendtypes, 
                          sqrt_Cor_mat_loc_h_j.data(), recvcounts, recvdispls, recvtypes, 
                          MPI_COMM_WORLD);

            MPI_Alltoallw(sqrt_Cor_mat_glo_u_j.data(), sendcounts, senddispls, sendtypes, 
                          sqrt_Cor_mat_loc_u_j.data(), recvcounts, recvdispls, recvtypes, 
                          MPI_COMM_WORLD);

            MPI_Alltoallw(sqrt_Cor_mat_glo_v_j.data(), sendcounts, senddispls, sendtypes, 
                          sqrt_Cor_mat_loc_v_j.data(), recvcounts, recvdispls, recvtypes, 
                          MPI_COMM_WORLD);

            //Save local correlation matrix for debug purposes
	        // if (j==0)
		       //  sqrt_Cor_mat_loc_h_file.Add_Matrix_Dim(sqrt_Cor_mat_loc_h_j);
	            
	        // var_sqrt_Cor_mat="sqrt_Cor_loc"+std::to_string(j);
	        // sqrt_Cor_mat_loc_h_file.Write_Matrix(sqrt_Cor_mat_loc_h_j,var_sqrt_Cor_mat);
            //The end of save local correlation matrix for debug purposes

            sqrt_Cor_mat_loc_j.head(mpi_obj.nx_loc()*mpi_obj.ny_loc())=Eigen::Map<VectorXd>(sqrt_Cor_mat_loc_h_j.data(), sqrt_Cor_mat_loc_h_j.rows()*sqrt_Cor_mat_loc_h_j.cols());
            sqrt_Cor_mat_loc_j.segment(mpi_obj.nx_loc()*mpi_obj.ny_loc(), mpi_obj.nx_loc()*mpi_obj.ny_loc())=Eigen::Map<VectorXd>(sqrt_Cor_mat_loc_u_j.data(), sqrt_Cor_mat_loc_u_j.rows()*sqrt_Cor_mat_loc_u_j.cols());
            sqrt_Cor_mat_loc_j.tail(mpi_obj.nx_loc()*mpi_obj.ny_loc())=Eigen::Map<VectorXd>(sqrt_Cor_mat_loc_v_j.data(), sqrt_Cor_mat_loc_h_j.rows()*sqrt_Cor_mat_loc_v_j.cols());

            sqrt_Cor_mat_.col(j)=sqrt_Cor_mat_loc_j;
        }

		// cout<<"my rank: "<<mpi_obj.myrank()<<" sqrt_Cor_mat_loc rows: "<<sqrt_Cor_mat_.rows()<<endl;
		// cout<<"my rank: "<<mpi_obj.myrank()<<" sqrt_Cor_mat_loc cols: "<<sqrt_Cor_mat_.cols()<<endl;
#else 
		sqrt_Cor_mat_=sqrt_Cor_mat_glo_;
#endif

        auto t_fin=Clock::now();
        if (mpi_obj.myrank()==mpi_obj.root())
            cout<<"time elapsed in Localization ScatterCorrelationMatrix: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
	}

	void Localization::save_Pb0(const Data3D& sqrtBens, MpiDom& mpi_obj)
	{
		MatrixXd B,sqrtB;
		B.resize(sqrtBens.size()[2], sqrtBens.size()[2]);
		sqrtB.resize(sqrtBens.size()[2], sqrtBens.size()[0]);

		int i=0;
		for(auto i_iter=sqrtBens.begin(); i_iter!=sqrtBens.end(); ++i_iter)
		{
			sqrtB.col(i)=i_iter->at(0);
			i++;
		}

		assert(sqrtB.cols()==sqrtBens.size()[0]);
		assert(sqrtB.rows()==sqrtBens.size()[2]);
		cout<<"sqrtB has "<<sqrtB.rows()<<" rows and "<<sqrtB.cols()<<" columns."<<endl;

		NetCDFIO sqrtB_mat_file("sqrtB_mat", Parameter::Local, mpi_obj);
		sqrtB_mat_file.Create_File();
		std::string var_sqrtB("sqrtB");
		sqrtB_mat_file.Add_Matrix_Dim(sqrtB);
		sqrtB_mat_file.Write_Matrix(sqrtB,var_sqrtB);

		if ( Localization_type_ == Parameter::NoneLocal )
		{
			B=sqrtB*sqrtB.transpose();

			NetCDFIO B_mat_file("B_mat", Parameter::Local, mpi_obj);
			B_mat_file.Create_File();
			std::string var_B("B");
			B_mat_file.Add_Matrix_Dim(B);
			B_mat_file.Write_Matrix(B,var_B);
		}
		else if ( Localization_type_ == Parameter::Localize_Covariance )
		{
			MatrixXd sqrtP,Xb_rep;
			sqrtP.resize(sqrtBens.size()[2], sqrtBens.size()[0]*r_);

			for(std::size_t i=0; i<sqrtBens.size()[0]; i++)
			{
				Xb_rep=sqrtB.col(i).replicate(1,r_);
				sqrtP.block(0,i*r_,sqrtP.rows(),r_)=Xb_rep.array()*sqrt_Cor_mat_.array();
			}

			B=sqrtP*sqrtP.transpose();

			NetCDFIO P_mat_file("P_mat", Parameter::Local, mpi_obj);
			P_mat_file.Create_File();
			std::string var_B("B");
			P_mat_file.Add_Matrix_Dim(B);
			P_mat_file.Write_Matrix(B,var_B);
		}

	}

    VectorXd  Localization::LocalAnalysisOperatorToVector(const VectorXd& X, int& iInd, int& jInd, int& lx, int& ly, MpiDom& mpi_obj)
	{
		VectorXd res;
		MatrixXd h,u,v,lh,lu,lv;

		h=get_h(X, mpi_obj.nx_glo(), mpi_obj.ny_glo());
		u=get_u(X, mpi_obj.nx_glo(), mpi_obj.ny_glo());
		v=get_v(X, mpi_obj.nx_glo(), mpi_obj.ny_glo());

		lh=h.block(iInd, jInd, lx, ly);
		lu=u.block(iInd, jInd, lx, ly);
		lv=v.block(iInd, jInd, lx, ly);

		res=get_ctl_vector(lh, lu, lv);
		return res;
	}

    VectorXd  Localization::LocalAnalysisOperator(const VectorXd& X, int& iInd, int& jInd, int& lx, int& ly, MpiDom& mpi_obj)
	{
		VectorXd res;

		res=LocalAnalysisOperatorToVector(X, iInd, jInd, lx, ly, mpi_obj);
		return res;
	}

    Data2D  Localization::LocalAnalysisOperator(const Data2D& X, int& iInd, int& jInd, int& lx, int& ly, MpiDom& mpi_obj)
	{
		Data2D res;
		VectorXd ctl_v;

		for (auto i_iter=X.begin(); i_iter!=X.end(); ++i_iter)
		{

			ctl_v=LocalAnalysisOperatorToVector(*i_iter, iInd, jInd, lx, ly, mpi_obj);

			res.setDataSlice(ctl_v);
		}
		return res;
	}


    Data3D  Localization::LocalAnalysisOperator(const Data3D& X, int& iInd, int& jInd, int& lx, int& ly, MpiDom& mpi_obj)
	{
		Data3D res;
		Data2D res_member;
		VectorXd ctl_v;

		for (auto n_iter=X.begin(); n_iter!=X.end(); ++n_iter)
		{
			for (auto i_iter=n_iter->begin(); i_iter!=n_iter->end(); ++i_iter)
			{
				ctl_v=LocalAnalysisOperatorToVector(*i_iter, iInd, jInd, lx, ly, mpi_obj);

				res_member.setDataSlice(ctl_v);
			}
			res.setDataSlice(res_member);
		}
		return res;
	}

}