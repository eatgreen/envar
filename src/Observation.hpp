#ifndef OBSERVATION
#define OBSERVATION

#include <Eigen/Core>
#include <vector>
#include <fstream>
#include "Parameter.hpp"
#include "Data.hpp"
#include "DataAssim.hpp"
#include "NormalRandom.hpp"
#include "Model.hpp"
#include "../model/MpiDom.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

// using Data2D=EnVar::DataBase<VectorXd>;
// using Data3D=EnVar::DataBase<Data2D>;

namespace EnVar
{
    class Observation
    {
    public:
        // Constructor and destructor.
        Observation();
        Observation(Parameter&, MpiDom&);
        ~Observation();

        // Methods
        void   make_true_initialcondition(DataAssim&, ModelPara&, MpiDom&);
        void   make_true_solution(DataAssim&, Model&, MpiDom&);

        void   make_obs(DataAssim&, MpiDom&, ModelPara* synthobs_modpar_obj_ptr=NULL);
        void   make_invR(DataAssim&, MpiDom&);
        void   make_obsens(DataAssim&, int, MpiDom&);

        VectorXd ObservationSpace_operator(const VectorXd&, Parameter::Assimilation_Type);
        Data2D   ObservationSpace_operator(const Data2D&, Parameter::Assimilation_Type);

    private:
        double          codbkg_;
        MatrixXd        bkgnoise_;
        MatrixXd        h0_;
        MatrixXd        u0_;
        MatrixXd        v0_;
        VectorXd        x0_;
        Data2D          Xt_;
        std::string     obsreal_path_;
        bool            obsreal_flag_;
        Data2D          Xobs_;
        Data2D          dXobs_;
        VectorXd        Rinv_;
        Data3D          XobsENS_;
        int             nobsSize_;
        int             nobsState_;
        int             nState_;

    public:
        const VectorXd& x0(void) const { return x0_; }
        const Data2D&   Xt(void) const { return Xt_; }

        const bool&         obsreal_flag(void) const{ return obsreal_flag_;}
        const std::string&  obsreal_path(void) const{ return obsreal_path_;}

        const Data2D&   Xobs(void) const { return Xobs_; }

        const VectorXd& Rinv(void) const { return Rinv_; }
        const Data3D&   XobsENS(void) const { return XobsENS_; }

        const int& nobsSize(void) const { return nobsSize_; }
        const int& nobsState(void) const { return nobsState_; }
    };

    void make_obssynth(Observation&, DataAssim&, Model&, ModelPara&, int, MpiDom&);
    void make_obsreal(Observation&, DataAssim&, int, MpiDom&);

}
#endif
