#ifndef OPTIMIZATION
#define OPTIMIZATION

#include <Eigen/Core>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
#include <chrono>
// #include <lbfgs.h>
#include "Parameter.hpp"
#include "Data.hpp"
#include "Observation.hpp"
#include "Ensemble.hpp"
#include "Localization.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

using Clock=std::chrono::high_resolution_clock;

namespace EnVar{
	class Optimization{
	private:
        VectorXd                delta_z_;
        MatrixXd                delta_zENS_;
        VectorXd                delta_x_;
        MatrixXd                delta_xENS_;
        int                     nens_;
        int                     nobsState_;
        int                     nCtlVec_;
        int                     itobs_number_;
		int 			        MaxIter_Outer_;
        double  				epsdxx_;
        int			            MaxIter_Inner_;
        double  				epsg_;
        double  				epsx_;
        Parameter::Disp_Type	Disp_type_;

    public:

    	Optimization();
    	~Optimization();

        Optimization(Parameter&);
        Optimization(Parameter&, int);

        void        OptInner(Ensemble& , Observation& , Localization&, MpiDom&);
        void        OptSingle(Data3D&, Data3D&, VectorXd&, Localization&, Data3D&, MpiDom&);
        void        OptEnsemble(Data3D&, Data3D&, VectorXd&, Localization&, Data3D&, MpiDom&);

        void        OptLocalInner(Ensemble&, Observation&, Localization&, DataAssim&, MpiDom&);
        MatrixXd    OptLocalSingle(Data2D&, Data3D&, VectorXd&, Localization&, MpiDom&);

        VectorXd    Inner_cg(double, VectorXd&, Data3D&, VectorXd&, Localization&, MpiDom&);

        // int     OptInner_lbfgs();

    protected:

        // lbfgsfloatval_t _evaluate(
        // const lbfgsfloatval_t*,
        // lbfgsfloatval_t*,
        // const int,
        // const lbfgsfloatval_t);

        // int _progress(
        // const lbfgsfloatval_t*,
        // const lbfgsfloatval_t*,
        // const lbfgsfloatval_t,
        // const lbfgsfloatval_t,
        // const lbfgsfloatval_t,
        // const lbfgsfloatval_t,
        // int,
        // int,
        // int);

        // static lbfgsfloatval_t evaluate(void*,
        // const lbfgsfloatval_t*,
        // lbfgsfloatval_t*,
        // const int,
        // const lbfgsfloatval_t);
        
        // static int progress(void*,
        // const lbfgsfloatval_t*,
        // const lbfgsfloatval_t*,
        // const lbfgsfloatval_t,
        // const lbfgsfloatval_t,
        // const lbfgsfloatval_t,
        // const lbfgsfloatval_t,
        // int,
        // int,
        // int);

    public:
        const VectorXd& delta_x(void) const{ return delta_x_; }
        const VectorXd& delta_z(void) const{ return delta_z_; }
        const MatrixXd& delta_xENS(void) const{ return delta_xENS_; }
        const MatrixXd& delta_zENS(void) const{ return delta_zENS_; }
        const int& nCtlVec(void) const{ return nCtlVec_; }
        const int& nens(void) const{ return nens_; }
        const int& itobs_number(void) const{ return itobs_number_; }
        const int& MaxIter_Outer(void) const{ return MaxIter_Outer_; }
        const int& MaxIter_Inner(void) const{ return MaxIter_Inner_; }
        const double& epsdxx(void) const{ return epsdxx_; }
        const double& epsg(void) const{ return epsg_; }
        const double& epsx(void) const{ return epsx_; }
        const Parameter::Disp_Type& Disp_type(void) const{ return Disp_type_; }
	};

    Data2D    apply_Rinv(const Data2D&, const VectorXd&, const int&);

    VectorXd  apply_sqBTRinvsqB(const VectorXd&, const Data3D&, const VectorXd&, const int&, const int&, const int&, MpiDom&);
    VectorXd  apply_sqBTRinv(const Data3D&, const Data2D&, const VectorXd&, const int&, const int&, MpiDom&);
    Data2D    apply_sqB(const VectorXd&, const Data3D&, const int&, const int&, MpiDom&);
    VectorXd  apply_sqBT(const Data2D&, const Data3D&, const int&, const int&, MpiDom&);
    VectorXd  apply_sqB0_state(const VectorXd&, const Data3D&, MpiDom&);

    VectorXd  apply_sqBTRinvsqB(const VectorXd&, const Data3D&, const VectorXd&, const int&, const int&, const int&, const int&, Localization&, MpiDom&);
    VectorXd  apply_sqBTRinv(const Data3D&, const Data2D&, const VectorXd&, const int&, const int&, const int&, Localization&, MpiDom&);
    Data2D    apply_sqB(const VectorXd&, const Data3D&, const int&, const int&, const int&, Localization&, MpiDom&);
    VectorXd  apply_sqBT(const Data2D&, const Data3D&, const int&, const int&, const int&, Localization&, MpiDom&);
    VectorXd  apply_sqB0_state(const VectorXd&, const Data3D&, const int&, Localization&, MpiDom&);

}

#endif