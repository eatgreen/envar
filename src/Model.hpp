#ifndef MODEL
#define MODEL

#include <Eigen/Core>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>
#include "Parameter.hpp"
#include "Data.hpp"
#include "../model/ModelPara.hpp"
#include "../model/MpiDom.hpp"
#include "../model/ShallowWaterModel.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

using Clock=std::chrono::high_resolution_clock;

// using Data2D=EnVar::DataBase<VectorXd>;
// using Data3D=EnVar::DataBase<Data2D>;

namespace EnVar
{
    class Model
    {
    public:
        // Constructor and destructor.
        Model();
        ~Model();

    public:
        ShallowWaterModel swmodel_;

    public:
        Model(ModelPara&, MpiDom&);

        ShallowWaterModel& Imp(void) { return swmodel_; }
    };

    Data2D runmodel_frame(const VectorXd& x0, double t0, double tf, Model& mod_obj, MpiDom& mpi_obj);

    VectorXd runmodel_final(const VectorXd& x0, double t0, double tf, Model& mod_obj, MpiDom& mpi_obj, int ens_member=1, int nens=1);
 
}
#endif
