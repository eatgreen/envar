#ifndef ENSEMBLE
#define ENSEMBLE

#include <Eigen/Core>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>
// #include <omp.h> 
#include "Parameter.hpp"
#include "Data.hpp"
#include "Model.hpp"
#include "Observation.hpp"
#include "DataAssim.hpp"
#include "NormalRandom.hpp"
#include "../Model/MpiDom.hpp"
#if defined(MPI) || defined(MPI_simple)
#include <mpi.h> 
#endif

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

using Clock=std::chrono::high_resolution_clock;

// using Data2D=EnVar::DataBase<VectorXd>;
// using Data3D=EnVar::DataBase<EnVar::DataBase<VectorXd>>;

namespace EnVar{

    class Ensemble
    {
    private:
        int                       nens_;
        double                    codENS_;
        Parameter::Ensemble_Type  Ensemble_type_;
        std::string               EnsType_suffix_;
        bool                      EnsembleInflation_flag_;
        std::string               EnsInf_suffix_;

        MatrixXd                  xensf_;
        MatrixXd                  xensa_;
        Data3D                    Xens_;
        Data2D                    Xens_mean_;
        Data3D                    MSb_;
        Data3D                    HMSb_;
        Data3D                    V_InnoENS_;

    public:
        // Main constructor
        Ensemble();
        Ensemble(Parameter&);
        // Destructor
        ~Ensemble();
        // Methods
        void    EnsembleInitial(DataAssim&, Model&, MpiDom& );
        void    UpdateEnsembleInitial(const MatrixXd& );
        void    EnsembleForecast(DataAssim&, Model&, MpiDom& );
        void    EnsembleAnomaly(DataAssim&, Observation&, MpiDom& );
        Data3D  EnsembleAnomalyLocal(const Data3D&, const Data2D&, MpiDom& );
        void    EnsembleInnovation(DataAssim&, const Data3D&, MpiDom& );
        Data2D  EnsembleInnovationLocal(DataAssim&, const Data2D&, const Data2D&, MpiDom& );
        void    EnsembleIntegration(DataAssim&, Model&, MpiDom& );

        const int& nens(void) const{ return nens_;}
        const MatrixXd& xensf(void) const{ return xensf_;}
        const MatrixXd& xensa(void) const{ return xensa_;}
        const Data3D& Xens(void) const{ return Xens_;}
        const Data2D& Xens_mean(void) const{ return Xens_mean_;}
        const Data3D& MSb(void) const{ return MSb_;}
        const Data3D& HMSb(void) const{ return HMSb_;}
        const Data3D& V_InnoENS(void) const{ return V_InnoENS_;}

    };
}
#endif