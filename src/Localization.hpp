#ifndef LOCALIZATION
#define LOCALIZATION

#include <Eigen/Dense>
#include <SymEigsSolver.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>

#include "Parameter.hpp"
#include "Data.hpp"
#include "Model.hpp"
#include "DataAssim.hpp"
#include "Ensemble.hpp"
#include "../Model/MpiDom.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

using namespace Spectra;

using std::cout;
using std::endl;

using Clock=std::chrono::high_resolution_clock;

// using Data2D=EnVar::DataBase<VectorXd>;
// using Data3D=EnVar::DataBase<Data2D>;

namespace EnVar
{
	class Localization
    {
	private:
        Parameter::Algo_Localization_Type       Localization_type_;
        double                                  cod_;             //Cutoff distance
        int                                     r_;               //Truncated number of eigenvalues
        int                                     LocalSizex_;      //local size x
        int                                     LocalSizey_;      //local size y
        MatrixXd                                cor_mat_glo_;
        MatrixXd                                Cor_mat_glo_;       
        MatrixXd                                sqrt_Cor_mat_glo_;
        MatrixXd                                sqrt_Cor_mat_;               

        double                                  dx_;
        double                                  dy_;

    public:

    	Localization();
    	~Localization();

    	Localization(Parameter&);    

        // Methods
        void Construct5thOrderCorrelationMatrix(MpiDom&);
        class Apply_Cor_mat;
        void SpectralDecompositionOfCorrelationMatrix(MpiDom&);

        void CorrelationMatrix(MpiDom&);

        void ScatterCorrelationMatrix(MpiDom&);

        void save_Pb0(const Data3D&, MpiDom&);

        VectorXd LocalAnalysisOperatorToVector(const VectorXd&, int&, int&, int&, int&, MpiDom&);
        VectorXd LocalAnalysisOperator(const VectorXd&, int&, int&, int&, int&, MpiDom&);
        Data2D LocalAnalysisOperator(const Data2D&, int&, int&, int&, int&, MpiDom&);
        Data3D LocalAnalysisOperator(const Data3D&, int&, int&, int&, int&, MpiDom&);

        // getters
        const Parameter::Algo_Localization_Type& Localization_type(void) const{ return Localization_type_; }
        const double& cod(void) const{ return cod_; }
        const int& r(void) const{ return r_; }
        const int& LocalSizex(void) const{ return LocalSizex_; }
        const int& LocalSizey(void) const{ return LocalSizey_; }
        const MatrixXd& cor_mat_glo(void) const{ return cor_mat_glo_; }
        const MatrixXd& Cor_mat_glo(void) const{ return Cor_mat_glo_; }
        const MatrixXd& sqrt_Cor_mat_glo(void) const{ return sqrt_Cor_mat_glo_; }
        const MatrixXd& sqrt_Cor_mat(void) const{ return sqrt_Cor_mat_; }

        const double& dx(void) const{ return dx_; }
        const double& dy(void) const{ return dy_; }

	};
}
#endif
