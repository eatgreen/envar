#ifndef GRADIENT
#define GRADIENT

#include <vector>
using Eigen::MatrixXd;

namespace EnVar
{
	std::vector<MatrixXd> gradient(MatrixXd& field, double dx, double dy)
	{
		std::vector<MatrixXd> grad;
		MatrixXd grad_x,grad_y;

		grad_x.resize(field.rows(),field.cols());
		grad_y.resize(field.rows(),field.cols());

		grad_x.block(0,1,field.rows()-2,field.cols()-2)=0.5*( filed.block(0,2,field.rows()-2,field.cols()-2)-filed.block(0,0,field.rows()-2,field.cols()-2) )/dx;
		grad_y.block(1,0,field.rows()-2,field.cols()-2)=0.5*( filed.block(2,0,field.rows()-2,field.cols()-2)-filed.block(0,0,field.rows()-2,field.cols()-2) )/dy;

		grad_x.col(0)=( filed.col(1)-filed.col(0) )/dx;
		grad_x.col(field.cols()-1)=( filed.col(field.cols()-1)-filed.col(field.cols()-2) )/dx;
		grad_y.row(1)=( filed.row(1)-filed.row(0) )/dy;
		grad_y.row(field.rows()-1)=( filed.row(field.rows()-1)-filed.row(field.rows()-2) )/dy;

		grad.push_back(grad_x);
		grad.push_back(grad_y);

		return grad;
	}
}

#endif