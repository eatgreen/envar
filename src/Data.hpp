#ifndef DATA
#define DATA

#include <Eigen/Core>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

namespace EnVar {

template<typename T>
class DataBase {
 private:
    std::vector<T> xens_;

 public:
    DataBase() {};
    ~DataBase() {};

    const std::vector<T>& data(void) const { return xens_; }
    const T&         at(const int) const;
    const VectorXd&  at(const int, const int) const;
    std::vector<unsigned int> size() const;
    typename std::vector<T>::const_iterator begin() const;
    typename std::vector<T>::const_iterator end() const;
    const T     front() const;
    const T     back() const;
    void  info() const;
    void  setDataSlice(const T&);
    void  setDataSlice(const T&&);
    void  setData(const std::vector<T>&);
    void  setDataMatrixXd(const MatrixXd&);
    void  setZero(const int, const int);
    void  setZero(const int, const int, const int);

    const DataBase  operator+(const DataBase&) const;
    const DataBase  operator+=(const DataBase&);
    const DataBase  operator+=(const DataBase&&);
    const DataBase  operator-(const DataBase&) const;
    const DataBase  operator*(const double) const;
    const DataBase  operator/(const double) const;
};

//! A member get the T data at index i

template<typename T>
inline const T& DataBase<T>::at(const int i) const {
    return this->data()[i];
}

//! A member get the VectorXd data at index i,j

template<typename T>
inline const VectorXd& DataBase<T>::at(const int i, const int j) const {
    std::cerr << "The type not supported" << endl;
    return {};
}

template<>
inline const VectorXd& DataBase<DataBase<VectorXd>>::at(const int i, const int j) const {
    return this->data()[i].data()[j];
}

//! A member get the begin iterator of datan

template<typename T>
typename std::vector<T>::const_iterator DataBase<T>::begin() const {
    return this->data().begin();
}

//! A member get the end iterator of data

template<typename T>
typename std::vector<T>::const_iterator DataBase<T>::end() const {
    return this->data().end();
}

//! A member get the front element of data

template<typename T>
const T DataBase<T>::front() const {
    return this->data().front();
}

//! A member get the back element of data

template<typename T>
const T DataBase<T>::back() const {
    return this->data().back();
}

//! A member get the size

template<typename T>
std::vector<unsigned int> DataBase<T>::size() const {
    std::cerr << "The type not supported!!!" << endl;
    return {};
}

template<>
inline std::vector<unsigned int> DataBase<VectorXd>::size() const {
    std::vector<unsigned int> v;
    v.push_back(this->data().size());
    v.push_back(this->at(0).size());
    return v;
}

template<>
inline std::vector<unsigned int> DataBase<MatrixXd>::size() const {
    std::vector<unsigned int> v;
    v.push_back(this->data().size());
    v.push_back(this->at(0).rows());
    v.push_back(this->at(0).cols());
    return v;
}

template<>
inline std::vector<unsigned int> DataBase<DataBase<VectorXd>>::size() const {
    std::vector<unsigned int> v, w;
    v.push_back(this->data().size());
    w = this->at(0).size();
    v.insert(v.end(), w.begin(), w.end());

    return v;
}

//! A member displaying the basic info

template<typename T>
void DataBase<T>::info() const {
    std::cerr << "The type not supported!!!" << endl;
}

template<>
inline void DataBase<VectorXd>::info() const {
    cout << "Data2D type" << endl;
    cout << "time sampling size,                 " << this->size()[0] << endl;
    cout << "state variable space sampling size, " << this->size()[1] << endl;
}

template<>
inline void DataBase<MatrixXd>::info() const {
    cout << "Data2D MatrixXd type" << endl;
    cout << "time sampling size,                      " << this->size()[0] << endl;
    cout << "state variable space x directional size, " << this->size()[1] << endl;
    cout << "state variable space y directional size, " << this->size()[2] << endl;
}

template<>
inline void DataBase<DataBase<VectorXd>>::info() const {
    cout << "Data3D type" << endl;
    cout << "control vector size,                " << this->size()[0] << endl;
    cout << "time sampling size,                 " << this->size()[1] << endl;
    cout << "state variable space sampling size, " << this->size()[2] << endl;
}

//! A member taking one std::vector<T> argument, set data.
/*!
    \param b is a const std::vector<T> argument
*/

template<typename T>
void DataBase<T>::setData(const std::vector<T>& b) {
    xens_ = b;
}

//! A member taking one MatrixXd argument, set data of Data2D .
/*!
    \param b is a const MatrixXd argument
*/

template<typename T>
void DataBase<T>::setDataMatrixXd(const MatrixXd& b) {
    std::cerr << "The type not supported!!!" << endl;
}

template<>
inline void DataBase<VectorXd>::setDataMatrixXd(const MatrixXd& b) {
    for (std::size_t i=0; i < (unsigned int)b.cols(); i++) {
        xens_.push_back( b.col(i) );
    }
}

//! A member taking two size arguments, set data to zero.
/*!
    \param stdvector_size is a const int argument, size of std::vector
    \param vectorxd_size is a const int argument, size of vectorxd as a member of std::vector
*/

template<typename T>
void DataBase<T>::setZero(const int stdvector_size, const int vectorxd_size ) {
    std::cerr << "The type not supported!!!" << endl;
}

template<>
inline void DataBase<VectorXd>::setZero(const int stdvector_size, const int vectorxd_size ) {
    for (std::size_t i = 0; i < (unsigned int)stdvector_size; i++) {
        xens_.push_back(VectorXd::Zero(vectorxd_size));
    }
}

//! A member taking three size arguments, set data to zero.
/*!
    \param stdvector_size is a const int argument, size of std::vector
    \param matrixxd_nx is a const int argument, size of matrixxd_nx as a member of std::vector
    \param matrixxd_ny is a const int argument, size of matrixxd_ny as a member of std::vector
*/

template<typename T>
void DataBase<T>::setZero(const int stdvector_size, const int matrixxd_nx, const int matrixxd_ny ) {
    std::cerr << "The type not supported!!!" << endl;
}

template<>
inline void DataBase<MatrixXd>::setZero(const int stdvector_size, const int matrixxd_nx, const int matrixxd_ny ) {
    for (std::size_t i = 0; i < (unsigned int)stdvector_size; i++) {
        xens_.push_back(MatrixXd::Zero(matrixxd_nx, matrixxd_ny));
    }
}

//! A member taking one T argument, set one slice of data.
/*!
    \param b is a const T argument
*/

template<typename T>
void DataBase<T>::setDataSlice(const T& b) {
    xens_.push_back(b);
}

template<typename T>
void DataBase<T>::setDataSlice(const T&& b) {
    this->setDataSlice(b);
}

// template<typename T>
// void DataBase<T>::operator = (const DataBase<T>& b) const
// {
//  xens_ = b.data();
// }

//! A member taking one T argument, overload operator +.
/*!
    \param b is a const T argument
    \return the sum
*/

template<typename T>
const DataBase<T> DataBase<T>::operator+(const DataBase<T>& b) const {
    DataBase<T> plus;
    std::vector<T> plus_xens;

    if (this->size()[0]!= b.size()[0]){
        std::cerr<<"left term dimension:"<<endl;
        this->info();
        std::cerr<<"right term dimension:"<<endl;
        b.info();
        std::cerr << "Data first dimenstion inconsistent" << endl;
        std::abort();
    }

    for (std::size_t i = 0; i < this->size()[0]; i++)
        plus_xens.push_back(this->at(i)+b.at(i));
    
    plus.setData(plus_xens);

    return plus;
}

//! A member taking one T argument, overload operator +=.
/*!
    \param b is a const T argument
    \return the added value in *this
*/

template <typename T>
const DataBase<T> DataBase<T>::operator+=(const DataBase<T>& b) {
    std::vector<T> plus_xens;

    if (this->size()[0]!= b.size()[0]){
        std::cerr<<"left term dimension:"<<endl;
        this->info();
        std::cerr<<"right term dimension:"<<endl;
        b.info();
        std::cerr << "Data first dimenstion inconsistent" << endl;
        std::abort();
    }

    for (std::size_t i = 0; i < this->size()[0]; i++) 
        plus_xens.push_back(this->at(i)+b.at(i));
    
    this->setData(plus_xens);

    return *this;
}

//! A member taking one T rvalue argument, overload operator +=.
/*!
    \param b is a const T rvalue argument
    \return the added value in *this
*/

template <typename T>
const DataBase<T> DataBase<T>::operator+=(const DataBase<T>&& b) {
    this->operator+=(b);
    return *this;
}

//! A member taking one T argument, overload operator -.
/*!
    \param b is a const T argument
    \return the difference
*/

template<typename T>
inline const DataBase<T> DataBase<T>::operator-(const DataBase<T>& b) const {
    DataBase<T> minus;
    std::vector<T> minus_xens;

    if (this->size()[0] != b.size()[0]) {
        std::cerr<<"In DataBase<T> operator-"<<endl;
        std::cerr<<"left term dimension:"<<endl;
        this->info();
        std::cerr<<"right term dimension:"<<endl;
        b.info();
        std::cerr << "Data first dimension inconsistent" << endl;
        std::abort();
    }

    for (std::size_t i=0; i < this->size()[0]; i++)
        minus_xens.push_back(this->at(i)-b.at(i));

    minus.setData(minus_xens);

    return minus;
}

//! A member taking one T argument, overload operator *.
/*!
    \param b is a const T argument
    \return the product
*/

template <typename T>
const DataBase<T> DataBase<T>::operator*(const double b) const {
    DataBase times;
    std::vector<T> times_xens;

    for (std::size_t i = 0; i < this->size()[0]; i++) {
        times_xens.push_back(this->at(i)*b);
    }
    times.setData(times_xens);

    return times;
}


//! A member taking one T argument, overload operator /.
/*!
    \param b is a const T argument
    \return the quotient
*/

template <typename T>
const DataBase<T> DataBase<T>::operator/(const double b) const {
    DataBase<T> divide;
    std::vector<VectorXd> divide_xens;

    for (std::size_t i = 0; i < this->size()[0]; i++) {
        divide_xens.push_back(this->at(i)/b);
    }
    divide.setData(divide_xens);

    return divide;
}

typedef DataBase<VectorXd> Data2D;
typedef DataBase<DataBase<VectorXd>> Data3D;

}  // namespace Envar
#endif
