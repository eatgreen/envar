#ifndef PSEUDO2DM
#define PSEUDO2DM

#include <iostream>
#include <vector>
#include "fftw3.h"
#include <assert.h>
#include "boost/random.hpp"
#include "boost/random/uniform_real_distribution.hpp"
#include <Eigen/Core>

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

namespace EnVar
{

	void pseudo2dM(MatrixXd&, int, int, int, double, double, double, double, int, int, double, int noise_const=1);

	void newton2D(double&, double&, int, int, double, double, double, double, bool& );

	void newtonfunc2D(double&, double&, double&, double&, double&, double&, double, double, int, int, double, double, double, double );
}

#endif