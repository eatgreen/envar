#ifndef NETCDFIO
#define NETCDFIO

#include <Eigen/Core>
#include <iostream>
#include <vector>
#include <netcdf>
#include <assert.h>
#include "Parameter.hpp"
#include "Model.hpp"
#include "../model/MpiDom.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

// using Data2D=EnVar::DataBase<VectorXd>;
// using Data3D=EnVar::DataBase<Data2D>;

using std::cout;
using std::endl;

namespace EnVar
{

    class NetCDFIO
    {
    private:

        std::string                 IO_path_;
        std::string                 file_name_;
        std::string                 file_name_bis_;
        netCDF::NcFile              dataFile_;
        int                         nx_loc_eff_;
        int                         ny_loc_eff_;
        netCDF::NcDim               dim_; 
        std::vector<netCDF::NcDim>  dims_;

    public:
        NetCDFIO(void) {}
        ~NetCDFIO(void) {}

        NetCDFIO(const std::string&, Parameter::IO_Domain_Type, MpiDom&);

        void                    Create_File();

        void                    Add_Vector_Dim(const VectorXd&);
        void                    Write_Vector(const VectorXd&, const std::string&);

        void                    Add_Matrix_Dim(const MatrixXd&);
        void                    Write_Matrix(const MatrixXd&, const std::string&);
        
        void                    Add_StateFrame_Dim(const std::vector<MatrixXd>&);
        void                    Write_StateFrame(const std::vector<MatrixXd>&, const std::vector<std::string>&);
        
        void                    Add_StateTrajectory_Dim(const Data2D& D2Data2D);
        void                    Write_StateTrajectory(const Data2D&, const std::vector<std::string>&);
        
        MatrixXd                Read_Matrix(const std::string&); 
        MatrixXd                Read_Matrix(const std::string&, MpiDom&, int);
        std::vector<MatrixXd>   Read_StateFrame(const std::vector<std::string>&);
        std::vector<MatrixXd>   Read_StateFrame(const std::vector<std::string>&, MpiDom&, int);
        Data2D                  Read_StateTrajectory(const std::vector<std::string>&);
        Data2D                  Read_StateTrajectory(const std::vector<std::string>&, MpiDom&, int);
        
        void                    Combine_Matrix(const std::string&, MpiDom&);
        void                    Combine_StateFrame(const std::vector<std::string>&, MpiDom&);
        void                    Combine_StateTrajectory(const std::vector<std::string>&, MpiDom&);

    };
}
#endif