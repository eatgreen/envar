#ifndef OUTPUT
#define OUTPUT

#include "Parameter.hpp"
#include "NetCDFIO.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

// using Data2D=EnVar::DataBase<VectorXd>;
// using Data3D=EnVar::DataBase<Data2D>;

namespace EnVar
{
	class Output
	{
	public: 
		Output(int CycleIndex, Data2D& Xobs_cycle, Data2D& Xb, Data2D& Xf, Data2D& Xa, MatrixXd& RMSXobsXt, MatrixXd& RMSXbXt, MatrixXd& RMSXaXt)
		{
			CycleIndex_=CycleIndex;
			Xobs_=Xobs_cycle;
			Xb_=Xb;
			Xf_=Xf;
			Xa_=Xa;
			RMSXobsXt_=RMSXobsXt;
			RMSXbXt_=RMSXbXt;
			RMSXaXt_=RMSXaXt;
		}

		void Write_to_File(MpiDom&) const;

	private:
		int 									CycleIndex_;
		Data2D 									Xobs_;
     	Data2D                                  Xb_;
        Data2D                                  Xf_;
        Data2D                                  Xa_;    
        MatrixXd 		                        RMSXobsXt_;
        MatrixXd 		                        RMSXbXt_;
        MatrixXd 		                        RMSXaXt_;

	public:
        const int& CycleIndex(void) const { return CycleIndex_; }
        const Data2D& Xobs(void) const{ return Xobs_;}
        const Data2D& Xb(void) const{ return Xb_;}
        const Data2D& Xf(void) const{ return Xf_;}
        const Data2D& Xa(void) const{ return Xa_;}
        const MatrixXd& RMSXobsXt(void) const{ return RMSXobsXt_;}
        const MatrixXd& RMSXbXt(void) const{ return RMSXbXt_;}
        const MatrixXd& RMSXaXt(void) const{ return RMSXaXt_;}

	};

	inline void Output::Write_to_File(MpiDom& mpi_obj) const{

		//write RMS
		if (mpi_obj.myrank()==mpi_obj.root()){

			NetCDFIO nc_rms_obs_file("RMS_obs", Parameter::Global, mpi_obj);
			NetCDFIO nc_rms_tru_file("RMS_tru", Parameter::Global, mpi_obj);

			nc_rms_obs_file.Create_File();
			nc_rms_obs_file.Add_Matrix_Dim(RMSXobsXt_);
			nc_rms_obs_file.Write_Matrix(RMSXobsXt_,"RMSXobsXt");
 
			nc_rms_tru_file.Create_File();
			nc_rms_tru_file.Add_Matrix_Dim(RMSXbXt_);
			nc_rms_tru_file.Write_Matrix(RMSXbXt_,"RMSXbXt");
			nc_rms_tru_file.Write_Matrix(RMSXaXt_,"RMSXaXt");

		}

		//write state
	}
}
#endif