#include "pseudo2dM.hpp"

namespace EnVar
{

	void pseudo2dM(MatrixXd& res, int nx, int ny, int lde, double rx, double ry, double dx, double dy, int n1, int n2, double theta, int noise_const)
	{
		double r1,r2,c;

		fftw_plan plan;
		int l,p,j,l_off,p_off;
		double kappa2,lambda2,kappa,lambda;
	   	double pi2,deltak,summ;
	    double a11tmp,a22tmp,a11,a22,a12,torad;
	    
	    double*   Amat;

	    static boost::mt19937 rng;
	    static boost::random::uniform_real_distribution<> nd(0,1);

	    const double   pi=3.141592653589;
	    bool   		   cnv;
	    double 		   e;

	    if(lde<1)      std::cerr << "pseudo2D: error lde < 1\n"<<endl;
    	if(rx <= 0.0)  std::cerr << "pseudo2D: error, rx <= 0.0\n"<<endl;
   		if(ry <= 0.0)  std::cerr << "pseudo2D: error, ry <= 0.0\n"<<endl;
   		if(n1 < nx)    std::cerr << "pseudo2D: n1 < nx\n"<<endl;
   		if(n2 < ny)    std::cerr << "pseudo2D: n2 < ny\n"<<endl;

	    double *fampl,*phi,*y;
	    fftw_complex *x;

        assert(res.rows()==nx*ny);
        assert(res.cols()==lde);

	    Amat  = (double*) malloc(nx*ny*lde*sizeof(double));

	    fampl = (double*) malloc(n1*(n2/2+1)*2*sizeof(double));
	    phi   = (double*) malloc(n1*(n2/2+1)*sizeof(double));
	    y     = (double*) malloc(n1*n2*sizeof(double));
	    x     = (fftw_complex*) fftw_malloc(n1*(n2/2+1)*sizeof(fftw_complex));

	    pi2=2.0*pi;
	    deltak=std::pow(pi2,2)/(double(n1*n2)*dx*dy);
   		kappa=pi2/(double(n1)*dx);
   		kappa2=std::pow(kappa,2);
   		lambda=pi2/(double(n2)*dy);
   		lambda2=std::pow(lambda,2);

   		if(y!=NULL)
   		{
   			free(y);
	   		y = (double*) malloc(n1*n2*sizeof(double));
   		} 

   		plan=fftw_plan_dft_c2r_2d(n1,n2,x,y,FFTW_ESTIMATE);

   		r1=3.0/rx;
   		r2=3.0/ry;

   		newton2D(r1,r2,n1,n2,dx,dy,rx,ry,cnv);

   		if (!cnv) 
   		{
      		std::cerr<<"newton did not converge"<<endl;
      		std::abort();
   		}

      	summ=0.0;
  		for(l=0;l<n1;l++){
  			l_off=l-n1/2;
      		for(p=0;p<n2;p++){
	  			p_off=p-n2/2;
			    summ=summ+std::exp(-2.0*(kappa2*double(l_off*l_off)/std::pow(r1,2) + lambda2*double(p_off*p_off)/std::pow(r2,2)));
      		}
      	}

      	summ--;

      	c=std::sqrt(1.0/(deltak*summ));

      	// Rotation to angle theta
	    a11tmp=1.0/std::pow(r1,2);
	    a22tmp=1.0/std::pow(r2,2);
	    torad=pi/180.0;
	    a11=a11tmp*std::pow(std::cos(theta*torad),2) + a22tmp*std::pow(std::sin(theta*torad),2);
	    a22=a11tmp*std::pow(std::sin(theta*torad),2) + a22tmp*std::pow(std::cos(theta*torad),2);
	    a12=(a22tmp-a11tmp)*std::cos(theta*torad)*std::sin(theta*torad);

	    for(j=0;j<lde;j++)
	    {
		  	// Calculating the random wave phases
		    for(l=0;l<n1+1;l++)
		    {
		    	for(p=0;p<n2/2+1;p++)
		    	{
		    		double temp=nd(rng);
				    phi[p+(n2/2+1)*l]=pi2*temp;
		    	}
		    }

		    // Calculating the wave amplitues
	        for(l=0;l<n1;l++)
	        {
	        	// if (l<n1/2+1)
	        	l_off=l;
	        	// else
	        	// l_off=l-n1;

			    for(p=0;p<n2/2+1;p++)
			    {
			    	p_off=p;

			        e=std::exp(-( a11*kappa2*double(l_off*l_off) + 2.0*a12*kappa*lambda*double(l_off*p_off) + a22*lambda2*double(p_off*p_off) ));

			        fampl[2*(p+(n2/2+1)*l)]  =e*std::cos(phi[p+(n2/2+1)*l])*std::sqrt(deltak)*c;
			        fampl[1+2*(p+(n2/2+1)*l)]=e*std::sin(phi[p+(n2/2+1)*l])*std::sqrt(deltak)*c;
			        
		    	}
			}
		    fampl[0]=0.0;
		    fampl[1]=0.0;

		    for(l=0;l<n1/2;l++)
		    {
		    	for(p=0;p<n2/2+1;p++) 
	        	{
	        		x[p+(n2/2+1)*l][0]=fampl[2*(p+(n2/2+1)*l)];
	        		x[p+(n2/2+1)*l][1]=fampl[1+2*(p+(n2/2+1)*l)];
	        	}
	        }

		    for(l=n1/2;l<n1;l++)
		    {
	  	        for(p=0;p<n2/2+1;p++) 
	  	        {
		        	x[p+(n2/2+1)*l][0]=fampl[2*(p+(n2/2+1)*l)];
		        	x[p+(n2/2+1)*l][1]=fampl[1+2*(p+(n2/2+1)*l)];
		        	//Symmetric this section by inverting the index 
		        	// x[p+(n2/2+1)*l][0]=fampl[2*(p+(n2/2+1)*(n1-1-l))];
		        	// x[p+(n2/2+1)*l][1]=fampl[1+2*(p+(n2/2+1)*(n1-1-l))];		        	
	  	        }
		    }
	   
      		fftw_execute(plan);

	      	for(l=0;l<nx;l++)
	      	{
	      		for(p=0;p<ny;p++)
	      		{
		         	// Amat[j+lde*(p+ny*l)]=y[p+n2*l];
		         	Amat[p+ny*l+nx*ny*j]=y[p+n2*l];
	      		}
	      	}

        }

        Eigen::Map<MatrixXd> Amat_eigen(Amat,nx*ny,lde);

 	    fftw_destroy_plan(plan);

		free ( fampl );
 	    free ( phi );
  		free ( y );
  		fftw_free ( x );

  		res=Amat_eigen*noise_const;
 	    // return res;
	}

	void newton2D(double& r1, double& r2, int n1, int n2, double dx, double dy, double rx, double ry, bool& lconv)
	{
		const double eps=1e-5;
		double inc1,inc2,err1,err2;
	    int i,j;
	    double f,g,f1,g1,f2,g2;

	    double r1ini,r2ini,gamma;

	    r1ini=r1;
	    r2ini=r2;
	    gamma=1.25;

	    lconv=false;

	    for(j=1;j<=10;j++){
	    	r1=double(j)*r1ini;
	    	r2=double(j)*r2ini;
      		gamma=gamma-0.25/double(j);

      		for(i=0;i<100;i++){

      			newtonfunc2D(f,g,f1,g1,f2,g2,r1,r2,n1,n2,dx,dy,rx,ry);

		        inc1 =  (f*g2-f2*g)/(f1*g2-f2*g1);
		        inc2 =  (f1*g-f*g1)/(f1*g2-f2*g1);

		        r1 = r1 - gamma*inc1;
		        r2 = r2 - gamma*inc2;

		        r1=std::max(r1,0.000001);
		        r2=std::max(r2,0.000001);

		        r1=std::min(r1,1.0);
		        r2=std::min(r2,1.0);

		        err1 = inc1/(std::abs(r1)+eps);
		        err2 = inc2/(std::abs(r2)+eps);
      		
	      		if(std::abs(err1)+std::abs(err2) < eps)
	      		{
	      			lconv=true;
	      			break;
	      		}	
	      	}

	      	if (lconv) break;

	    }

	    if (!lconv) 
	    {
	    	cout<<"Newton did not converge."<<endl;
	        cout<<"Probably an error in input parameters."<<endl;
	        cout<<"Is the dx resolving the rx scale?"<<endl;
	    }

	}

	void newtonfunc2D(double& f,double& g,double& f1,double& g1,double& f2,double& g2,double r1,double r2,int n1,int n2,double dx,double dy,double rx,double ry)
	{
		const double pi=3.141592653589;

		int l,p,l_off,p_off;
   		double pi2,e,kappa,kappa2,lambda,lambda2;

   		pi2=2.0*pi;

	    kappa=pi2/(double(n1)*dx);
	    kappa2=std::pow(kappa,2);

	    lambda=pi2/(double(n2)*dy);
	    lambda2=std::pow(lambda,2);

	    f=0.0; g=0.0; f1=0.0; g1=0.0; f2=0.0; g2=0.0;

    	for(l=0;l<(n1/2*2);l++)
    	{
    		l_off=l-n1/2+1;
		    for(p=0;p<(n2/2*2);p++)
		    {
	    		p_off=p-n2/2+1;

	    		if(p==n2/2-1 && l==n1/2-1) continue;

	 	   		e=std::exp( -2.0*( kappa2*double(l_off*l_off)/std::pow(r1,2) + lambda2*double(p_off*p_off)/std::pow(r2,2) ) );

	    		f=f + e * ( cos(kappa *double(l_off)*rx) - std::exp(-1.0) );
      			g=g + e * ( cos(lambda*double(p_off)*ry) - std::exp(-1.0) );

			    f1=f1 + e * (4.0*kappa2 *double(l_off*l_off)/std::pow(r1,3)) * ( std::cos(kappa *double(l_off)*rx) - std::exp(-1.0) );
			    g1=g1 + e * (4.0*kappa2 *double(l_off*l_off)/std::pow(r1,3)) * ( std::cos(lambda*double(p_off)*ry) - std::exp(-1.0) );
			    f2=f2 + e * (4.0*lambda2*double(p_off*p_off)/std::pow(r2,3)) * ( std::cos(kappa *double(l_off)*rx) - std::exp(-1.0) );
	  	        g2=g2 + e * (4.0*lambda2*double(p_off*p_off)/std::pow(r2,3)) * ( std::cos(lambda*double(p_off)*ry) - std::exp(-1.0) );

	    	}
	    }

	}
}
