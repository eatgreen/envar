#include "NetCDFIO.hpp"

namespace EnVar
{

    NetCDFIO::NetCDFIO(const std::string& file_name, Parameter::IO_Domain_Type IO_Domain_type, MpiDom& mpi_obj){
        
        IO_path_=std::string(std::getenv("ENV_PATH"))+"data/";

        file_name_=file_name;

        if (IO_Domain_type == Parameter::Global){
            if (mpi_obj.myrank()==mpi_obj.root()){
                file_name_bis_=IO_path_+file_name_+std::to_string(mpi_obj.proc_ni())+"x"+std::to_string(mpi_obj.proc_nj())+".nc";
                nx_loc_eff_=mpi_obj.nx_glo();
                ny_loc_eff_=mpi_obj.ny_glo();
            }
        }
        else if (IO_Domain_type == Parameter::Local){
            file_name_bis_=IO_path_+file_name_+std::to_string(mpi_obj.proc_ni())+"x"+std::to_string(mpi_obj.proc_nj())+std::to_string(mpi_obj.myrank())+".nc";
            nx_loc_eff_=mpi_obj.nx_loc();
            ny_loc_eff_=mpi_obj.ny_loc();
        }

        // if (mpi_obj.myrank()==mpi_obj.root())
            cout<<"NetCDFIO::NetCDFIO "<<file_name_bis_<<endl;
    }

    void NetCDFIO::Create_File(){
        
        dataFile_.open(file_name_bis_, netCDF::NcFile::replace);

    }

    void NetCDFIO::Add_Vector_Dim(const VectorXd& Vec){

        dim_ = dataFile_.addDim("t", Vec.size());

    }

    void NetCDFIO::Write_Vector(const VectorXd& Vec, const std::string& var_name){

        netCDF::NcVar var = dataFile_.addVar(var_name, netCDF::ncDouble, dim_);
        var.putVar(Vec.data());

    }

    void NetCDFIO::Add_Matrix_Dim(const MatrixXd& D2Mat){

        netCDF::NcDim xDim = dataFile_.addDim("x", D2Mat.cols());
        netCDF::NcDim yDim = dataFile_.addDim("y", D2Mat.rows());

        dims_.push_back(xDim);
        dims_.push_back(yDim);

    }

    void NetCDFIO::Write_Matrix(const MatrixXd& D2Mat, const std::string& var_name){

        netCDF::NcVar var = dataFile_.addVar(var_name, netCDF::ncDouble, dims_);
        var.putVar(D2Mat.data());

    }

    MatrixXd NetCDFIO::Read_Matrix(const std::string& var_name){

        MatrixXd    D2Mat;

        dataFile_.open(file_name_bis_, netCDF::NcFile::read);

        netCDF::NcVar var=dataFile_.getVar(var_name);

        //note that the file needed to be transposed back (cols,rows)
        D2Mat.resize(var.getDim(1).getSize(),var.getDim(0).getSize());

        var.getVar(D2Mat.data());

        dataFile_.close();

        return D2Mat;
    }

    MatrixXd NetCDFIO::Read_Matrix(const std::string& var_name, MpiDom& mpi_obj, int rank_index){

        MatrixXd    D2Mat;
        std::string file_name_bis_onroot_;

        assert(mpi_obj.myrank()==mpi_obj.root());

        file_name_bis_onroot_=IO_path_+file_name_+std::to_string(mpi_obj.proc_ni())+"x"+std::to_string(mpi_obj.proc_nj())+std::to_string(rank_index)+".nc";

        netCDF::NcFile dataFile_local(file_name_bis_onroot_, netCDF::NcFile::read);

        netCDF::NcVar var=dataFile_local.getVar(var_name);

        //note that the file needed to be transposed back (cols,rows)
        D2Mat.resize(var.getDim(1).getSize(),var.getDim(0).getSize());

        var.getVar(D2Mat.data());

        dataFile_local.close();

        return D2Mat;
    }

    void  NetCDFIO::Combine_Matrix(const std::string& var_name, MpiDom& mpi_obj){

        MatrixXd                D2Mat_read, D2Mat_glo;

        if (mpi_obj.myrank()==mpi_obj.root()){

            D2Mat_glo.resize(mpi_obj.nx_glo(),mpi_obj.ny_glo());

            for (std::size_t i=0; i<(unsigned int)mpi_obj.mysize(); i++){

                // cout<<"D2Mat_glo.rows()  "<<D2Mat_glo.rows()<<" D2Mat_glo.cols() "<<D2Mat_glo.cols()<<endl;
                // cout<<"file_name  "<<file_name<<endl;

                D2Mat_read=Read_Matrix(var_name,mpi_obj,i);

                // cout<<"D2Mat_read.rows() "<<D2Mat_read.rows()<<" D2Mat_read.cols() "<<D2Mat_read.cols()<<endl;
                // cout<<"mpi_obj.nx_loc_vec().at(i) "<<mpi_obj.nx_loc_vec().at(i)<<" mpi_obj.ny_loc_vec().at(i) "<<mpi_obj.ny_loc_vec().at(i)<<endl;

                D2Mat_glo.block(mpi_obj.ix_loc_vec().at(i),mpi_obj.iy_loc_vec().at(i),mpi_obj.nx_loc_vec().at(i),mpi_obj.ny_loc_vec().at(i))=D2Mat_read;     
            }

            Write_Matrix(D2Mat_glo,var_name);
        }
    }

    void NetCDFIO::Add_StateFrame_Dim(const std::vector<MatrixXd>& D3Mat){
        
        netCDF::NcDim xDim = dataFile_.addDim("x", D3Mat.at(0).cols());
        netCDF::NcDim yDim = dataFile_.addDim("y", D3Mat.at(0).rows());

        dims_.push_back(xDim);
        dims_.push_back(yDim);
    }

    void NetCDFIO::Write_StateFrame(const std::vector<MatrixXd>& D3Mat, const std::vector<std::string>& var_name){

        netCDF::NcVar var0 = dataFile_.addVar(var_name.at(0), netCDF::ncDouble, dims_);
        var0.putVar(D3Mat.at(0).data());
        netCDF::NcVar var1 = dataFile_.addVar(var_name.at(1), netCDF::ncDouble, dims_);
        var1.putVar(D3Mat.at(1).data());
        netCDF::NcVar var2 = dataFile_.addVar(var_name.at(2), netCDF::ncDouble, dims_);
        var2.putVar(D3Mat.at(2).data());

    }

    std::vector<MatrixXd>  NetCDFIO::Read_StateFrame(const std::vector<std::string>& var_name){

        std::vector<MatrixXd>   D3Mat;
        MatrixXd                D2Mat;

        dataFile_.open(file_name_bis_, netCDF::NcFile::read);


        netCDF::NcVar var0 = dataFile_.getVar(var_name.at(0));
        D2Mat.resize(var0.getDim(1).getSize(),var0.getDim(0).getSize());


        var0.getVar(D2Mat.data());
        D3Mat.push_back(D2Mat);

        netCDF::NcVar var1 = dataFile_.getVar(var_name.at(1));
        var1.getVar(D2Mat.data());
        D3Mat.push_back(D2Mat);

        netCDF::NcVar var2 = dataFile_.getVar(var_name.at(2));
        var2.getVar(D2Mat.data());
        D3Mat.push_back(D2Mat);

        dataFile_.close();

        return D3Mat;
    }

    std::vector<MatrixXd>  NetCDFIO::Read_StateFrame(const std::vector<std::string>& var_name, MpiDom& mpi_obj, int rank_index){

        std::vector<MatrixXd>   D3Mat;
        std::string file_name_bis_onroot_;
        MatrixXd                D2Mat;

        file_name_bis_onroot_=IO_path_+file_name_+std::to_string(mpi_obj.proc_ni())+"x"+std::to_string(mpi_obj.proc_nj())+std::to_string(rank_index)+".nc";

        // cout<<"mpi_obj.myrank() "<<mpi_obj.myrank()<<" file_name_bis_onroot_ "<<file_name_bis_onroot_<<endl;

        netCDF::NcFile dataFile_local(file_name_bis_onroot_, netCDF::NcFile::read);

        netCDF::NcVar var0 = dataFile_local.getVar(var_name.at(0));
        D2Mat.resize(var0.getDim(1).getSize(),var0.getDim(0).getSize());

        var0.getVar(D2Mat.data());
        D3Mat.push_back(D2Mat);

        netCDF::NcVar var1 = dataFile_local.getVar(var_name.at(1));
        var1.getVar(D2Mat.data());
        D3Mat.push_back(D2Mat);

        netCDF::NcVar var2 = dataFile_local.getVar(var_name.at(2));
        var2.getVar(D2Mat.data());
        D3Mat.push_back(D2Mat);

        dataFile_local.close();

        return D3Mat;
    }

    void NetCDFIO::Add_StateTrajectory_Dim(const Data2D& D2Data2D){

        netCDF::NcDim xDim = dataFile_.addDim("x", ny_loc_eff_);
        netCDF::NcDim yDim = dataFile_.addDim("y", nx_loc_eff_);
        netCDF::NcDim tDim = dataFile_.addDim("t", D2Data2D.size().at(0));

        dims_.push_back(tDim);
        dims_.push_back(xDim);
        dims_.push_back(yDim);
    }

    void NetCDFIO::Write_StateTrajectory(const Data2D& D2Data2D, const std::vector<std::string>& var_name){

        std::string file_name_bis;
        MatrixXd h,u,v;

        // cout<<"D2Data2D.at(0).size() "<<D2Data2D.at(0).size()<<endl;
        // cout<<"mpi_obj.nx()*mpi_obj.ny()*mpi_obj.nState() "<<mpi_obj.nx()*mpi_obj.ny()*mpi_obj.nState()<<endl;

        std::size_t rec=0;
        std::vector<std::size_t> startp,countp;
        startp.push_back(0);
        startp.push_back(0);
        startp.push_back(0);
        countp.push_back(1);
        countp.push_back(ny_loc_eff_);
        countp.push_back(nx_loc_eff_);

        netCDF::NcVar var0 = dataFile_.addVar(var_name.at(0), netCDF::ncDouble, dims_);
        netCDF::NcVar var1 = dataFile_.addVar(var_name.at(1), netCDF::ncDouble, dims_);
        netCDF::NcVar var2 = dataFile_.addVar(var_name.at(2), netCDF::ncDouble, dims_);

        for (auto T_iter = D2Data2D.begin(); T_iter!=D2Data2D.end(); ++T_iter){

            startp[0]=rec;

            h=get_h( *T_iter, const_cast<int&>(nx_loc_eff_), const_cast<int&>(ny_loc_eff_) );
            u=get_u( *T_iter, const_cast<int&>(nx_loc_eff_), const_cast<int&>(ny_loc_eff_) );
            v=get_v( *T_iter, const_cast<int&>(nx_loc_eff_), const_cast<int&>(ny_loc_eff_) );

            var0.putVar(startp,countp,h.data());
            var1.putVar(startp,countp,u.data());
            var2.putVar(startp,countp,v.data());

            rec++;
        }

    }

    Data2D NetCDFIO::Read_StateTrajectory(const std::vector<std::string>& var_name){

        Data2D D2Data2D;
        MatrixXd h,u,v;
        VectorXd vec_ctl;

        dataFile_.open(file_name_bis_, netCDF::NcFile::read);

        netCDF::NcVar var0 = dataFile_.getVar(var_name.at(0));
        netCDF::NcVar var1 = dataFile_.getVar(var_name.at(1));
        netCDF::NcVar var2 = dataFile_.getVar(var_name.at(2));

        std::vector<std::size_t> startp,countp;
        startp.push_back(0);
        startp.push_back(0);
        startp.push_back(0);
        countp.push_back(1);
        countp.push_back(var0.getDim(1).getSize());
        countp.push_back(var0.getDim(2).getSize());

        h.resize(var0.getDim(2).getSize(),var0.getDim(1).getSize());
        u.resize(var0.getDim(2).getSize(),var0.getDim(1).getSize());
        v.resize(var0.getDim(2).getSize(),var0.getDim(1).getSize());

        for(std::size_t rec=0; rec < var0.getDim(0).getSize(); rec++){

            startp[0]=rec;

            var0.getVar(startp,countp,h.data());
            var1.getVar(startp,countp,u.data());
            var2.getVar(startp,countp,v.data());

            vec_ctl=get_ctl_vector(h,u,v);
            D2Data2D.setDataSlice(vec_ctl);

        }

        dataFile_.close();

        return D2Data2D;
    }

    Data2D NetCDFIO::Read_StateTrajectory(const std::vector<std::string>& var_name, MpiDom& mpi_obj, int rank_index){

        Data2D D2Data2D;
        std::string file_name_bis_onroot_;
        MatrixXd h,u,v;
        VectorXd vec_ctl;

        file_name_bis_onroot_=IO_path_+file_name_+std::to_string(mpi_obj.proc_ni())+"x"+std::to_string(mpi_obj.proc_nj())+std::to_string(rank_index)+".nc";

        netCDF::NcFile dataFile(file_name_bis_onroot_, netCDF::NcFile::read);

        netCDF::NcVar var0 = dataFile.getVar(var_name.at(0));
        netCDF::NcVar var1 = dataFile.getVar(var_name.at(1));
        netCDF::NcVar var2 = dataFile.getVar(var_name.at(2));

        // if (mpi_obj.myrank()==mpi_obj.root()){
        //     cout<<"var0.getDim(0).getSize() "<<var0.getDim(0).getSize()<<endl;
        //     cout<<"var0.getDim(1).getSize() "<<var0.getDim(1).getSize()<<endl;
        //     cout<<"var0.getDim(2).getSize() "<<var0.getDim(2).getSize()<<endl;
        // }

        std::vector<std::size_t> startp,countp;
        startp.push_back(0);
        startp.push_back(0);
        startp.push_back(0);
        countp.push_back(1);
        countp.push_back(var0.getDim(1).getSize());
        countp.push_back(var0.getDim(2).getSize());

        // if (mpi_obj.myrank()==mpi_obj.root()){
        //     cout<<"startp.at(0) "<<startp.at(0)<<endl;
        //     cout<<"startp.at(1) "<<startp.at(1)<<endl;
        //     cout<<"startp.at(2) "<<startp.at(2)<<endl;
        //     cout<<"countp.at(0) "<<countp.at(0)<<endl;
        //     cout<<"countp.at(1) "<<countp.at(1)<<endl;
        //     cout<<"countp.at(2) "<<countp.at(2)<<endl;
        // }

        h.resize(var0.getDim(2).getSize(),var0.getDim(1).getSize());
        u.resize(var0.getDim(2).getSize(),var0.getDim(1).getSize());
        v.resize(var0.getDim(2).getSize(),var0.getDim(1).getSize());

        for(std::size_t rec=0; rec < var0.getDim(0).getSize(); rec++){

            startp[0]=rec;

            var0.getVar(startp,countp,h.data());
            var1.getVar(startp,countp,u.data());
            var2.getVar(startp,countp,v.data());

            vec_ctl=get_ctl_vector(h,u,v);
            D2Data2D.setDataSlice(vec_ctl);

        }

        dataFile.close();

        return D2Data2D;
    }

    void  NetCDFIO::Combine_StateFrame(const std::vector<std::string>& var_name, MpiDom& mpi_obj){

        std::vector<MatrixXd>   D3Mat_glo,D3Mat_read;
        MatrixXd                D2Mat_h_glo,D2Mat_u_glo,D2Mat_v_glo;

        if (mpi_obj.myrank()==mpi_obj.root()){

            D2Mat_h_glo.resize(mpi_obj.nx_glo(),mpi_obj.ny_glo());
            D2Mat_u_glo.resize(mpi_obj.nx_glo(),mpi_obj.ny_glo());
            D2Mat_v_glo.resize(mpi_obj.nx_glo(),mpi_obj.ny_glo());

            for (std::size_t i=0; i<(unsigned int)mpi_obj.mysize(); i++){
                D3Mat_read=Read_StateFrame(var_name,mpi_obj,i);

                D2Mat_h_glo.block(mpi_obj.ix_loc_vec().at(i),mpi_obj.iy_loc_vec().at(i),mpi_obj.nx_loc_vec().at(i),mpi_obj.ny_loc_vec().at(i))=D3Mat_read.at(0);
        
                D2Mat_u_glo.block(mpi_obj.ix_loc_vec().at(i),mpi_obj.iy_loc_vec().at(i),mpi_obj.nx_loc_vec().at(i),mpi_obj.ny_loc_vec().at(i))=D3Mat_read.at(1);                   

                D2Mat_v_glo.block(mpi_obj.ix_loc_vec().at(i),mpi_obj.iy_loc_vec().at(i),mpi_obj.nx_loc_vec().at(i),mpi_obj.ny_loc_vec().at(i))=D3Mat_read.at(2);
            
            }

            D3Mat_glo.push_back(D2Mat_h_glo);
            D3Mat_glo.push_back(D2Mat_u_glo);
            D3Mat_glo.push_back(D2Mat_v_glo);

            Write_StateFrame(D3Mat_glo,var_name);

        }
    }

    void NetCDFIO::Combine_StateTrajectory(const std::vector<std::string>& var_name, MpiDom& mpi_obj){

        Data2D                      D2Data2D_glo,D2Data2D_read;
        std::vector<MatrixXd>       D3Mat_h_glo,D3Mat_u_glo,D3Mat_v_glo;
        MatrixXd                    D2Mat_h,D2Mat_u,D2Mat_v;
        VectorXd                    vec_ctl;        
        int                         rec;

        if (mpi_obj.myrank()==mpi_obj.root()){

            D2Data2D_read=Read_StateTrajectory(var_name,mpi_obj,0);

            // D3Mat_h_glo.setZero(D2Data2D_read.size().at(0), mpi_obj.nx(), mpi_obj.ny());
            // D3Mat_u_glo.setZero(D2Data2D_read.size().at(0), mpi_obj.nx(), mpi_obj.ny());
            // D3Mat_v_glo.setZero(D2Data2D_read.size().at(0), mpi_obj.nx(), mpi_obj.ny());

            for (auto T_iter = D2Data2D_read.begin(); T_iter!=D2Data2D_read.end(); ++T_iter){
                D3Mat_h_glo.push_back( MatrixXd::Zero(mpi_obj.nx_glo(),mpi_obj.ny_glo()) );
                D3Mat_u_glo.push_back( MatrixXd::Zero(mpi_obj.nx_glo(),mpi_obj.ny_glo()) );
                D3Mat_v_glo.push_back( MatrixXd::Zero(mpi_obj.nx_glo(),mpi_obj.ny_glo()) );
            }

            for (std::size_t i=0; i<(unsigned int)mpi_obj.mysize(); i++){

                D2Data2D_read=Read_StateTrajectory(var_name,mpi_obj,i);

                rec=0;

                for (auto T_iter = D2Data2D_read.begin(); T_iter!=D2Data2D_read.end(); ++T_iter){

                    D2Mat_h=get_h(*T_iter, const_cast<int&>(mpi_obj.nx_loc_vec().at(i)), const_cast<int&>(mpi_obj.ny_loc_vec().at(i)));
                    D2Mat_u=get_u(*T_iter, const_cast<int&>(mpi_obj.nx_loc_vec().at(i)), const_cast<int&>(mpi_obj.ny_loc_vec().at(i)));
                    D2Mat_v=get_v(*T_iter, const_cast<int&>(mpi_obj.nx_loc_vec().at(i)), const_cast<int&>(mpi_obj.ny_loc_vec().at(i)));

                    D3Mat_h_glo.at(rec).block(mpi_obj.ix_loc_vec().at(i),mpi_obj.iy_loc_vec().at(i),mpi_obj.nx_loc_vec().at(i),mpi_obj.ny_loc_vec().at(i))=D2Mat_h;
                    D3Mat_u_glo.at(rec).block(mpi_obj.ix_loc_vec().at(i),mpi_obj.iy_loc_vec().at(i),mpi_obj.nx_loc_vec().at(i),mpi_obj.ny_loc_vec().at(i))=D2Mat_u;
                    D3Mat_v_glo.at(rec).block(mpi_obj.ix_loc_vec().at(i),mpi_obj.iy_loc_vec().at(i),mpi_obj.nx_loc_vec().at(i),mpi_obj.ny_loc_vec().at(i))=D2Mat_v;

                    rec++;
                }

                assert( (unsigned int)rec==D2Data2D_read.size().at(0) );
            }

            for (std::size_t t=0; t<D2Data2D_read.size().at(0); t++){
                vec_ctl=get_ctl_vector(D3Mat_h_glo.at(t), D3Mat_u_glo.at(t), D3Mat_v_glo.at(t));
                D2Data2D_glo.setDataSlice(vec_ctl);
            }

            Write_StateTrajectory(D2Data2D_glo,var_name);

        }

    }

}