#include "Observation.hpp"

namespace EnVar
{

	Observation::Observation(void){	
	}

	Observation::Observation(Parameter& par_obj, MpiDom& obs_mpidom_obj){
	    obsreal_flag_=par_obj.obsreal_flag();
		obsreal_path_=par_obj.obsreal_path();

		codbkg_=par_obj.codENS();
		nobsSize_=obs_mpidom_obj.nx_loc()*obs_mpidom_obj.ny_loc();
		nobsState_=par_obj.nobsState();
		nState_=obs_mpidom_obj.nState();
	}

	Observation::~Observation(void){	
	}

	void Observation::make_true_initialcondition(DataAssim& das_obj, ModelPara& synthobs_modpar_obj, MpiDom& synthobs_mpidom_obj){
	    if (synthobs_mpidom_obj.myrank()==synthobs_mpidom_obj.root())
			cout<<"Observation::make_true_initialcondition for synthetic obs"<<endl;

		MatrixXd h0_glo,u0_glo,v0_glo;
		VectorXd x0_glo;
		h0_glo.setZero(synthobs_mpidom_obj.nx_glo(),synthobs_mpidom_obj.ny_glo());
		u0_glo.setZero(synthobs_mpidom_obj.nx_glo(),synthobs_mpidom_obj.ny_glo());
		v0_glo.setZero(synthobs_mpidom_obj.nx_glo(),synthobs_mpidom_obj.ny_glo());

		if (synthobs_mpidom_obj.myrank()==synthobs_mpidom_obj.root()){
			//make noise
	        bkgnoise_.resize(synthobs_mpidom_obj.nSize_glo(),1);
	        //use pseudo2dM
	        pseudo2dM(bkgnoise_,synthobs_mpidom_obj.ny_glo(),synthobs_mpidom_obj.nx_glo(),1,codbkg_,codbkg_,1,1,synthobs_mpidom_obj.ny_glo(),synthobs_mpidom_obj.nx_glo(),0);
	        Eigen::Map<MatrixXd> bkgnoise_h(bkgnoise_.data(),synthobs_mpidom_obj.nx_glo(),synthobs_mpidom_obj.ny_glo());

#ifdef DEBUG
            NetCDFIO bkgnoise_file("bkg_noise", Parameter::Global, synthobs_mpidom_obj);
            bkgnoise_file.Create_File();
            std::string var("h");

            bkgnoise_file.Add_Matrix_Dim(bkgnoise_h);
            bkgnoise_file.Write_Matrix(bkgnoise_h,var);
#endif

	        h0_glo=5000.0*MatrixXd::Ones(synthobs_mpidom_obj.nx_glo(),synthobs_mpidom_obj.ny_glo())-4.0e-4/synthobs_modpar_obj.g()*synthobs_mpidom_obj.xx_glo()+das_obj.Err_hb()*das_obj.Err_beta()*bkgnoise_h;
	        u0_glo.setZero(synthobs_mpidom_obj.nx_glo(),synthobs_mpidom_obj.ny_glo());
	        v0_glo.setZero(synthobs_mpidom_obj.nx_glo(),synthobs_mpidom_obj.ny_glo());
			x0_glo=get_ctl_vector(h0_glo, u0_glo, v0_glo);
		}

#if defined(MPI) || defined(MPI_simple)
    	//Bcast to each processor, then work on each copy
    	//Or Alltoallw scatter different size to each processor
		MPI_Bcast(h0_glo.data(), synthobs_mpidom_obj.nSize_glo(), MPI_DOUBLE, synthobs_mpidom_obj.root(), MPI_COMM_WORLD);
		MPI_Bcast(u0_glo.data(), synthobs_mpidom_obj.nSize_glo(), MPI_DOUBLE, synthobs_mpidom_obj.root(), MPI_COMM_WORLD);
		MPI_Bcast(v0_glo.data(), synthobs_mpidom_obj.nSize_glo(), MPI_DOUBLE, synthobs_mpidom_obj.root(), MPI_COMM_WORLD);

		h0_=h0_glo.block(synthobs_mpidom_obj.ix_loc(), synthobs_mpidom_obj.iy_loc(), synthobs_mpidom_obj.nx_loc(), synthobs_mpidom_obj.ny_loc());
		u0_=u0_glo.block(synthobs_mpidom_obj.ix_loc(), synthobs_mpidom_obj.iy_loc(), synthobs_mpidom_obj.nx_loc(), synthobs_mpidom_obj.ny_loc());
		v0_=v0_glo.block(synthobs_mpidom_obj.ix_loc(), synthobs_mpidom_obj.iy_loc(), synthobs_mpidom_obj.nx_loc(), synthobs_mpidom_obj.ny_loc());
		x0_=get_ctl_vector(h0_, u0_, v0_);
#else
		h0_=h0_glo;
		u0_=u0_glo;
		v0_=v0_glo;
		x0_=x0_glo;
#endif
	}

	void Observation::make_true_solution(DataAssim& das_obj, Model& synthmod_obj, MpiDom& synthobs_mpidom_obj){
	    if (synthobs_mpidom_obj.myrank()==synthobs_mpidom_obj.root())
			cout<<"Observation::make_true_solution for synthetic obs"<<endl;

		Xt_=runmodel_frame(x0_, das_obj.t_init(), das_obj.tmax(), synthmod_obj, synthobs_mpidom_obj);

#ifdef DEBUG
#if defined(MPI) || defined(MPI_simple)
		Parameter::IO_Domain_Type Domain_flag=Parameter::Local;
#else
		Parameter::IO_Domain_Type Domain_flag=Parameter::Global;
#endif
    	NetCDFIO Xt_file("Xt", Domain_flag, synthobs_mpidom_obj);
    	Xt_file.Create_File();
    	std::vector<std::string> var_name({"h","u","v"});
    
	    Xt_file.Add_StateTrajectory_Dim(Xt_);
        Xt_file.Write_StateTrajectory(Xt_,var_name);
#endif
	}

	void Observation::make_obs(DataAssim& das_obj, MpiDom& obs_mpidom_obj, ModelPara* synthobs_modpar_obj_ptr){
	    if (obs_mpidom_obj.myrank()==obs_mpidom_obj.root())
			cout<<"Observation::make_obs make observations"<<endl;

		if(obsreal_flag_)
		{
			//read obs from file
		}
		else
		{
			VectorXd dxobs;
			dxobs.setZero(nobsSize_*nState_);
			VectorXd dxobs_glo;
			dxobs_glo.setZero(obs_mpidom_obj.nSize_glo()*nState_);
			MatrixXd h_glo,u_glo,v_glo,h_loc,u_loc,v_loc;

			Data2D Xobs_All;
			auto Xt_iter=Xt_.begin();
			for(std::size_t i=0; i<das_obj.itobs_sum(); i++)
			{
				//TODO: global noise send to slaves
				if (obs_mpidom_obj.myrank()==obs_mpidom_obj.root()){
					//use NormalRandomMatrix to produce i.i.d. noise
					dxobs_glo.head(obs_mpidom_obj.nSize_glo())=NormalRandomMatrix(obs_mpidom_obj.nSize_glo(), 1, 0, das_obj.Err_ho(), obs_mpidom_obj.myrank());
					dxobs_glo.segment(obs_mpidom_obj.nSize_glo(),obs_mpidom_obj.nSize_glo())=NormalRandomMatrix(obs_mpidom_obj.nSize_glo(), 1, 0, das_obj.Err_uo(), obs_mpidom_obj.myrank());
					dxobs_glo.tail(obs_mpidom_obj.nSize_glo())=NormalRandomMatrix(obs_mpidom_obj.nSize_glo(), 1, 0, das_obj.Err_vo(), obs_mpidom_obj.myrank());
#ifdef DEBUG

			        NetCDFIO dxobs_file("dxobs"+std::to_string(i), Parameter::Global, obs_mpidom_obj);
			        dxobs_file.Create_File();

			        dxobs_file.Add_Vector_Dim(dxobs_glo.segment(obs_mpidom_obj.nSize_glo(),obs_mpidom_obj.nSize_glo()));
			        dxobs_file.Write_Vector(dxobs_glo,"u");

			        // dxobs_file.Add_StateFrame_Dim(dxobs_glo_huv);
			        // dxobs_file.Write_StateFrame(dxobs_glo_huv,var_name);

			        // NetCDFIO dxobs_file("dxobs"+std::to_string(i), Parameter::Global, obs_mpidom_obj);
			        // dxobs_file.Create_File();
			        // std::vector<std::string> var_name({"h"+std::to_string(i),"u"+std::to_string(i),"v"+std::to_string(i)});
			        // std::vector<MatrixXd>    dxobs_glo_huv;

			        // for (std::size_t i=0; i<(unsigned int)nState_; i++)
			        // 	dxobs_glo_huv.push_back(Eigen::Map<MatrixXd>(dxobs_glo.data()+i*nobsSize_,obs_mpidom_obj.nx_glo(),obs_mpidom_obj.ny_glo()));

			        // dxobs_file.Add_StateFrame_Dim(dxobs_glo_huv);
			        // dxobs_file.Write_StateFrame(dxobs_glo_huv,var_name);
#endif
				}

#if defined(MPI) || defined(MPI_simple)
		    	//Bcast to each processor, then work on each copy
		    	//Or Alltoallw scatter different size to each processor
				MPI_Bcast(dxobs_glo.data(), obs_mpidom_obj.nSize_glo()*nState_, MPI_DOUBLE, obs_mpidom_obj.root(), MPI_COMM_WORLD);

				h_glo=get_h(dxobs_glo, obs_mpidom_obj.nx_glo(), obs_mpidom_obj.ny_glo());
				u_glo=get_u(dxobs_glo, obs_mpidom_obj.nx_glo(), obs_mpidom_obj.ny_glo());
				v_glo=get_v(dxobs_glo, obs_mpidom_obj.nx_glo(), obs_mpidom_obj.ny_glo());

				h_loc=h_glo.block(obs_mpidom_obj.ix_loc(), obs_mpidom_obj.iy_loc(), obs_mpidom_obj.nx_loc(), obs_mpidom_obj.ny_loc());
				u_loc=u_glo.block(obs_mpidom_obj.ix_loc(), obs_mpidom_obj.iy_loc(), obs_mpidom_obj.nx_loc(), obs_mpidom_obj.ny_loc());
				v_loc=v_glo.block(obs_mpidom_obj.ix_loc(), obs_mpidom_obj.iy_loc(), obs_mpidom_obj.nx_loc(), obs_mpidom_obj.ny_loc());
				dxobs=get_ctl_vector(h_loc, u_loc, v_loc);

#else
				dxobs=dxobs_glo;
#endif

				dXobs_.setDataSlice(dxobs);	
#ifdef DEBUG
			    if (obs_mpidom_obj.myrank()==obs_mpidom_obj.root()){
					cout<<"nobsSize_ "<<nobsSize_<<endl;
					cout<<"Observation::make_obs dXobs_ size info:"<<endl;
					dXobs_.info();
					cout<<"Observation::make_obs Xt_ size info:"<<endl;
					Xt_.info();
				}
#endif
				Xobs_All.setDataSlice(*(Xt_iter+(int)(das_obj.t_offset()/synthobs_modpar_obj_ptr->dt())+i*das_obj.t_ratio())+dxobs);
			}

			Xobs_=ObservationSpace_operator(Xobs_All, das_obj.Assimilation_type());

#ifdef DEBUG
		    if (obs_mpidom_obj.myrank()==obs_mpidom_obj.root()){
				cout<<"Observation::make_obs Xobs_ size info:"<<endl;
				Xobs_.info();
			}
			
#if defined(MPI) || defined(MPI_simple)
			Parameter::IO_Domain_Type Domain_flag=Parameter::Local;
#else
			Parameter::IO_Domain_Type Domain_flag=Parameter::Global;
#endif
        	NetCDFIO Xobs_file("Xobs", Domain_flag, obs_mpidom_obj);
        	Xobs_file.Create_File();
        	std::vector<std::string> var_name({"h","u","v"});
        
    	    Xobs_file.Add_StateTrajectory_Dim(Xobs_All);
	        Xobs_file.Write_StateTrajectory(Xobs_All,var_name);
#endif
		}
	}

	VectorXd Observation::ObservationSpace_operator(const VectorXd& Xstate, Parameter::Assimilation_Type assim_type){
		VectorXd Xobs;

		switch( assim_type )
		{
			case Parameter::All:
				Xobs=Xstate;
				break;
			case Parameter::Hei:
				Xobs=Xstate.head(nobsSize_*nobsState_);
				break;
			case Parameter::Vel:
				Xobs=Xstate.tail(nobsSize_*nobsState_);
				break;
			default:
				std::cerr<<"unspecified Assimilation_type"<<endl;
				std::abort();
		}

		return Xobs;
	}

	Data2D Observation::ObservationSpace_operator(const Data2D& Xstate, Parameter::Assimilation_Type assim_type){
		Data2D Xobs;

		for(auto t_iter=Xstate.begin(); t_iter!=Xstate.end(); ++t_iter)
			Xobs.setDataSlice( ObservationSpace_operator(*t_iter, assim_type) );

		return Xobs;
	}

	void Observation::make_invR(DataAssim& das_obj, MpiDom& obs_mpidom_obj){
	    if (obs_mpidom_obj.myrank()==obs_mpidom_obj.root())
			cout<<"Observation::make_invR make Observation error covariance matrix"<<endl;	

		VectorXd Rinv_All;

		Rinv_All.resize(nobsSize_*nState_);
		Rinv_All.head(nobsSize_)=(1.0/std::pow(das_obj.Err_ho(),2))*VectorXd::Ones(nobsSize_);
		Rinv_All.segment(nobsSize_,nobsSize_)=(1.0/std::pow(das_obj.Err_uo(),2))*VectorXd::Ones(nobsSize_);
		Rinv_All.tail(nobsSize_)=(1.0/std::pow(das_obj.Err_vo(),2))*VectorXd::Ones(nobsSize_);

		Rinv_=ObservationSpace_operator(Rinv_All, das_obj.Assimilation_type());

#ifdef DEBUG
#if defined(MPI) || defined(MPI_simple)
		Parameter::IO_Domain_Type Domain_flag=Parameter::Local;
#else
		Parameter::IO_Domain_Type Domain_flag=Parameter::Global;
#endif
        NetCDFIO Rinv_file("Rinv", Domain_flag, obs_mpidom_obj);
        Rinv_file.Create_File();
        std::vector<std::string> var_name({"Rinv_h","Rinv_u","Rinv_v"});
        std::vector<MatrixXd>    Rinv_huv;

        for (std::size_t i=0; i<(unsigned int)nState_; i++)
        	Rinv_huv.push_back(Eigen::Map<MatrixXd>(Rinv_All.data()+i*nobsSize_,obs_mpidom_obj.nx_glo(),obs_mpidom_obj.ny_glo()));

        Rinv_file.Add_StateFrame_Dim(Rinv_huv);
        Rinv_file.Write_StateFrame(Rinv_huv,var_name);
#endif
	}

	void Observation::make_obsens(DataAssim& das_obj, int nens, MpiDom& obs_mpidom_obj){
		VectorXd dxobs;
        Data2D   XobsENS_member;
		std::vector<VectorXd>  XobsENS_member_temp;

	    if (obs_mpidom_obj.myrank()==obs_mpidom_obj.root())
			cout<<"Observation::make_obsens make Observation Ensemble"<<endl;	

		dxobs.setZero(nobsSize_*nState_);
        for(std::size_t i=0; i<(unsigned int)nens; i++){
			for (auto Xobs_iter=Xobs_.begin(); Xobs_iter!=Xobs_.end(); Xobs_iter++){

				dxobs.head(nobsSize_)=NormalRandomMatrix(nobsSize_,1,0,das_obj.Err_ho(),obs_mpidom_obj.myrank());
				dxobs.segment(nobsSize_,nobsSize_)=NormalRandomMatrix(nobsSize_,1,0,das_obj.Err_uo(),obs_mpidom_obj.myrank());
				dxobs.tail(nobsSize_)=NormalRandomMatrix(nobsSize_,1,0,das_obj.Err_vo(),obs_mpidom_obj.myrank());

				XobsENS_member_temp.push_back( *(Xobs_iter)+ObservationSpace_operator(dxobs, das_obj.Assimilation_type()) );
			}
			XobsENS_member.setData(XobsENS_member_temp);
			XobsENS_member_temp.clear();
			XobsENS_.setDataSlice( ObservationSpace_operator(XobsENS_member,das_obj.Assimilation_type()) );
		}

		XobsENS_.setDataSlice(Xobs_);

#ifdef DEBUG
	    if (obs_mpidom_obj.myrank()==obs_mpidom_obj.root()){
			cout<<"Observation::make_obsens XobsENS_ size info:"<<endl;
			XobsENS_.info();
		}

// #if defined(MPI) || defined(MPI_simple)
// 		Parameter::IO_Domain_Type Domain_flag=Parameter::Local;
// #else
// 		Parameter::IO_Domain_Type Domain_flag=Parameter::Global;
// #endif
//         NetCDFIO XobsENS_file("XobsENS", Domain_flag, obs_mpidom_obj);
//         XobsENS_file.Create_File();
//         std::vector<std::string> var_name;

//         for (std::size_t i=0; i<(unsigned int)nens; i++){
//             if (i==0)
//                 XobsENS_file.Add_StateTrajectory_Dim(XobsENS_.at(i));
//             var_name.assign({"h"+std::to_string(i),"u"+std::to_string(i),"v"+std::to_string(i)});
//             XobsENS_file.Write_StateTrajectory(XobsENS_.at(i),var_name);
//         }
#endif
	}

	void make_obssynth(Observation& obssynth_obj, DataAssim& das_obj, Model& mod_obj, ModelPara& modelpara_obj, int nens, MpiDom& synthobs_mpidom_obj)
	{
		obssynth_obj.make_true_initialcondition(das_obj, modelpara_obj, synthobs_mpidom_obj);
		obssynth_obj.make_true_solution(das_obj, mod_obj, synthobs_mpidom_obj);
		obssynth_obj.make_obs(das_obj, synthobs_mpidom_obj, &modelpara_obj);
		obssynth_obj.make_invR(das_obj, synthobs_mpidom_obj);
		obssynth_obj.make_obsens(das_obj, nens, synthobs_mpidom_obj);
	}

	void make_obsreal(Observation& obsreal_obj, DataAssim& das_obj, int nens, MpiDom& realobs_mpidom_obj)
	{
		obsreal_obj.make_obs(das_obj, realobs_mpidom_obj);
		obsreal_obj.make_invR(das_obj, realobs_mpidom_obj);
		obsreal_obj.make_obsens(das_obj, nens, realobs_mpidom_obj);
	}

}