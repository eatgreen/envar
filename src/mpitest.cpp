#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Eigen/Core>
#include <chrono>
#include "Parameter.hpp"
#include "DataAssim.hpp"
#include "Ensemble.hpp"
#include "Model.hpp"
#include "Observation.hpp"
#include "Data.hpp"
#include "Localization.hpp"
#include "Optimization.hpp"
#include "NetCDFIO.hpp"
#include "../model/ModelPara.hpp"
#include "../model/MpiDom.hpp"

using namespace EnVar;
using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

using Clock=std::chrono::high_resolution_clock;

using Data2D=EnVar::DataBase<VectorXd>;
using Data3D=EnVar::DataBase<Data2D>;

int main(int argc, char *argv[])
{
	int myrank=0, size=1;
#if defined(MPI) || defined(MPI_simple)
	MPI_Init(&argc, &argv);                   /* Initialize MPI       */
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);   /* Get my rank          */
    MPI_Comm_size(MPI_COMM_WORLD, &size);     /* Get the total number of processors */
#endif

	int proc_ni=0, proc_nj=0;
    if( argc == 3 ) {
   		proc_ni=std::stoi(argv[1]);
   		proc_nj=std::stoi(argv[2]);
    }
    else if( argc ==1 ) {
        proc_ni=std::stoi(std::getenv("NPX"));
        proc_nj=std::stoi(std::getenv("NPY"));
    }
    else {
		if(myrank==0)
	      	std::cerr<<"Provide arguments for processors: 1 1 for single run, 2 2 for mpi run, \b"
      			 		" no arguments using system variables: NPX and NPY"<<endl;
      	std::abort();
    }

    if( size!=proc_ni*proc_nj ){
		if(myrank==0)
	    	std::cerr<<"Command line input processor number not equal to this run's processor configuration"<<endl;
    	std::abort();
    }

	Parameter 	para(0,proc_ni,proc_nj);

#if defined(MPI) || defined(MPI_simple)
	if(myrank==0)
#endif
		para.info();

#if defined(MPI) || defined(MPI_simple)
	MPI_Barrier(MPI_COMM_WORLD);
#endif

	// if (para.obsreal_flag()){
	// 	MpiDom  realobs_mpidom_obj(para.Realobs_dimpara());
	// 	realobs_mpidom_obj.Init_domain();
	// 	Observation       realobs_obj(para, realobs_mpidom_obj);
	// }
	// else{
		MpiDom  synthobs_mpidom_obj(para.Synthobs_modelpara().dimpara());
		synthobs_mpidom_obj.Init_domain();
		Model 			  synthmod_obj(para.Synthobs_modelpara(), synthobs_mpidom_obj);
		Observation 	  synthobs_obj(para, synthobs_mpidom_obj);
	// }

	MpiDom da_mpidom_obj(para.DA_modelpara().dimpara());
	da_mpidom_obj.Init_domain();
	
	DataAssim     das_obj(para, para.DA_modelpara(), da_mpidom_obj);

	// if (para.obsreal_flag())
	    // make_obsreal(realobs_obj, das_obj, ens_obj, realobs_mpidom_obj);
	// else
		make_obssynth(synthobs_obj, das_obj, synthmod_obj, para.Synthobs_modelpara(), para.nens(), synthobs_mpidom_obj);

	Ensemble 	  ens_obj(para);
	Optimization  opt_obj(para);

	auto t_deb=Clock::now();

	Model 		  moddyn_obj(para.DA_modelpara(), da_mpidom_obj);
	Localization  loc_obj(para);

	std::vector<Output> res_obj;

	for(std::size_t cycle=0; cycle<(unsigned int)das_obj.Assimilation_cycle(); cycle++)
	{
		das_obj.setCycleIndex(cycle);

		if(cycle==0)
			das_obj.BackgroundState(moddyn_obj,da_mpidom_obj);

		das_obj.InitialState(moddyn_obj,res_obj,da_mpidom_obj);

		if(cycle==0)
			ens_obj.EnsembleInitial(das_obj,moddyn_obj,da_mpidom_obj);

		auto t_ensinit=Clock::now();

		if (da_mpidom_obj.myrank()==da_mpidom_obj.root())
			cout<<"my rank: "<<da_mpidom_obj.myrank()<<" time elapsed until ensemble initial: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_ensinit-t_deb).count()<<" ms"<<endl;

		for(std::size_t iter=0; iter<(unsigned int)para.MaxIter_Outer(); iter++)
		{
			das_obj.ForecastState(moddyn_obj,da_mpidom_obj);

			ens_obj.EnsembleForecast(das_obj,moddyn_obj,da_mpidom_obj);

			// switch(das_obj.Algo_EnsembleUpdate_type())
			// {
				// case Parameter::Ensemble_Analysis:				

					ens_obj.EnsembleAnomaly(das_obj, synthobs_obj, da_mpidom_obj);

					ens_obj.EnsembleInnovation(das_obj, synthobs_obj.XobsENS(), da_mpidom_obj);

					if(loc_obj.Localization_type()==Parameter::Localize_Covariance){
						// loc_obj.CorrelationMatrix(da_mpidom_obj); //Memory demanding way
						loc_obj.Construct5thOrderCorrelationMatrix(da_mpidom_obj); //CPU demanding way
						loc_obj.SpectralDecompositionOfCorrelationMatrix(da_mpidom_obj); 
						loc_obj.ScatterCorrelationMatrix(da_mpidom_obj);
					}

					opt_obj.OptInner(ens_obj, synthobs_obj, loc_obj, da_mpidom_obj);

					// break;

				// case Parameter::Ensemble_Transform:

				// 	opt_obj.OptLocalInner(ens_obj, synthobs_obj, loc_obj, das_obj, da_mpidom_obj);

				// 	break;

				// default:
			// }
	
			// das_obj.UpdateInitialState(opt_obj.delta_x(), da_mpidom_obj);

			// ens_obj.UpdateEnsembleInitial(opt_obj.delta_xENS());

		}

		// das_obj.AnalysisState(moddyn_obj,da_mpidom_obj);

		// das_obj.CalculateRMS(synthobs_obj.Xt(), synthobs_obj.Xobs(), da_mpidom_obj);

		// das_obj.SaveResult();

		// das_obj.Output_cycle().at(cycle).Write_to_File(da_mpidom_obj);

		// if(das_obj.Assimilation_cycle()!=1 && das_obj.CycleIndex()!=das_obj.Assimilation_cycle())
		// 	ens_obj.EnsembleIntegration(das_obj,moddyn_obj,da_mpidom_obj);

	}
	
	auto t_fin=Clock::now();

	if (da_mpidom_obj.myrank()==da_mpidom_obj.root())
		cout<<"my rank: "<<da_mpidom_obj.myrank()<<" time elapsed until end: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;

#if defined(MPI) || defined(MPI_simple)
	MPI_Barrier(MPI_COMM_WORLD);
	cout<<da_mpidom_obj.myrank()<<" this is mpitest!!!"<<endl;
	MPI_Finalize();
#else
	cout<<da_mpidom_obj.myrank()<<" this is NOT mpitest!!!"<<endl;
#endif

	return 0;
}