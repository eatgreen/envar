#include "DataAssim.hpp"

namespace EnVar
{

	DataAssim::DataAssim(void)
	{}

	DataAssim::~DataAssim(void)
	{}

	//! A constructor taking one Parameter argument, one MpiDom argument.
	/*!
		\param par_obj is a Parameter argument
		\param da_mpi_obj is a MpiDom argument
	*/

	DataAssim::DataAssim(Parameter& par_obj, ModelPara& da_modelpara_obj, MpiDom& da_mpi_obj)
	{
		Obs_type_=par_obj.Obs_type();
		Assimilation_cycle_=par_obj.Assimilation_cycle();
		nobs_=par_obj.nobs();
		dtobs_=par_obj.dtobs();
		t_ratio_=par_obj.t_ratio();
		t_init_=par_obj.t_init();
		t_offset_=par_obj.t_offset();
		tobs_assim_=par_obj.tobs_assim();
		tmax_=par_obj.tmax();
		t_assim_deb_=par_obj.t_assim_deb();
		t_assim_fin_=par_obj.t_assim_fin();
		itobs_sum_=par_obj.itobs_sum();
		itobs_number_=par_obj.itobs_number();
		ittru_sum_=par_obj.ittru_sum();
		itobs_deb_=par_obj.itobs_deb();
		itobs_fin_=par_obj.itobs_fin();
		ittru_=par_obj.ittru();
		ittru_assim_deb_=par_obj.ittru_assim_deb();
		ittru_assim_fin_=par_obj.ittru_assim_fin();
		ittru_number_=par_obj.ittru_number();
		itmax_=par_obj.itmax();
		itinit_=par_obj.itinit();
		Assimilation_type_=par_obj.Assimilation_type();
		VarRangeDeb_=par_obj.VarRangeDeb();
		VarRangeFin_=par_obj.VarRangeFin();
		augState_=par_obj.augState();
		Assimflag_suffix_=par_obj.Assimflag_suffix();
		ParameterEstimation_flag_=par_obj.ParameterEstimation_flag();
		Parameter_type_=par_obj.Parameter_type();
		ParameterPert_type_=par_obj.ParameterPert_type();
		BackgroundState_type_=par_obj.BackgroundState_type();
		Algo_ControlVector_type_=par_obj.Algo_ControlVector_type();
		Algo_Precondition_type_=par_obj.Algo_Precondition_type();
		Algo_Localization_type_=par_obj.Algo_Localization_type();
		Algo_EnsembleUpdate_type_=par_obj.Algo_EnsembleUpdate_type();
		Write_Res_Each_Cycle_flag_=par_obj.Write_Res_Each_Cycle_flag();
		Err_type_=par_obj.Err_type();
		Err_hb_=par_obj.Err_hb();
		Err_ub_=par_obj.Err_ub();
		Err_vb_=par_obj.Err_vb();
		Err_ho_=par_obj.Err_ho();
		Err_uo_=par_obj.Err_uo();
		Err_vo_=par_obj.Err_vo();
		Err_beta_=par_obj.Err_beta();
		Inc_beta_=par_obj.Err_beta();

		//Initialize the background condition
		MatrixXd hb_glo,ub_glo,vb_glo;
		VectorXd sb_glo;
		hb_glo.setZero(da_mpi_obj.nx_glo(),da_mpi_obj.ny_glo());
		ub_glo.setZero(da_mpi_obj.nx_glo(),da_mpi_obj.ny_glo());
		vb_glo.setZero(da_mpi_obj.nx_glo(),da_mpi_obj.ny_glo());

		if (da_mpi_obj.myrank()==da_mpi_obj.root()){
	        switch(Obs_type_)
	        {
	            case 1: 
	            {   
	                // hb_glo=5000.0*MatrixXd::Ones(nx_,ny_)-f_cori_*4.0*xx_-f_cori_*8.0*yy_;
	                hb_glo=5000.0*MatrixXd::Ones(da_mpi_obj.nx_glo(),da_mpi_obj.ny_glo())-4.0e-4/da_modelpara_obj.g()*da_mpi_obj.xx_glo();
	                ub_glo.setZero(da_mpi_obj.nx_glo(),da_mpi_obj.ny_glo());
	                vb_glo.setZero(da_mpi_obj.nx_glo(),da_mpi_obj.ny_glo());
					sb_glo=get_ctl_vector(hb_glo, ub_glo, vb_glo);
	            }
	        }
    	}
                
#if defined(MPI) || defined(MPI_simple)
    	//Bcast to each processor, then work on each copy
    	//Or Alltoallw scatter different size to each processor
		MPI_Bcast(hb_glo.data(), da_mpi_obj.nSize_glo(), MPI_DOUBLE, da_mpi_obj.root(), MPI_COMM_WORLD);
		MPI_Bcast(ub_glo.data(), da_mpi_obj.nSize_glo(), MPI_DOUBLE, da_mpi_obj.root(), MPI_COMM_WORLD);
		MPI_Bcast(vb_glo.data(), da_mpi_obj.nSize_glo(), MPI_DOUBLE, da_mpi_obj.root(), MPI_COMM_WORLD);

		hb_=hb_glo.block(da_mpi_obj.ix_loc(), da_mpi_obj.iy_loc(), da_mpi_obj.nx_loc(), da_mpi_obj.ny_loc());
		ub_=ub_glo.block(da_mpi_obj.ix_loc(), da_mpi_obj.iy_loc(), da_mpi_obj.nx_loc(), da_mpi_obj.ny_loc());
		vb_=vb_glo.block(da_mpi_obj.ix_loc(), da_mpi_obj.iy_loc(), da_mpi_obj.nx_loc(), da_mpi_obj.ny_loc());
		sb_=get_ctl_vector(hb_, ub_, vb_);
#else
		hb_=hb_glo;
		ub_=ub_glo;
		vb_=vb_glo;
		sb_=sb_glo;
#endif

#ifdef DEBUG
#if defined(MPI) || defined(MPI_simple)
		Parameter::IO_Domain_Type Domain_flag=Parameter::Local;
#else
		Parameter::IO_Domain_Type Domain_flag=Parameter::Global;
#endif
        NetCDFIO bkgstate_file("bkg_state", Domain_flag, da_mpi_obj);
        bkgstate_file.Create_File();
        std::string var("h");

        bkgstate_file.Add_Matrix_Dim(hb_);
        bkgstate_file.Write_Matrix(hb_,var);
#endif
	}

	//! A member taking one Model argument and one MpiDom argument, compute Background trajectory 
	/*!
		Computing the background trajectory based on the background state
		\param mod_obj is a Model argument
		\param da_mpi_obj is a MpiDom argument
	*/

	void DataAssim::BackgroundState(Model& mod_obj, MpiDom& da_mpi_obj)
	{
		if (da_mpi_obj.myrank()==da_mpi_obj.root())
		    cout<<"DataAssim::make the background trajectory"<<endl;

	    Xb_=runmodel_frame(sb_,0,tmax_,mod_obj,da_mpi_obj);

#ifdef DEBUG
#if defined(MPI) || defined(MPI_simple)
		Parameter::IO_Domain_Type Domain_flag=Parameter::Local;
#else
		Parameter::IO_Domain_Type Domain_flag=Parameter::Global;
#endif
        NetCDFIO Xb_file("Xb", Domain_flag, da_mpi_obj);
        Xb_file.Create_File();
        std::vector<std::string> var_name({"h","u","v"});
        
        Xb_file.Add_StateTrajectory_Dim(Xb_);
        Xb_file.Write_StateTrajectory(Xb_,var_name);
#endif
	}

	//! A member taking one Model argument and one std::vector<Output> argument, compute Initial trajectory 
	/*!
		Computing the initial trajectory based on the initial state
		\param mod_obj is a Model argument
		\param res_obj is a std::vector<Output> argument
	*/

	void DataAssim::InitialState(Model& mod_obj, std::vector<Output>& res_obj, MpiDom& da_mpi_obj)
	{
		if (da_mpi_obj.myrank()==da_mpi_obj.root())
		    cout<<"DataAssim::make the initial state"<<endl;
	    if(CycleIndex_==0)
	    {
	    	if(t_offset_==0)
	    	{
	    		sf_.resize(sb_.size());
	    		sf_=sb_;
	    	}
	    	else
	    	{
	    		sf_=runmodel_final(sb_,t_init_,t_offset_,mod_obj,da_mpi_obj);
	    	}
	    }
	    else
	    {
	    	sf_=res_obj.at(CycleIndex_-1).Xa().back();
	    }

	}

	void DataAssim::ForecastState(Model& mod_obj, MpiDom& da_mpi_obj)
	{
		if (da_mpi_obj.myrank()==da_mpi_obj.root())
		    cout<<"DataAssim::make the forecast state trajectory"<<endl;
	    Xf_=runmodel_frame(sf_,t_assim_deb_.at(CycleIndex_),t_assim_fin_.at(CycleIndex_),mod_obj,da_mpi_obj);

	}

	void DataAssim::UpdateInitialState(const VectorXd& delta_x, MpiDom& da_mpi_obj)
	{
		if (da_mpi_obj.myrank()==da_mpi_obj.root())
		    cout<<"DataAssim::update the initial state"<<endl;
		sa_=sf_+delta_x;
	}

	void DataAssim::AnalysisState(Model& mod_obj, MpiDom& da_mpi_obj)
	{
		if (da_mpi_obj.myrank()==da_mpi_obj.root())
		    cout<<"DataAssim::make the analysis state trajectory"<<endl;
	    Xa_=runmodel_frame(sa_,t_assim_deb_.at(CycleIndex_),t_assim_fin_.at(CycleIndex_),mod_obj,da_mpi_obj);
	}

	void DataAssim::CalculateRMS(const Data2D& Xt, const Data2D& Xobs, MpiDom& da_mpi_obj)
	{
		std::vector<VectorXd> Xobs_cycle_temp(itobs_number_);

		std::copy(Xobs.begin()+CycleIndex_*itobs_number_, 
				  Xobs.begin()+(CycleIndex_+1)*itobs_number_, 
				  Xobs_cycle_temp.begin());

#ifdef DEBUG
		cout<<"Xt cycle"<<endl;
		Xt.info();

		cout<<"Xobs whole"<<endl;
		Xobs.info();
#endif

		Xobs_cycle_.setData(Xobs_cycle_temp);

#ifdef DEBUG
		cout<<"Xobs cycle"<<endl;
		Xobs_cycle_.info();
#endif

		if (da_mpi_obj.myrank()==da_mpi_obj.root())
		    cout<<"DataAssim::calculate RMS"<<endl;

	    RMSXobsXt_.resize(itobs_number_, augState_);
	    
		for(std::size_t i=0; i<itobs_number_; i++)
		{
			RMSXobsXt_(i,0)=std::sqrt((( Xobs_cycle_.at(i).head(da_mpi_obj.nSize_loc())-Xt.at( ittru_assim_deb_.at(CycleIndex_)+i*t_ratio_ ).head(da_mpi_obj.nSize_loc()) ).cwiseAbs2()).mean());
			RMSXobsXt_(i,1)=std::sqrt((( Xobs_cycle_.at(i).segment(da_mpi_obj.nSize_loc(),da_mpi_obj.nSize_loc())-Xt.at( ittru_assim_deb_.at(CycleIndex_)+i*t_ratio_ ).segment(da_mpi_obj.nSize_loc(),da_mpi_obj.nSize_loc()) ).cwiseAbs2()).mean());		
			RMSXobsXt_(i,2)=std::sqrt((( Xobs_cycle_.at(i).tail(da_mpi_obj.nSize_loc())-Xt.at( ittru_assim_deb_.at(CycleIndex_)+i*t_ratio_ ).tail(da_mpi_obj.nSize_loc()) ).cwiseAbs2()).mean());
		}

		RMSXbXt_.resize(ittru_number_.at(CycleIndex_), augState_);
		RMSXaXt_.resize(ittru_number_.at(CycleIndex_), augState_);
		for(std::size_t i=0; i<(unsigned int)ittru_number_.at(CycleIndex_); i++)
		{
			RMSXbXt_(i,0)=std::sqrt((( Xb_.at(ittru_assim_deb_.at(CycleIndex_)+i).head(da_mpi_obj.nSize_loc())-Xt.at(ittru_assim_deb_.at(CycleIndex_)+i).head(da_mpi_obj.nSize_loc()) ).cwiseAbs2()).mean());
			RMSXbXt_(i,1)=std::sqrt((( Xb_.at(ittru_assim_deb_.at(CycleIndex_)+i).segment(da_mpi_obj.nSize_loc(),da_mpi_obj.nSize_loc())-Xt.at( ittru_assim_deb_.at(CycleIndex_)+i).segment(da_mpi_obj.nSize_loc(),da_mpi_obj.nSize_loc()) ).cwiseAbs2()).mean());
			RMSXbXt_(i,2)=std::sqrt((( Xb_.at(ittru_assim_deb_.at(CycleIndex_)+i).tail(da_mpi_obj.nSize_loc())-Xt.at( ittru_assim_deb_.at(CycleIndex_)+i).tail(da_mpi_obj.nSize_loc()) ).cwiseAbs2()).mean());

			RMSXaXt_(i,0)=std::sqrt((( Xa_.at(i).head(da_mpi_obj.nSize_loc())-Xt.at(ittru_assim_deb_.at(CycleIndex_)+i).head(da_mpi_obj.nSize_loc()) ).cwiseAbs2()).mean());
			RMSXaXt_(i,1)=std::sqrt((( Xa_.at(i).segment(da_mpi_obj.nSize_loc(),da_mpi_obj.nSize_loc())-Xt.at(ittru_assim_deb_.at(CycleIndex_)+i).segment(da_mpi_obj.nSize_loc(),da_mpi_obj.nSize_loc()) ).cwiseAbs2()).mean());
			RMSXaXt_(i,2)=std::sqrt((( Xa_.at(i).tail(da_mpi_obj.nSize_loc())-Xt.at(ittru_assim_deb_.at(CycleIndex_)+i).tail(da_mpi_obj.nSize_loc()) ).cwiseAbs2()).mean());
		}
	}

	void DataAssim::SaveResult()
	{	
		Output output_current_cycle_obj(CycleIndex_,Xobs_cycle_,Xb_,Xf_,Xa_,RMSXobsXt_,RMSXbXt_,RMSXaXt_);
		Output_cycle_.push_back(output_current_cycle_obj);
	}

	Data2D TimeDownSample2D(const Data2D& X, const int& t_ratio)
	{
		Data2D Y;

		for(std::size_t i=0; i<X.size()[0]-t_ratio; i+=t_ratio)
		{
			Y.setDataSlice(X.at(i));
		}

		//TODO: problematic when thinning Xt
		// for(auto N_iter=X.begin(); N_iter!=X.end(); std::advance(N_iter,t_ratio))	
		// {
		// 	Y.setDataSlice(*N_iter);
		// }
		return Y;
	}

}