#ifndef _Data
#define _Data

#include <Eigen/Core>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

namespace EnVar {

class DataBase {
 private:
    std::vector<MatrixXd> xens_;

 public:
    DataBase() {}
    ~DataBase() {}

    const std::vector<MatrixXd>& data(void) const { return xens_; }
    MatrixXd  at(const int) const;
    VectorXd  at(const int, const int) const;
    std::vector<unsigned int> size() const;
    std::vector<MatrixXd>::const_iterator begin() const;
    std::vector<MatrixXd>::const_iterator end() const;
    const MatrixXd     front() const;
    const MatrixXd     back() const;
    void  info() const;
    void  setDataSlice(const MatrixXd&);
    void  setDataSlice(const MatrixXd&&);
    void  setData(const std::vector<MatrixXd>&);
    void  setZero(const int, const int, const int);

    const DataBase  operator+(const DataBase&) const;
    const DataBase  operator+=(const DataBase&);
    const DataBase  operator+=(const DataBase&&);
    const DataBase  operator-(const DataBase&) const;
    const DataBase  operator*(const double) const;
    const DataBase  operator/(const double) const;
};

//! A member get the MatrixXd data at index i

inline MatrixXd DataBase::at(const int i) const {
    MatrixXd res;
    res = this->data()[i];
    return res;
}

//! A member get the VectorXd data at index i,j

inline VectorXd DataBase::at(const int i, const int j) const {
    VectorXd res;
    res = this->data()[i].col(j);
    return res;
}

//! A member get the begin iterator of data

std::vector<MatrixXd>::const_iterator DataBase::begin() const {
    return this->data().begin();
}

//! A member get the end iterator of data

std::vector<MatrixXd>::const_iterator DataBase::end() const {
    return this->data().end();
}

//! A member get the front element of data

const MatrixXd DataBase::front() const {
    return this->data().front();
}

//! A member get the back element of data

const MatrixXd DataBase::back() const {
    return this->data().back();
}

//! A member get the size

inline std::vector<unsigned int> DataBase::size() const {
    std::vector<unsigned int> v;
    v.push_back(this->data().size());
    v.push_back(this->at(0).rows());
    v.push_back(this->at(0).cols());
    return v;
}

//! A member displaying the basic info

inline void DataBase::info() const {
    cout << "Data3D type" << endl;
    cout << "control vector size,                " << this->size()[0] << endl;
    cout << "state variable space sampling size, " << this->size()[1] << endl;
    cout << "time sampling size,                 " << this->size()[2] << endl;
}

inline void DataBase::setData(const std::vector<MatrixXd>& b) {
    xens_ = b;
}

inline void DataBase::setZero(const int stdvector_size, const int matrixxd_nx, const int matrixxd_ny ) {
    for (std::size_t i = 0; i < (unsigned int)stdvector_size; i++) {
        xens_.push_back(MatrixXd::Zero(matrixxd_nx, matrixxd_ny));
    }
}

void DataBase::setDataSlice(const MatrixXd& b) {
    xens_.push_back(b);
}

void DataBase::setDataSlice(const MatrixXd&& b) {
    this->setDataSlice(b);
}


const DataBase DataBase::operator+(const DataBase& b) const {
    DataBase plus;
    std::vector<MatrixXd> plus_xens;

    if (this->size()[0]!= b.size()[0]) {
        std::cerr << "Data first dimenstion inconsistent" << endl;
        std::abort();
    }

    for (std::size_t i = 0; i < this->size()[0]; i++) {
        plus_xens.push_back(this->at(i)+b.at(i));
    }
    plus.setData(plus_xens);

    return plus;
}

const DataBase DataBase::operator+=(const DataBase& b) {
    std::vector<MatrixXd> plus_xens;
    for (std::size_t i = 0; i < this->size()[0]; i++) {
        plus_xens.push_back(this->at(i)+b.at(i));
    }
    this->setData(plus_xens);

    return *this;
}

const DataBase DataBase::operator+=(const DataBase&& b) {
    this->operator+=(b);
    return *this;
}

const DataBase DataBase::operator-(const DataBase& b) const {
    DataBase minus;
    std::vector<MatrixXd> minus_xens;

    if (this->size()[0] != b.size()[0]) {
        std::cerr << "Data first dimension inconsistent" << endl;
        std::abort();
    }

    for (std::size_t i=0; i < this->size()[0]; i++) {
        minus_xens.push_back(this->at(i)-b.at(i));
    }
    minus.setData(minus_xens);

    return minus;
}

const DataBase DataBase::operator*(const double b) const {
    DataBase times;
    std::vector<MatrixXd> times_xens;

    for (std::size_t i = 0; i < this->size()[0]; i++) {
        times_xens.push_back(this->at(i)*b);
    }
    times.setData(times_xens);

    return times;
}

const DataBase DataBase::operator/(const double b) const {
    DataBase divide;
    std::vector<MatrixXd> divide_xens;

    for (std::size_t i = 0; i < this->size()[0]; i++) {
        divide_xens.push_back(this->at(i)/b);
    }
    divide.setData(divide_xens);

    return divide;
}

}  // namespace Envar
#endif
