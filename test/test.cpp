#ifdef RUN_UNIT_TESTS
#include "../unit_tests/unit_tests.hpp"

#else
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Eigen/Core>
#include "Parameter.hpp"
#include "DynObs.hpp"
#include "DataAssim.hpp"
#include "Model.hpp"
#include "Observation.hpp"
#include "Data.hpp"
#include "Localization.hpp"
#include "Optimization.hpp"

using namespace EnVar;
using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

int main(int argc, char *argv[])
{
#ifdef _MPI
	int rank,size;
#endif

	Parameter 	para(0);
	para.info();

#ifdef _MPI
	MPI_Init(&argc, &argv);                 /* Initialize MPI       */
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);   /* Get my rank          */
    MPI_Comm_size(MPI_COMM_WORLD, &size);   /* Get the total number of processors */
#endif

	cout<<"size:   "<<size<<endl;
	cout<<"myrank: "<<rank<<endl;

	// MpiDom 		  mpi_dom(para);
	// mpi_dom.Init_domain();

	// DataAssim 	  das_obj(para,mpi_dom);
	// Ensemble 	  ens_obj(para);

	// Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;
	// Model 		  model_obj(para, assim_dyn, mpi_dom);

	// std::vector<Output> res_obj;

	// das_obj.setCycleIndex(0);

	// das_obj.InitialState(model_obj,res_obj,mpi_dom);

	// ens_obj.EnsembleInitial(das_obj,model_obj,mpi_dom);

	// das_obj.ForecastState(model_obj,mpi_dom);

	// ens_obj.EnsembleForecast(das_obj,model_obj,mpi_dom);
	
	// ens_obj.EnsembleAnomaly(das_obj);

	// int lde=1, n1=mpi_dom.nx(), n2=mpi_dom.ny();
	// double rx=5.0, ry=5.0, dx=1.0, dy=1.0, theta=0;  

	// MatrixXd NoiseMat,NoiseMatT;

	// NoiseMat.resize(mpi_dom.nx()*mpi_dom.ny(),lde);
	// pseudo2dM(NoiseMat,mpi_dom.ny(),mpi_dom.nx(),lde,rx,ry,dx,dy,n2,n1,theta);
    // Eigen::Map<MatrixXd> noise_h(NoiseMat.data(),mpi_dom.nx(),mpi_dom.ny());

	// NetCDFIO noisefile("noise", Parameter::Global, mpi_dom);
	// noisefile.Create_File();
	// noisefile.Add_Matrix_Dim(noise_h);
	// noisefile.Write_Matrix(noise_h,"h");

	// Parameter 	   para;
	// Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;

	// Model 		   model_obj(para, assim_dyn);
	// DataAssim	   das_obj(para);

	// VectorXd MQx;

	// MQx=model_obj.Physical2Movement(para.sb());

	// das_obj.BackgroundState(model_obj);

	// std::ofstream file0("data/xb0", std::ofstream::out);
	// if(file0.is_open()){
	// 	file0<<das_obj.sb();
	// 	file0.close();
	// }

	// std::ofstream file1("data/xbf", std::ofstream::out);
	// if(file1.is_open()){
	// 	file1<<das_obj.Xb().back();
	// 	file1.close();
	// }

	// Optimization   opt_obj(para,3);
	// opt_obj.OptSingle();

	// Parameter 	   para;
	// Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;
	// DataAssim 	   das_obj(para);
	// Localization   loc_obj(para,assim_dyn);

	// loc_obj.CorrelationMatrix(das_obj);

	// Parameter::ModelSpace_Type synth_obs=Parameter::obs_space;

	// Model modsynth_obj(para, synth_obs);
	// Observation 	 obssynth_obj(para, synth_obs);

	// DataAssim 	  das_obj(para);
	// Ensemble 	  ens_obj(para);

	// obssynth_obj.make_true_solution(das_obj, modsynth_obj);
	// cout<<"Xt size"<<obssynth_obj.Xt().size()[0]<<"capacity"<<obssynth_obj.Xt().data().capacity()<<endl;
	// obssynth_obj.make_obs(das_obj);
	// cout<<"Xobs size"<<obssynth_obj.Xobs().size()[0]<<"capacity"<<obssynth_obj.Xobs().data().capacity()<<endl;

	// Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;

	// Model 		  moddyn_obj(para, assim_dyn);

	// cout<<das_obj.tmax()<<endl;
	// cout<<das_obj.sb().size()<<endl;

	// das_obj.BackgroundState(moddyn_obj);
	// cout<<das_obj.Xb().size()[0]<<endl;

	// std::string   file_name("xensf");

	// for (auto iter=das_obj.ittru().begin(); iter!=das_obj.ittru().end(); ++iter){
	// 	cout<<"ittru "<<*iter<<endl;
	// }

	// ens_obj.EnsembleInitial(das_obj, moddyn_obj);
	// write_data(ens_obj.xensf(), file_name);

	// std::vector<VectorXd> Xt;
	// cout<<"size"<<Xt.size()<<"capacity"<<Xt.capacity()<<endl;
	// cout<<"size"<<Xt.size()<<"capacity"<<Xt.capacity()<<endl;	

	// std::vector<std::string> var_name;
	// var_name.push_back("h");
	// var_name.push_back("u");
	// var_name.push_back("v");

	// VectorXd q1,q2,q3,q4,xp,k1,k2,k3,x1,x2,x3;
	// MatrixXd h,u,v;

	// q1=model_obj.Physical2Movement(das_obj.sb());

	// Model::BC hBC,uBC,vBC;

	// x1=model_obj.InvPhysical2Movement(q1);

	// h=get_h(x1, const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
	// u=get_u(x1, const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
	// v=get_v(x1, const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));

	// hBC=model_obj.set_BC(h, "h", mpidom_obj);
	// uBC=model_obj.set_BC(u, "u", mpidom_obj);
	// vBC=model_obj.set_BC(v, "v", mpidom_obj);

	// k1=model_obj.Advection(q1, mpidom_obj);
 // 	q2=q1 + 0.5*model_obj.dt()*k1;

 //    k2=model_obj.Advection(q2, mpidom_obj);
 //    q3=q1 - model_obj.dt()*k1+2.0*model_obj.dt()*k2;

	// x2=model_obj.InvPhysical2Movement(q3);

	// h=get_h(x2, const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
	// u=get_u(x2, const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
	// v=get_v(x2, const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));

	// hBC=model_obj.set_BC(h, "h", mpidom_obj);
	// uBC=model_obj.set_BC(u, "u", mpidom_obj);
	// vBC=model_obj.set_BC(v, "v", mpidom_obj);

 //    k3=model_obj.Advection(q3, mpidom_obj);
 //    q4=q1 + model_obj.dt()*(1.0/6*k1+2.0/3*k2+1.0/6*k3);

	// bool flagX=1;
	// bool flagY=!flagX;
	// VectorXd Flux;
	// std::vector<MatrixXd> F,G,Fluxfoo;

	// F=model_obj.RoeFlux(hBC.WBC(),hBC.EBC(),uBC.WBC(),uBC.EBC(),vBC.WBC(),vBC.EBC(),flagX);

	// G=model_obj.RoeFlux(hBC.NBC(),hBC.SBC(),uBC.NBC(),uBC.SBC(),vBC.NBC(),vBC.SBC(),flagY);

		// flagX=0;
		// flagY=!flagX;
		// int  rows, cols;
		// MatrixXd hBC_l,hBC_r,uBC_l,uBC_r,vBC_l,vBC_r;
		// hBC_l=hBC.NBC();
		// hBC_r=hBC.SBC();
		// uBC_l=uBC.NBC();
		// uBC_r=uBC.SBC();
		// vBC_l=vBC.NBC();
		// vBC_r=vBC.SBC();

		// rows=hBC_l.rows();
		// cols=hBC_l.cols();

		// MatrixXd duml,dumr,hhat,uhat,vhat,chat,chatl,chatr,dh,du,dv,uNl,uNr;
		// MatrixXd al2,al3,ar2,ar3,a1,a2,a3,da2,da3;
		// MatrixXd R21,R22,R23,R31,R32,R33,alpha1,alpha2,alpha3,FL1,FL2,FL3,FR1,FR2,FR3;

		// duml=hBC_l.cwiseSqrt();
		// dumr=hBC_r.cwiseSqrt();

		// hhat=duml.cwiseProduct(dumr);
		// uhat=(duml.cwiseProduct(uBC_l)+dumr.cwiseProduct(uBC_r)).cwiseQuotient(duml+dumr);
		// vhat=(duml.cwiseProduct(vBC_l)+dumr.cwiseProduct(vBC_r)).cwiseQuotient(duml+dumr);

		// chat=(0.5*para.g()*(hBC_l+hBC_r)).cwiseSqrt();
		// chatl=(para.g()*hBC_l).cwiseSqrt();
		// chatr=(para.g()*hBC_r).cwiseSqrt();

		// dh=hBC_r-hBC_l;
		// du=uBC_r-uBC_l;
		// dv=vBC_r-vBC_l;

		// uNl = uBC_l*flagX + vBC_l*flagY;
		// uNr = uBC_r*flagX + vBC_r*flagY;

		// al2=uNl+chatl;
		// al3=uNl-chatl;
		// ar2=uNr+chatr;
		// ar3=uNr-chatr;

		// a1 = (uhat*flagX + vhat*flagY).cwiseAbs() ;
		// a2 = (uhat*flagX + vhat*flagY + chat).cwiseAbs();
		// a3 = (uhat*flagX + vhat*flagY - chat).cwiseAbs();

		// da2=(2*(ar2-al2)).cwiseMax(MatrixXd::Zero(rows,cols));
		// da3=(2*(ar3-al3)).cwiseMax(MatrixXd::Zero(rows,cols));

		// a2=(a2.array()<da2.array()).select((a2.array()*a2.array()/da2.array()+da2.array())*0.5,a2);

		// a3=(a3.array()<da3.array()).select((a3.array()*a3.array()/da3.array()+da3.array())*0.5,a3);

		// R21=-flagY*MatrixXd::Ones(rows,cols);
		// R22=uhat+flagX*chat;
		// R23=uhat-flagX*chat;
		// R31=flagX*MatrixXd::Ones(rows,cols);
		// R32=vhat+flagY*chat;
		// R33=vhat-flagY*chat;

		// alpha1 = (hhat.array()*dv.array()*flagX - hhat.array()*du.array()*flagY)*a1.array();
		// alpha2 = 0.5 * (dh.array() + (hhat.array()*du.array()*flagX + hhat.array()*dv.array()*flagY)/chat.array()).array()*a2.array();
		// alpha3 = 0.5 * (dh.array() - (hhat.array()*du.array()*flagX + hhat.array()*dv.array()*flagY)/chat.array()).array()*a3.array();

		// // Left flux
		// FL1=uNl.array()*hBC_l.array();
		// FL2=uBC_l.array()*uNl.array()*hBC_l.array() + 0.5*para.g()*hBC_l.array()*hBC_l.array()*flagX;
		// FL3=vBC_l.array()*uNl.array()*hBC_l.array() + 0.5*para.g()*hBC_l.array()*hBC_l.array()*flagY;

		//  // Right flux
		// FR1=uNr.array()*hBC_r.array();
		// FR2=uBC_r.array()*uNr.array()*hBC_r.array() + 0.5*para.g()*hBC_r.array()*hBC_r.array()*flagX;
		// FR3=vBC_r.array()*uNr.array()*hBC_r.array() + 0.5*para.g()*hBC_r.array()*hBC_r.array()*flagY;

		// G.push_back(0.5*(FL1+FR1-alpha2-alpha3));
		// G.push_back(0.5*(FL2.array()+FR2.array()-(R21.array()*alpha1.array())-(R22.array()*alpha2.array())-(R23.array()*alpha3.array())));
		// G.push_back(0.5*(FL3.array()+FR3.array()-(R31.array()*alpha1.array())-(R32.array()*alpha2.array())-(R33.array()*alpha3.array())));
			
	// for(std::size_t i=0; i<(unsigned int)model_obj.nState(); i++){
	// 	Fluxfoo.push_back( -( ( F.at(i).topRightCorner(F.at(i).rows()-1,F.at(i).cols()-1) - F.at(i).topLeftCorner(F.at(i).rows()-1,F.at(i).cols()-1) )/model_obj.dx()
	// 	   + ( G.at(i).bottomLeftCorner(G.at(i).rows()-1,G.at(i).cols()-1) - G.at(i).topLeftCorner(G.at(i).rows()-1,G.at(i).cols()-1) )/model_obj.dy() ) );
	// }

	// Flux=get_ctl_vector(Fluxfoo.at(0),Fluxfoo.at(1),Fluxfoo.at(2));

    // q3=q1-model_obj.dt()*k1+2.0*model_obj.dt()*k2;
    // k3=model_obj.Advection(q3, mpidom_obj);
    // q4=q1+model_obj.dt()*(1.0/6*k1+2.0/3*k2+1.0/6*k3);

	// h=get_h(Flux, const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
	// u=get_u(Flux, const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
	// v=get_v(Flux, const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));

	// std::string file_name0,file_name1,file_name2,file_name3,file_name4,file_name5,file_name6,file_name7,file_name8,file_name9,file_namea;
// 	file_name0="test_xp";
// 	file_name1="test_h";
// 	file_name2="test_u";
// 	file_name3="test_v";
// 	file_name4="test_hL";
// 	file_name5="test_hR";
// 	file_name6="test_uL";
// 	file_name7="test_uR";
// 	file_name8="test_vL";
// 	file_name9="test_vR";
// 	file_namea="test_sb";

// // Write huv netcdf
// 	ncdf_obj.Write_Matrix(file_name1,h,var_name.at(0),mpidom_obj);
// 	ncdf_obj.Write_Matrix(file_name2,u,var_name.at(0),mpidom_obj);
// 	ncdf_obj.Write_Matrix(file_name3,v,var_name.at(0),mpidom_obj);
// 	ncdf_obj.Write_Matrix(file_name4,hBC.WBC(),var_name.at(0),mpidom_obj);
// 	ncdf_obj.Write_Matrix(file_name5,hBC.EBC(),var_name.at(0),mpidom_obj);
// 	ncdf_obj.Write_Matrix(file_name6,uBC.WBC(),var_name.at(0),mpidom_obj);
// 	ncdf_obj.Write_Matrix(file_name7,uBC.EBC(),var_name.at(0),mpidom_obj);
// 	ncdf_obj.Write_Matrix(file_name8,vBC.WBC(),var_name.at(0),mpidom_obj);
// 	ncdf_obj.Write_Matrix(file_name9,vBC.EBC(),var_name.at(0),mpidom_obj);

// #if defined(_MPI) || defined(_MPI_simple)
//Combine huv netcdf
// 	MPI_Barrier(MPI_COMM_WORLD);
// 	ncdf_obj.Combine_Matrix(file_name1,var_name.at(0),mpidom_obj);
// 	ncdf_obj.Combine_Matrix(file_name2,var_name.at(0),mpidom_obj);
// 	ncdf_obj.Combine_Matrix(file_name3,var_name.at(0),mpidom_obj);
// #endif

// 	file_name3="test_xp";
// 	file_name4="test_sb";
// 	file_name5="test_sf";

// 	xp=model_obj.TimeIntegration2D(das_obj.sb(), mpidom_obj);

// 	Data2D xp2D;
// 	xp2D=runmodel_frame(das_obj.sb(), 0, das_obj.tmax(), model_obj, mpidom_obj);

// 	std::vector<MatrixXd> xp_s,xp_sp;
// 	h=get_h(xp2D.back(), const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
// 	u=get_u(xp2D.back(), const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
// 	v=get_v(xp2D.back(), const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
// 	xp_s.push_back(h);
// 	xp_s.push_back(u);
// 	xp_s.push_back(v);

// //Write huv netcdf
// 	ncdf_obj.Write_StateFrame(file_name3,xp_s,var_name,mpidom_obj);

// #if defined(_MPI) || defined(_MPI_simple)
// // Combine huv netcdf
// 	ncdf_obj.Combine_StateFrame(file_name3,var_name,mpidom_obj);
// 	MPI_Barrier(MPI_COMM_WORLD);
// #endif

//  	das_obj.BackgroundState(model_obj, mpidom_obj);

// 	h=get_h(das_obj.Xb().back(), const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
// 	u=get_u(das_obj.Xb().back(), const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
// 	v=get_v(das_obj.Xb().back(), const_cast<int&>(mpidom_obj.nx_loc()), const_cast<int&>(mpidom_obj.ny_loc()));
// 	xp_sp.push_back(h);
// 	xp_sp.push_back(u);
// 	xp_sp.push_back(v);

// //Write huv netcdf
// 	ncdf_obj.Write_StateFrame(file_name4,xp_sp,var_name,mpidom_obj);

// #if defined(_MPI) || defined(_MPI_simple)
// // Combine huv netcdf
// 	ncdf_obj.Combine_StateFrame(file_name4,var_name,mpidom_obj);
// 	MPI_Barrier(MPI_COMM_WORLD);
// #endif

// //Write huvt netcdf
// 	ncdf_obj.Write_StateTrajectory(file_name5,das_obj.Xb(),var_name,mpidom_obj);

// #if defined(_MPI) || defined(_MPI_simple)
// //Combine huvt netcdf
// 	ncdf_obj.Combine_StateTrajectory(file_name5,var_name,mpidom_obj);
// 	MPI_Barrier(MPI_COMM_WORLD);
// #endif

//Read huv netcdf
	// std::vector<MatrixXd> F_read;
	// F_read=ncdf_obj.Read_StateFrame(file_name1,var_name,mpidom_obj,mpidom_obj.myrank());

	// if(mpidom_obj.myrank()==0)
	// 	cout<<F_read.at(0)<<endl;

//Read huvt netcdf
	// Data2D Xb;
	// Xb=ncdf_obj.Read_StateTrajectory(file_name4,var_name,mpidom_obj,mpidom_obj.myrank());

	#ifdef _MPI
	MPI_Finalize();
	#endif

	return 0;
}
#endif