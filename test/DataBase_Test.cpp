#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Eigen/Core>
#include <chrono>
#include "DataBase.hpp"

using namespace EnVar;
using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

using Clock=std::chrono::high_resolution_clock;

using Data2D=Eigen::MatrixXd;
using Data3D=EnVar::DataBase;

int main(int argc, char *argv[])
{
	DataBase x;

	int nens=8;
	int nx=2;
	int ny=3;
	int t=3;

	int nsize=nx*ny;

	MatrixXd x_elem;
	// std::vector<MatrixXd> y;

	DataBase y,z;

	for (int i=0; i<nens; i++){
		x_elem=MatrixXd::Ones(nsize,t)*i;
		x.setDataSlice(x_elem);
		// y.push_back(x_elem);
	}

	// x.setData(y);

	// x.setZero(nens, nsize, t);

	x.info();

	y=x*2.0;

	z=y/5.0+x;

	x+=z;

	cout<<x.at(7)<<endl;

	cout<<x.front()<<endl;

	// cout<<x.at(7)<<endl;

	// cout<<x.front()<<endl;


}