#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Eigen/Core>
#include "Parameter.hpp"
#include "DynObs.hpp"
#include "DataAssim.hpp"
#include "Ensemble.hpp"
#include "Model.hpp"
#include "Observation.hpp"
#include "Data.hpp"
#include "Localization.hpp"
#include "Optimization.hpp"
#include "MpiDom.hpp"
#include <chrono>
#include "NetCDFIO.hpp"

using namespace EnVar;
using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

using Clock=std::chrono::high_resolution_clock;

using Data2D=EnVar::DataBase<VectorXd>;
using Data3D=EnVar::DataBase<Data2D>;

int main(int argc, char *argv[])
{
#if defined(_MPI) || defined(_MPI_simple)
	int myrank,size;
	MPI_Init(&argc, &argv);                   /* Initialize MPI       */
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);   /* Get my rank          */
    MPI_Comm_size(MPI_COMM_WORLD, &size);     /* Get the total number of processors */
#endif

	Parameter 	para(0);

#if defined(_MPI) || defined(_MPI_simple)
	if(myrank==0)
#endif
		para.info();

#if defined(_MPI) || defined(_MPI_simple)
	MPI_Barrier(MPI_COMM_WORLD);
#endif

	MpiDom 		  mpidom_obj(para);
	mpidom_obj.Init_domain();

	Parameter::ModelSpace_Type synth_obs=Parameter::obs_space;
	Model 			modsynth_obj(para, synth_obs, mpidom_obj);
	Observation 	obssynth_obj(para, synth_obs, mpidom_obj);

	DataAssim     das_obj(para,mpidom_obj);
	Ensemble 	  ens_obj(para);
	Optimization  opt_obj(para);
	NetCDFIO 	  ncdf_obj;

	make_obssynth(obssynth_obj, das_obj, modsynth_obj, ens_obj, mpidom_obj);

	auto t_deb=Clock::now();

	Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;
	Model 		  moddyn_obj(para, assim_dyn, mpidom_obj);
	Localization  loc_obj(para, assim_dyn, mpidom_obj);

	std::vector<Output> res_obj;

	for(std::size_t cycle=0; cycle<(unsigned int)das_obj.Assimilation_cycle(); cycle++)
	{
		das_obj.setCycleIndex(cycle);

		if(cycle==0)
			das_obj.BackgroundState(moddyn_obj,mpidom_obj);

		das_obj.InitialState(moddyn_obj,res_obj,mpidom_obj);

		if(cycle==0)
			ens_obj.EnsembleInitial(das_obj,moddyn_obj,mpidom_obj);

		for(std::size_t iter=0; iter<(unsigned int)para.MaxIter_Outer(); iter++)
		{
			das_obj.ForecastState(moddyn_obj,mpidom_obj);

			ens_obj.EnsembleForecast(das_obj,moddyn_obj,mpidom_obj);

			// switch(das_obj.Algo_EnsembleUpdate_type())
			// {
				// case Parameter::Ensemble_Analysis:				

					ens_obj.EnsembleAnomaly(das_obj,mpidom_obj);

					ens_obj.EnsembleInnovation(das_obj, obssynth_obj.XobsENS(),mpidom_obj);

					if(loc_obj.Localization_type()==Parameter::Localize_Covariance){
						loc_obj.CorrelationMatrix(das_obj,mpidom_obj);
					}

					loc_obj.save_Pb0(ens_obj.HMSb(), mpidom_obj);

					// opt_obj.OptInner(ens_obj, obssynth_obj, loc_obj, mpidom_obj);

					// break;
			// }
		}
	}
	
	auto t_fin=Clock::now();

	if (mpidom_obj.myrank()==mpidom_obj.root())
		cout<<"time elapsed: "<<std::chrono::duration_cast<std::chrono::seconds>(t_fin-t_deb).count()<<" s"<<endl;

#if defined(_MPI) || defined(_MPI_simple)
	MPI_Barrier(MPI_COMM_WORLD);
	cout<<mpidom_obj.myrank()<<" this is mpi Btest!!!"<<endl;
	MPI_Finalize();
#else
	cout<<mpidom_obj.myrank()<<" this is NOT mpi Btest!!!"<<endl;
#endif

	return 0;
}