#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Eigen/Core>
#include <chrono>
#include "Data.hpp"

using namespace EnVar;
using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

using Clock=std::chrono::high_resolution_clock;

using Data2D=EnVar::DataBase<VectorXd>;
using Data3D=EnVar::DataBase<Data2D>;

int main(int argc, char *argv[])
{
	Data3D x;

	int nens=8;
	int nx=2;
	int ny=3;
	int t=3;

	int nsize=nx*ny;

	Data2D y,y1,y2;
	VectorXd v;
	// y.setZero(nsize, t);

	for (int i=0; i<t; i++){
		v=VectorXd::Ones(nsize)*i;
		y.setDataSlice(v);
		y1.setDataSlice(v*10);
	}

	// for (int n=0; n<nens; n++){
	// 	x.setDataSlice(y*n);
	// }

	// x.setData(y);

	y2=y+y1;

	y2.info();

	cout<<y2.at(2)<<endl;

	// x.info();

	// cout<<x.at(7,2)<<endl;

	// y=x*2.0;

	// z=y/5.0+x;

	// x+=z;

	// cout<<x.at(7)<<endl;

	// cout<<x.front()<<endl;

	// cout<<x.at(7)<<endl;

	// cout<<x.front()<<endl;


}