#include <Eigen/core>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using Eigen::MatrixXd;
using Eigen::VectorXd;


VectorXd unit_test_readfile_singlecolumn(std::string& str)
{
	std::ifstream ut_file(str);	
    std::vector<std::string> col;
    std::string  line,num;

    if(ut_file.is_open())
    {
        while(std::getline(ut_file,line)){
            std::istringstream is(line);
            if(is >> num){
                col.push_back(num);
            }
        }
    }
    else
    {
        std::cerr<<"Error opening file unit_test_file"<<std::endl;
    }

    VectorXd v;
    v.resize(col.size());

    for(std::size_t i=0; i<col.size(); i++)
    {
    	v(i)=std::stod(col.at(i));
    }

    return v;
}

int main(){

	VectorXd a,b,c,d;
	std::string str;

	str="data/hBC_l";

	a=unit_test_readfile_singlecolumn(str);

	std::cout<<"a\n"<<a<<std::endl;

	b=a.array()*a.array();

	std::cout<<"b\n"<<b<<std::endl;

	c=a.cwiseProduct(a);

	std::cout<<"c\n"<<c<<std::endl;

	d=c-b;

	std::cout<<"d\n"<<d<<std::endl;

	return 0;
}