#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include "mpi.h"

int malloc2dchar(char ***array, int n, int m) {

    /* allocate the n*m contiguous items */
    char *p = (char *)malloc(n*m*sizeof(char));
    if (!p) return -1;

    /* allocate the row pointers into the memory */
    (*array) = (char **)malloc(n*sizeof(char*));
    if (!(*array)) {
       free(p);
       return -1;
    }

    /* set up the pointers into the contiguous memory */
    for (int i=0; i<n; i++)
       (*array)[i] = &(p[i*m]);

    return 0;
}

int free2dchar(char ***array) {
    /* free the memory - the first element of the array is at the start */
    free(&((*array)[0][0]));

    /* free the pointers into the memory */
    free(*array);

    return 0;
}

void rowcol(int rank, const int blocks[2], int *row, int *col) {
    *row = rank/blocks[1];
    *col = rank % blocks[1];
}

int isLastRow(int row, const int blocks[2]) {
    return (row == blocks[0]-1);
}

int isLastCol(int col, const int blocks[2]) {
    return (col == blocks[1]-1);
}

int typeIdx(int row, int col, const int blocks[2]) {
    int lastrow = (row == blocks[0]-1);
    int lastcol = (col == blocks[1]-1);

    return lastrow*2 + lastcol;
}

int main(int argc, char **argv) {
    char **global, **local;
    const int gridsize=10; // size of grid
    const int procgridsize=2;  // size of process grid
    int rank, size;        // rank of current process and no. of processes

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size != procgridsize*procgridsize) {
        fprintf(stderr,"%s: Only works with np=%d for now\n", argv[0], procgridsize);
        MPI_Abort(MPI_COMM_WORLD,1);
    }

    int blocks[2]={0,0};
    MPI_Dims_create(size, 2, blocks);

    if (rank == 0) {
        /* fill in the array, and print it */
        malloc2dchar(&global, gridsize, gridsize);
        for (int i=0; i<gridsize; i++) {
            for (int j=0; j<gridsize; j++)
                global[i][j] = '0'+(3*i+j)%10;
        }


        printf("Global array is:\n");
        for (int i=0; i<gridsize; i++) {
            for (int j=0; j<gridsize; j++)
                putchar(global[i][j]);

            printf("\n");
        }
    }

    int localgridsize_x,localgridsize_y;
    int localgridsize_xv[4],localgridsize_yv[4];
    if (rank==0){
        localgridsize_x=3;
        localgridsize_y=3;

        localgridsize_xv[0]=3;
        localgridsize_xv[1]=3;
        localgridsize_xv[2]=7;
        localgridsize_xv[3]=7;
        localgridsize_yv[0]=3;
        localgridsize_yv[1]=7;
        localgridsize_yv[2]=3;
        localgridsize_yv[3]=7;

    }else if(rank==1){
        localgridsize_x=3;
        localgridsize_y=7;
    }else if(rank==2){
        localgridsize_x=7;
        localgridsize_y=3;
    }else if(rank==3){
        localgridsize_x=7;
        localgridsize_y=7;
    }else{
        fprintf(stderr,"%s: Only works with np=%d for now\n", argv[0], procgridsize);
        MPI_Abort(MPI_COMM_WORLD,1);
    }

    /* create the local array which we'll process */
    malloc2dchar(&local, localgridsize_x, localgridsize_y);

    int sendcounts[ size ];
    int senddispls[ size ];
    MPI_Datatype sendtypes[size];
    int recvcounts[ size ];
    int recvdispls[ size ];
    MPI_Datatype recvtypes[size];

    for (int proc=0; proc<size; proc++) {
        recvcounts[proc] = 0;
        recvdispls[proc] = 0;
        recvtypes[proc]  = MPI_CHAR;

        sendcounts[proc] = 0;
        senddispls[proc] = 0;
        sendtypes[proc]  = MPI_CHAR;
    }
    recvcounts[0] = localgridsize_x*localgridsize_y;
    recvdispls[0] = 0;
    /* create a datatype to describe the subarrays of the global array */

    int sizes[2]  = {gridsize, gridsize};         /* global size */
    MPI_Datatype type[4], subarrtype[4];
    
    if (rank == 0)
    {
        int subsizes[2];   /* local size */
        int starts[2] = {0,0};                        /* where this one starts */
        MPI_Aint lb, extent;

        for (int i=0; i<procgridsize; i++) {
            for (int j=0; j<procgridsize; j++) {
                subsizes[0]=localgridsize_xv[i*procgridsize+j];
                subsizes[1]=localgridsize_yv[i*procgridsize+j];
                MPI_Type_create_subarray(2, sizes, subsizes, starts, MPI_ORDER_C, MPI_CHAR, &subarrtype[2*i+j]);
                MPI_Type_commit(&subarrtype[2*i+j]);
            }
        }
    
        for (int proc=0; proc<size; proc++){
            int row, col;
            rowcol(proc, blocks, &row, &col);

            sendcounts[proc]=1;

            if (proc==0) senddispls[proc] = 0;
            if (proc==1) senddispls[proc] = ( col*localgridsize_yv[proc-1] )*sizeof(char);
            if (proc==2) senddispls[proc] = ( row*localgridsize_xv[proc-1]*sizes[1] )*sizeof(char);
            if (proc==3) senddispls[proc] = ( row*localgridsize_xv[proc-2]*sizes[1] + col*localgridsize_yv[proc-1] )*sizeof(char);

            std::cout<<"proc "<<proc<<" senddispls "<<senddispls[proc]<<std::endl;

            int idx = typeIdx(row, col, blocks);
            std::cout<<"proc "<<proc<<" idx "<<idx<<std::endl;
            sendtypes[proc]=subarrtype[idx];
        }
    }
    
    char *globalptr=NULL;
    if (rank == 0) globalptr = &(global[0][0]);

    MPI_Alltoallw(globalptr, sendcounts, senddispls, sendtypes,
                  &(local[0][0]), recvcounts, recvdispls, recvtypes, 
                  MPI_COMM_WORLD);

    for (int p=0; p<size; p++) {
        if (rank == p) {
            printf("Local process on rank %d is:\n", rank);
            for (int i=0; i<localgridsize_x; i++) {
                putchar('|');
                for (int j=0; j<localgridsize_y; j++) {
                    putchar(local[i][j]);
                }
                printf("|\n");
            }
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    /* or the MPI data type */
    // MPI_Type_free(&subarrtype);

    if (rank == 0) {
        printf("Processed grid:\n");
        for (int i=0; i<gridsize; i++) {
            for (int j=0; j<gridsize; j++) {
                putchar(global[i][j]);
            }
            printf("\n");
        }

        free2dchar(&global);
    }


    MPI_Finalize();

    return 0;
}