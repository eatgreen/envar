#include <iostream>
#include <vector>
#include <string>

using std::cout;
using std::endl;

class point {
public:
	int coord_x;
	int coord_y;
public:
	point(int x, int y):coord_x(x),coord_y(y) {}
};

class box {
public:
	int lx;
	int ly;
	point pt;
public:
	box(int x, int y, int a, int b):lx(x),ly(y),pt(a,b) {}
};

int main(){

	box b(5,6,2,3);
	cout<<b.lx<<endl;
	cout<<b.ly<<endl;
	cout<<b.pt.coord_x<<endl;
	cout<<b.pt.coord_y<<endl;
	return 1;
}