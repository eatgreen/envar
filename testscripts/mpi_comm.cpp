#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <mpi.h> 

using std::cout;
using std::endl;

int main(int argc, char *argv[])
{
	int myrank, size;
	MPI_Init(&argc, &argv);                   /* Initialize MPI       */
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);   /* Get my rank          */
    MPI_Comm_size(MPI_COMM_WORLD, &size);     /* Get the total number of processors */

    int ndims=2;
    int *dims,*coord,*periods;
    MPI_Comm        comm_cart, comm_cart2;

    dims    = (int*) malloc(ndims*sizeof(int));
    coord   = (int*) malloc(ndims*sizeof(int));
    periods = (int*) malloc(ndims*sizeof(int));

    dims[0]=2;
    dims[1]=2;

    periods[0]=0;
    periods[1]=0;

	cout<<"my rank: "<<myrank<<endl;

    MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, 0, &comm_cart); 

    MPI_Cart_coords(comm_cart, myrank, ndims, coord);

    int coord_x, coord_y;

    coord_x=coord[0];
    coord_y=coord[1];

    cout<<"my rank: "<<myrank<<" coord_x: "<<coord_x<<" coord_y: "<<coord_y<<endl;

    int h_glo=0;

    if (myrank==0)
    	h_glo=100;

    MPI_Bcast(&h_glo, 1, MPI_INT, 0, MPI_COMM_WORLD);

    cout<<"my rank: "<<myrank<<" h_glo: "<<h_glo<<endl;

    MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, 0, &comm_cart2); 

    MPI_Cart_coords(comm_cart2, myrank, ndims, coord);

    int coord_x2, coord_y2;

    coord_x2=coord[0];
    coord_y2=coord[1];

    cout<<"my rank: "<<myrank<<" coord_x2: "<<coord_x<<" coord_y2: "<<coord_y<<endl;

    int u_glo=0;

    if (myrank==0)
    	u_glo=100;

    MPI_Bcast(&u_glo, 1, MPI_INT, 0, comm_cart2);

    cout<<"my rank: "<<myrank<<" u_glo: "<<u_glo<<endl;

	MPI_Finalize();

	return 0;
}