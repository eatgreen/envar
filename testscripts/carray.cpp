#include <stdio.h>
#include <math.h>
#include <fftw3.h>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include "../src/pseudo2dM.hpp"
#include "../src/NetCDFIO.hpp"
#include <Eigen/core>

using Eigen::MatrixXd;
using namespace EnVar;

// #include "gnuplot_i.hpp" //Gnuplot class handles POSIX-Pipe-communikation with Gnuplot

// extern void pseudo2D_(double**, int*, int*, int*, double*, double*, double*, double*, int*, int*, double* );

// extern void newton2D_(double*, double*, int*, int*, double*, double*, double*, double*, bool& );

// extern void newtonfunc2D_(double*, double*, double*, double*, double*, double*, double*, double*, int*, int*, double*, double*, double*, double* );

int main(){

	// int i,j,m,n;
	// n=6;
	// m=3;

	// //test 1d array
	// int a[5];

	// for(i=0;i<5;i++)
	// {
	// 	a[i]=i;
	// 	printf("%d\n",a[i]);
	// }

	// //test 1d array pinter
	// int *b;
	// b=(int*) malloc(n*sizeof(int));

	// for(i=0;i<n;i++)
	// {
	// 	*(b+i)=n-i;
	// 	printf("%d\n",*(b+i));
	// }
	// free(b);

	// //test 2d array 
	// int c[n][m];
	// for(i=0;i<n;i++)
	// {
	// 	for(j=0;j<m;j++)
	// 	{
	// 		c[i][j]=j+m*i;
	// 		printf("%3d",c[i][j]);
	// 	}
	// 	printf("\n");
		
	// }

	// //test 2d array using 1d pointer
	// b=(int*) malloc(n*m*sizeof(int));

	// for(i=0;i<n;i++)
	// {
	// 	for(j=0;j<m;j++)
	// 	{
	// 		*(b+j+m*i)=j+m*i;
	// 		printf("%3d",*(b+j+m*i));
	// 	}
	// 	printf("\n");
		
	// }
	// free(b);


	// //test 2d array using 2d pointer use of [][]
	// int **d;

	// d=(int**) malloc(n*sizeof(int*));
	// for(i=0;i<n;i++)
	// {
	// 	d[i]=(int*) malloc(m*sizeof(int));
	// }

	// for(i=0;i<n;i++)
	// {
	// 	for(j=0;j<m;j++)
	// 	{
	// 		d[i][j]=j+m*i;
	// 		printf("%3d",d[i][j]);
	// 	}
	// 	printf("\n");
		
	// }

	// //test 2d array using 2d pointer use of **
	// for(i=0;i<n;i++)
	// {
	// 	for(j=0;j<m;j++)
	// 	{
	// 		*(j+*(d+i))=j+m*i;
	// 		printf("%3d",d[i][j]);
	// 	}
	// 	printf("\n");
		
	// }
	// free(d);

	// typedef test scalar

	// typedef double complex_number[2];

	// complex_number e;

	// e[0]=1;
	// e[1]=2;

	// printf("real %f, imag %f\n",e[0],e[1]);

	// // typedef test 1d array

	// complex_number* f;

	// f=(complex_number*) malloc(n*sizeof(complex_number));

	// for(i=0;i<n;i++)
	// {
	// 	*(f+i)[0]=i;
	// 	*(f+i)[1]=i+1;
	// 	printf("real %f, imag %f\n",*(f+i)[0],*(f+i)[1]);
	// }

	// // typedef test 2d array using 1d pointer

	// free(f);

	// f=(complex_number*) malloc(n*m*sizeof(complex_number));

	// for(i=0;i<n;i++)
	// {
	// 	for(j=0;j<n;j++)
	// 	{
	// 		*(f+i)[0]=i;
	// 		*(f+i)[1]=i+1;
	// 		printf("real %f, imag %f.",*(f+i)[0],*(f+i)[1]);
	// 	}
	// 	printf("\n");
	// }

	MpiDom 		  mpidom_obj(para);
	mpidom_obj.Init_domain();
	
	int nx=11, ny=26, lde=2, n1=nx, n2=ny, i, j;
	double rx=5.0, ry=5.0, dx=1.0, dy=1.0, theta=0;  

	// double* NoiseMat;

	//test fortran lib
	// pseudo2D_(&NoiseMat,&nx,&ny,&lde,&rx,&ry,&dx,&dy,&n1,&n2,&theta);


	// NoiseMat=EnVar::pseudo2dM(nx,ny,lde,rx,ry,dx,dy,n1,n2,theta);

	// Eigen::Map<MatrixXd> NoiseMat_eigen(NoiseMat,nx,ny);

	MatrixXd NoiseMat,NoiseMatT;


	NoiseMat.resize(nx*ny,lde);
	pseudo2dM(NoiseMat,ny,nx,lde,rx,ry,dx,dy,n2,n1,theta);

	NetCDFIO noise("noise", );
	noise.Create_File();

#ifdef TRANSPOSE
	NoiseMatT.resize(nx*ny,lde);
	pseudo2dM(NoiseMatT,ny,nx,lde,rx,ry,dx,dy,n2,n1,theta);

	// std::ofstream fileT("noise2DunsymT", std::ofstream::out);
	// if(fileT.is_open()){		
	// 		fileT<<NoiseMatT<<"\n";
	// 	}
	// fileT.close();
#endif

	// for(i=0;i<nx;i++){
	// 	for(j=0;j<ny;j++){
	// 		std::cout<<CorMat[j+i*ny]<<std::endl;
	// 	}
	// }

	// std::vector<int> x,y;
	// std::vector<double> z;

	// for(i=0;i<nx;i++){
	// 	x.push_back(i);
	// 	y.push_back(i);
	// 	z.push_back(*(CorMat+i));
	// }

	// Gnuplot gp("lines");

	// gp.set_xrange(0,nx);
	// gp.set_xrange(0,ny);

	// gp.plot_xy(x,z,"pseudo random fields");

	// gp.plot_xyz(x,y,z,"pseudo random fields");

	return 0;
}