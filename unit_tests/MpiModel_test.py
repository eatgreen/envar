from netCDF4 import Dataset
import matplotlib.pyplot as plt
import matplotlib.ticker as tick
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

name0="test_xp"
name1="test_h"
name2="test_u"
name3="test_v"
name4="test_hL"
name5="test_hR"	
name6="test_uL"
name7="test_uR"
name8="test_vL"
name9="test_vR"
namea="test_sb"
nameb="test_sf"

flagBC=False
flagX =True

file1x1=Dataset("../data/"+namea+"1x1.nc","r",format="NETCDF4")
file1x2=Dataset("../data/"+namea+"1x2.nc","r",format="NETCDF4")
file2x1=Dataset("../data/"+namea+"2x1.nc","r",format="NETCDF4")

if flagBC:
	filebc1x1=Dataset("../data/"+name9+"1x1.nc","r",format="NETCDF4")
	filebc1x2=Dataset("../data/"+name9+"1x21.nc","r",format="NETCDF4")
	filebc2x1=Dataset("../data/"+name9+"2x11.nc","r",format="NETCDF4")

if flagX:
	fileX1x1=Dataset("../data/test_sf1x1.nc","r",format="NETCDF4")
	fileX1x2=Dataset("../data/test_sf1x2.nc","r",format="NETCDF4")
	fileX2x1=Dataset("../data/test_sf2x1.nc","r",format="NETCDF4")

# state
h1x1 = file1x1.variables['h'][:,:]
h1x2 = file1x2.variables['h'][:,:]
h2x1 = file2x1.variables['h'][:,:]

fig = plt.figure(figsize=plt.figaspect(2.))
fig.subplots_adjust(left=0.1,right=0.95,hspace=0.38,wspace=0.25)

ax = fig.add_subplot(2, 1, 1, projection='3d')
# state
X = np.arange(0, 51, 1)
Y = np.arange(0, 21, 1)
X, Y = np.meshgrid(X, Y)

h1x1=np.transpose(h1x1)
h1x2=np.transpose(h1x2)
h2x1=np.transpose(h2x1)

h=h1x1-h1x2
ah=np.fabs(h)
max_ah=np.max(ah)

print(max_ah)

surf = ax.plot_surface(X, Y, ah, rstride=1, cstride=1, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

if flagX:

	hX1x1 = fileX1x1.variables['h'][-1,:,:]
	hX1x2 = fileX1x2.variables['h'][-1,:,:]
	hX2x1 = fileX2x1.variables['h'][-1,:,:]

	ax = fig.add_subplot(2, 1, 2, projection='3d')
	X = np.arange(0, 51, 1)
	Y = np.arange(0, 21, 1)
	X, Y = np.meshgrid(X, Y)

	hX1x1=np.transpose(hX1x1)
	hX1x2=np.transpose(hX1x2)
	hX2x1=np.transpose(hX2x1)

	h=hX1x1-hX1x2
	ah=np.fabs(h)
	max_ah=np.max(ah)

	print(max_ah)

	surf = ax.plot_surface(X, Y, ah, rstride=1, cstride=1, cmap=cm.coolwarm,
	                       linewidth=0, antialiased=False)

if flagBC:
	# bc x
	hbc1x1 = filebc1x1.variables['h'][0:27,:] #0
	hbc1x1 = filebc1x1.variables['h'][-26:,:] #1
	# bc y
	# hbc1x1 = filebc1x1.variables['h'][:,0:11] #0
	# hbc1x1 = filebc1x1.variables['h'][:,-12:] #1

	hbc1x2 = filebc1x2.variables['h'][:,:]
	hbc2x1 = filebc2x1.variables['h'][:,:]

	ax = fig.add_subplot(2, 1, 2, projection='3d')
	# bc x
	# X = np.arange(0, 27, 1)
	X = np.arange(0, 26, 1)
	Y = np.arange(0, 22, 1)
	# bc y
	# X = np.arange(0, 52, 1)
	# Y = np.arange(0, 11, 1)
	# Y = np.arange(0, 12, 1)
	X, Y = np.meshgrid(X, Y)

	hbc1x1=np.transpose(hbc1x1)
	hbc1x2=np.transpose(hbc1x2)
	hbc2x1=np.transpose(hbc2x1)

	print(hbc1x1.shape)
	print(hbc1x2.shape)
	print(hbc2x1.shape)

	h=hbc1x1-hbc1x2
	# h=hbc1x1-hbc2x1

	ah=np.fabs(h)
	max_ah=np.max(ah)

	print(max_ah)

	surf = ax.plot_surface(X, Y, ah, rstride=1, cstride=1, cmap=cm.coolwarm,
	                       linewidth=0, antialiased=False)

plt.show()
