#ifndef LOCALIZATION_TEST_HPP
#define LOCALIZATION_TEST_HPP

#include <boost/test/included/unit_test.hpp>
#include "../src/Localization.hpp"


using namespace EnVar;
using Eigen::MatrixXd;
using Eigen::VectorXd;

BOOST_AUTO_TEST_SUITE(Localization_tests)

struct F
{
	Parameter 	para;
	Localization 	loc_obj;
	Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;

	F():
		para(),
		loc_obj(para,assim_dyn)
	{
	}

	~F(){}
};

struct H
{
	Parameter 	para;
	Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;
	DataAssim 	  das_obj;
	Ensemble 	  ens_obj;
	Localization 	loc_obj;
	double ref=1e-6;

	H():
		para(),
		das_obj(para),
		ens_obj(para),
		loc_obj(para,assim_dyn)
	{
		loc_obj.CorrelationMatrix(das_obj);
		loc_obj.LocalizeCovariance(das_obj), ens_obj;
	}

	~H(){}
};

BOOST_FIXTURE_TEST_CASE(initialization_tesing, F){
	BOOST_CHECK_EQUAL(loc_obj.cod(), para.cod());
}

BOOST_FIXTURE_TEST_CASE(methods_testing, H){
	BOOST_CHECK_EQUAL(loc_obj.sqrt_Cor_mat_().rows(), loc_obj.nSize()*loc_obj.nState());
	BOOST_CHECK_EQUAL(loc_obj.sqrt_Cor_mat_().cols(), loc_obj.r());
	BOOST_CHECK_EQUAL(loc_obj.HMPb().data().size(), ens_obj.nens()*loc_obj.r());
	for(int i=0; i<=ens_obj.nens()*loc_obj.r(); i++){
		BOOST_CHECK_EQUAL(loc_obj.HMPb().data().at(i).data().size(), para.nobs()+1);
		for(int j=0; j<=para.nobs()+1; j++){
			BOOST_CHECK_EQUAL(loc_obj.HMPb().data().at(i).data().at(j).size(), loc_obj.nSize()*loc_obj.nState());
		}
	}
}


BOOST_AUTO_TEST_SUITE_END()
#endif