#ifndef ENSEMBLE_TEST_HPP
#define ENSEMBLE_TEST_HPP

#include <boost/test/included/unit_test.hpp>
#include "../src/Ensemble.hpp"

using namespace EnVar;
using Eigen::MatrixXd;
using Eigen::VectorXd;

BOOST_AUTO_TEST_SUITE(Ensemble_tests)

struct F
{
	Parameter 	para;
	Ensemble 	ens_obj;

	F():
		para(0),
		ens_obj(para)
	{
	}

	~F(){}
};

struct H
{
	Parameter 	  para;
	Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;
	Model 		  model_obj;
	DataAssim 	  das_obj;
	Ensemble 	  ens_obj;

	H():
		para(0),
		model_obj(para, assim_dyn),
		das_obj(para),
		ens_obj(para)
	{
		ens_obj.EnsembleInitial(das_obj,model_obj);
		ens_obj.EnsembleForecast(das_obj,model_obj);
		ens_obj.EnsembleAnomaly(das_obj);
	}

	~H(){}
};

BOOST_FIXTURE_TEST_CASE(initialization_tesing, F){
	BOOST_CHECK_EQUAL(ens_obj.nens(), para.nens());
}

BOOST_FIXTURE_TEST_CASE(methods_testing, H){
	BOOST_CHECK_EQUAL(ens_obj.Xens().data().size(), (std::size_t)para.nens()+1);
	for(int i=0; i<ens_obj.nens(); i++){
		BOOST_CHECK_EQUAL(ens_obj.Xens().data().at(i).data().size(), (std::size_t)para.nobs());
		for(int j=0; j<para.nobs()+1; j++){
		BOOST_CHECK_EQUAL(ens_obj.Xens().data().at(i).data().at(j).size(), para.nSize()*para.nState());
		}
	}

	BOOST_CHECK_EQUAL(ens_obj.HMSb().data().size(), (std::size_t)para.nens());
	for(int i=0; i<ens_obj.nens(); i++){
	BOOST_CHECK_EQUAL(ens_obj.HMSb().data().at(i).data().size(), (std::size_t)para.nobs());
		for(int j=0; j<para.nobs()+1; j++){
		BOOST_CHECK_EQUAL(ens_obj.HMSb().data().at(i).data().at(j).size(), para.nSize()*para.nState());
		}
	}
}


BOOST_AUTO_TEST_SUITE_END()
#endif