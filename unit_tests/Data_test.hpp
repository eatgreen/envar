#ifndef DATA_TEST_HPP
#define DATA_TEST_HPP

#include <boost/test/included/unit_test.hpp>
#include "../src/Data.hpp"

using namespace EnVar;
using Eigen::VectorXd;

using Data2D=EnVar::DataBase<VectorXd>;
using Data3D=EnVar::DataBase<Data2D>;

BOOST_AUTO_TEST_SUITE(Data_tests)

struct F
{
	Data2D data2d_obj;
	Data3D data3d_obj;

	VectorXd v0 = VectorXd::Zero(3);
	VectorXd v1 = VectorXd::Ones(3);

	F():
		data2d_obj(),
		data3d_obj()
	{
		data2d_obj.setDataSlice(v0);
		data3d_obj.setDataSlice(data2d_obj);
		data2d_obj.setDataSlice(v1);
		data3d_obj.setDataSlice(data2d_obj);
	}

	~F(){}
};

BOOST_FIXTURE_TEST_CASE(instantiation, F){
	// BOOST_CHECK( (data2d_obj.at(0)-v0).isMuchSmallerThan( VectorXd::Ones(3), 1e-6 ) );
	// BOOST_CHECK( (data3d_obj.at(0,0)-v0).isMuchSmallerThan( VectorXd::Ones(3), 1e-6 ) );
	// BOOST_CHECK( (data2d_obj.at(1)-v1).isMuchSmallerThan( VectorXd::Ones(3), 1e-6 ) );
	// BOOST_CHECK( (data3d_obj.at(1,0)-v0).isMuchSmallerThan( VectorXd::Ones(3), 1e-6 ) );
	// BOOST_CHECK( (data3d_obj.at(1,1)-v1).isMuchSmallerThan( VectorXd::Ones(3), 1e-6 ) );

	BOOST_CHECK_EQUAL(data2d_obj.size().at(0), unsigned(2));
	BOOST_CHECK_EQUAL(data2d_obj.size().at(1), unsigned(3));
	BOOST_CHECK_EQUAL(data3d_obj.size().at(0), unsigned(2));
	BOOST_CHECK_EQUAL(data3d_obj.size().at(1), unsigned(1));
	BOOST_CHECK_EQUAL(data3d_obj.size().at(2), unsigned(3));
}

BOOST_AUTO_TEST_SUITE_END()

#endif