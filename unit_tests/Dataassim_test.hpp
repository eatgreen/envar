#ifndef DATAASSIM_TEST_HPP
#define DATAASSIM_TEST_HPP

#include <boost/test/included/unit_test.hpp>
#include "../src/DataAssim.hpp"

using namespace EnVar;
using Eigen::MatrixXd;
using Eigen::VectorXd;

BOOST_AUTO_TEST_SUITE(DataAssim_tests)

struct F
{
	Parameter 	  para;
	DataAssim 	  das_obj;

	F():
		para(0),
		das_obj(para)
	{
	}

	~F(){}
};

struct G
{
	Parameter 	para;
	Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;
	Model 		model_obj;
	DataAssim 	  das_obj;
    Eigen::IOFormat    HeavyFmt_;

	G():
		para(0),
		model_obj(para, assim_dyn),
		das_obj(para),
		HeavyFmt_(Eigen::FullPrecision, 0)
	{
		para.info();

		das_obj.BackgroundState(model_obj);

		std::ofstream file0("data/xb0", std::ofstream::out);
		if(file0.is_open()){
			file0<<das_obj.sb().format(HeavyFmt_);
			file0.close();
		}

		std::ofstream file1("data/xbf", std::ofstream::out);
		if(file1.is_open()){
			file1<<das_obj.Xb().data().back().format(HeavyFmt_);
			file1.close();
		}
	}

	~G(){}
};

BOOST_FIXTURE_TEST_CASE(initialization_testing, F){
	BOOST_CHECK_EQUAL(das_obj.nobs(), para.nobs());
	BOOST_CHECK_EQUAL(das_obj.tmax(), para.tmax());
}

BOOST_FIXTURE_TEST_CASE(methods_testing, G){
	BOOST_CHECK_EQUAL(das_obj.Xb().data().size(), (std::size_t)(para.tmax()/para.dt())+1);
}

BOOST_AUTO_TEST_SUITE_END()
#endif