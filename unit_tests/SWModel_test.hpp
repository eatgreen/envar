#ifndef MODEL_TEST_HPP
#define MODEL_TEST_HPP

#include <boost/test/included/unit_test.hpp>
#include "../src/Model.hpp"

using namespace EnVar;
using Eigen::MatrixXd;
using Eigen::VectorXd;

BOOST_AUTO_TEST_SUITE(Model_tests)

struct F
{
	Parameter 	para;
	Model 		model_obj;
	Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;
	
	F(void):
		model_obj(para, assim_dyn)
	{
	}

	~F(void){}
};

struct G
{
	Parameter 	para;

	Model 		model_obj;
	Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;
    Eigen::IOFormat    HeavyFmt_;

	std::string path;
	VectorXd    res,x0,x1,res_adv,MQx,k1,MQx2,k2;
	std::vector<MatrixXd> RoeF,RoeG,hBC,uBC,vBC;
	MatrixXd    h;
	bool 		flagX=1;
	
	G(void):
		model_obj(para, assim_dyn),
		HeavyFmt_(Eigen::FullPrecision, 0)
	{
		para.info();

		path=std::string("data/k1");
		k1=unit_test_readfile_singlecolumn(path);

		MQx=model_obj.Physical2Movement(para.sb());

	    MQx2=MQx+0.5*model_obj.dt()*k1;

		res_adv=model_obj.Advection(MQx2);

		std::ofstream filek("data/k2_k1", std::ofstream::out);
		if(filek.is_open()){
			filek<<res_adv.format(HeavyFmt_);
			filek.close();
		}

		// hBC=model_obj.set_hBC(para.hb());
		// uBC=model_obj.set_uBC(para.ub());
		// vBC=model_obj.set_vBC(para.vb());

		// RoeF=model_obj.RoeFlux(hBC.at(0),hBC.at(1),uBC.at(0),uBC.at(1),vBC.at(0),vBC.at(1),flagX);

		// std::ofstream fileF("data/Fh", std::ofstream::out);
		// if(fileF.is_open()){
		// 	fileF<<RoeF.at(0).format(HeavyFmt_);
		// 	fileF.close();
		// }

		// std::ofstream fileFu("data/Fu", std::ofstream::out);
		// if(fileFu.is_open()){
		// 	fileFu<<RoeF.at(1).format(HeavyFmt_);
		// 	fileFu.close();
		// }

		// std::ofstream fileFv("data/Fv", std::ofstream::out);
		// if(fileFv.is_open()){
		// 	fileFv<<RoeF.at(2).format(HeavyFmt_);
		// 	fileFv.close();
		// }

	}

	~G(void){}
};

BOOST_FIXTURE_TEST_CASE(initialization_model, F){
	BOOST_CHECK_EQUAL(model_obj.g(), 9.806);
	BOOST_CHECK_EQUAL(model_obj.orderT(), 3);
}

BOOST_FIXTURE_TEST_CASE(get_variable_model, F){
	BOOST_CHECK_EQUAL(get_h(para.x0(),model_obj),para.h0());
	BOOST_CHECK_EQUAL(get_u(para.x0(),model_obj),para.u0());
	BOOST_CHECK_EQUAL(get_v(para.x0(),model_obj),para.v0());
	BOOST_CHECK_EQUAL(get_ctl_vector(para.h0(),para.u0(),para.v0(),model_obj),para.x0());
}

BOOST_FIXTURE_TEST_CASE(methods_testing, G){
	// BOOST_CHECK( (para.sb()-x0).isMuchSmallerThan( VectorXd::Ones(para.sb().size()), 1e-6 ) );
	// BOOST_CHECK( (res-x1).isMuchSmallerThan( VectorXd::Ones(para.sb().size()), 1e-3 ) );
	// BOOST_CHECK( (res_adv-k1).isMuchSmallerThan( VectorXd::Ones(para.sb().size()), 1e-3 ) );
}


BOOST_AUTO_TEST_SUITE_END()

#endif