#ifndef OPTIMIZATION_TEST_HPP
#define OPTIMIZATION_TEST_HPP

#include <boost/test/included/unit_test.hpp>
#include "../src/Optimization.hpp"

using namespace EnVar;
using Eigen::MatrixXd;
using Eigen::VectorXd;

BOOST_AUTO_TEST_SUITE(Optimization_tests)

struct Field_test
{
	Parameter 	    para;
	Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;

	Model 		 	moddyn_obj;
	Observation 	obssynth_obj;
	Ensemble     	ens_obj;
	Localization  	loc_obj;
	Optimization   	opt_obj;

	Field_test():
		para(0),
		moddyn_obj(para, assim_dyn),
		obssynth_obj(para, assim_dyn),
		ens_obj(para),
		loc_obj(para, assim_dyn),
		opt_obj(para)
	{

	}

	~Field_test(){}
};

//Pseudo_test_case1-3 run parameters are missing, not working so far.
// struct Pseudo_test_case1
// {
// 	Parameter 	   para;
// 	Ensemble       ens_obj;

// 	Optimization   opt_obj;

// 	VectorXd	   Xopt;
// 	std::string    Xoptpath;

// 	Pseudo_test_case1():
// 		para(1),
// 		ens_obj(para),
// 		opt_obj(para,1)
// 	{
// 		Xoptpath="unit_tests/unit_tests_data/opt_test1/Xopt";
// 		opt_obj.OptSingle();
// 		Xopt=unit_test_readfile_singlecolumn(Xoptpath);
// 	}

// 	~Pseudo_test_case1(){}
// };

// struct Pseudo_test_case2
// {
// 	Parameter 	   para;
// 	Ensemble       ens_obj;

// 	Optimization   opt_obj;

// 	VectorXd	   Xopt;
// 	std::string    Xoptpath;

// 	Pseudo_test_case2():
// 		para(2),
// 		ens_obj(para),
// 		opt_obj(para,2)
// 	{
// 		Xoptpath="unit_tests/unit_tests_data/opt_test2/Xopt";
// 		opt_obj.OptSingle();
// 		Xopt=unit_test_readfile_singlecolumn(Xoptpath);
// 	}

// 	~Pseudo_test_case2(){}
// };

// struct Pseudo_test_case3
// {
// 	Parameter 	   para;
// 	Ensemble       ens_obj;

// 	Optimization   opt_obj;

// 	VectorXd	   Xopt;
// 	std::string    Xoptpath;

// 	Pseudo_test_case3():
// 		para(3),
// 		ens_obj(para),
// 		opt_obj(para,3)
// 	{
// 		Xoptpath="unit_tests/unit_tests_data/opt_test3/Xopt";
// 		opt_obj.OptSingle();
// 		Xopt=unit_test_readfile_singlecolumn(Xoptpath);
// 	}

// 	~Pseudo_test_case3(){}
// };

struct Real_test_case4
{
	Parameter 	para;

	Parameter::ModelSpace_Type synth_obs=Parameter::obs_space;
	Model 		   modsynth_obj;
	Observation    obssynth_obj;

	DataAssim 	   das_obj;
	Ensemble 	   ens_obj;
	Optimization   opt_obj;

	Parameter::ModelSpace_Type assim_dyn=Parameter::dyn_space;
	Model 		   moddyn_obj;
	Localization   loc_obj;

	VectorXd	   Xopt;
	std::string    Xoptpath;

	Real_test_case4():
		para(4),
		modsynth_obj(para, synth_obs),
		obssynth_obj(para, synth_obs),
		das_obj(para),
		ens_obj(para),
		opt_obj(para),
		moddyn_obj(para, assim_dyn),
	    loc_obj(para, assim_dyn)
	{
		para.info();

		make_obssynth(obssynth_obj, das_obj, modsynth_obj, ens_obj);

		for(std::size_t cycle=0; cycle<(unsigned int)das_obj.Assimilation_cycle(); cycle++)
		{
			das_obj.setCycleIndex(cycle);

			if(cycle==0)
				das_obj.BackgroundState(moddyn_obj);

			das_obj.InitialState(moddyn_obj);

			if(cycle==0)
				ens_obj.EnsembleInitial(das_obj,moddyn_obj);

			for(std::size_t iter=0; iter<(unsigned int)para.MaxIter_Outer(); iter++)
			{
				das_obj.ForecastState(moddyn_obj);

				ens_obj.EnsembleForecast(das_obj,moddyn_obj);

				switch(das_obj.Algo_EnsembleUpdate_type())
				{
					case Parameter::Ensemble_Analysis:				

						ens_obj.EnsembleAnomaly(das_obj);

						ens_obj.EnsembleInnovation(das_obj, obssynth_obj.XobsENS());

						if(loc_obj.Localization_type()==Parameter::Localize_Covariance)
							loc_obj.LocalizeCovariance(das_obj,ens_obj);	

						opt_obj.OptPrepare(ens_obj, obssynth_obj, loc_obj);

						opt_obj.OptSingle();

						break;

					case Parameter::Ensemble_Transform:
						break;

					// default:
				}
			}
		}

		// das_obj.UpdateInitialState(opt_obj.delta_x());

		// das_obj.AnalysisState(moddyn_obj);

		Xoptpath="unit_tests/unit_tests_data/opt_test4/Xopt";
		Xopt=unit_test_readfile_singlecolumn(Xoptpath);
	}

	~Real_test_case4(){}
};

// BOOST_FIXTURE_TEST_CASE(initialization_tesing, Field_test){
// 	BOOST_CHECK_EQUAL(opt_obj.nCtlVec(), ens_obj.nens());
// }

// BOOST_FIXTURE_TEST_CASE(test_case1_methods_tesing, Pseudo_test_case1){
// 	BOOST_CHECK( (opt_obj.delta_z()-Xopt).isMuchSmallerThan( VectorXd::Ones(opt_obj.nCtlVec()), 1e-6 ) );
// }

// BOOST_FIXTURE_TEST_CASE(test_case2_methods_tesing, Pseudo_test_case2){
// 	BOOST_CHECK( (opt_obj.delta_z()-Xopt).isMuchSmallerThan( VectorXd::Ones(opt_obj.nCtlVec()), 1e-1 ) );
// }

// BOOST_FIXTURE_TEST_CASE(test_case3_methods_tesing, Pseudo_test_case3){
// 	BOOST_CHECK( (opt_obj.delta_z()-Xopt).isMuchSmallerThan( VectorXd::Ones(opt_obj.nCtlVec()), 1e-6 ) );
// }

BOOST_FIXTURE_TEST_CASE(test_case4_methods_tesing, Real_test_case4){
	BOOST_CHECK( (opt_obj.delta_z()-Xopt).isMuchSmallerThan( VectorXd::Ones(opt_obj.nCtlVec()), 1e-6 ) );
}

BOOST_AUTO_TEST_SUITE_END()
#endif