#ifndef OBServation_TEST_HPP
#define OBServation_TEST_HPP

#include <boost/test/included/unit_test.hpp>
#include "../src/Observation.hpp"

using namespace EnVar;
using Eigen::MatrixXd;
using Eigen::VectorXd;

BOOST_AUTO_TEST_SUITE(Observation_tests)

struct F
{
	Parameter 		para;
	Parameter::ModelSpace_Type synth_obs=Parameter::obs_space;
	Observation 	obssynth_obj;

	F():
		para(),
		obssynth_obj(para, synth_obs)
	{
	}

	~F(){}
};

struct H
{
	Parameter 		para;
	Parameter::ModelSpace_Type synth_obs=Parameter::obs_space;
	Model        	modsynth_obj;
	DataAssim 	 	das_obj;
	Ensemble 	 	ens_obj;
	Observation 	obssynth_obj;

	VectorXd x0,x1,xf;
	std::string path;

	H():
		para(),
		modsynth_obj(para, synth_obs),
		das_obj(para),
		ens_obj(para),
		obssynth_obj(para, synth_obs)
	{
		path=std::string("unit_tests/unit_tests_data/obs_test1/x0");
		x0=unit_test_readfile_singlecolumn(path);

		path=std::string("unit_tests/unit_tests_data/obs_test1/x1");
		x1=unit_test_readfile_singlecolumn(path);

		path=std::string("unit_tests/unit_tests_data/obs_test1/xf");
		xf=unit_test_readfile_singlecolumn(path);

		obssynth_obj.make_true_solution(das_obj,modsynth_obj);
		obssynth_obj.make_obs(das_obj);
		obssynth_obj.make_invR(das_obj);
		obssynth_obj.make_obsens(das_obj,ens_obj);
	}

	~H(){}
};

BOOST_FIXTURE_TEST_CASE(initialization_tesing, F){
	BOOST_CHECK_EQUAL(obssynth_obj.x0().size(), para.x0().size());
}

BOOST_FIXTURE_TEST_CASE(methods_testing, H){
	BOOST_CHECK_EQUAL(obssynth_obj.Xt().data().size(), para.ittru_sum());
	for(int i=0; i<para.ittru_sum(); i++){
		BOOST_CHECK_EQUAL(obssynth_obj.Xt().data().at(i).size(), para.nSize()*para.nState());
	}

	BOOST_CHECK_EQUAL(obssynth_obj.Xobs().data().size(), para.itobs_sum());
	for(int i=0; i<para.itobs_sum(); i++){
		BOOST_CHECK_EQUAL(obssynth_obj.Xobs().data().at(i).size(), para.nSize()*para.nState());
	}

	BOOST_CHECK_EQUAL(obssynth_obj.XobsENS().data().size(), (unsigned int)para.nens()+1);
	for(int i=0; i<para.nens()+1; i++){
		BOOST_CHECK_EQUAL(obssynth_obj.XobsENS().data().at(i).data().size(), para.itobs_sum());
		for(int j=0; j<para.itobs_sum(); j++){
		BOOST_CHECK_EQUAL(obssynth_obj.XobsENS().data().at(j).data().at(j).size(), para.nSize()*para.nState());
		}
	}

	BOOST_CHECK( (obssynth_obj.x0()-x0).isMuchSmallerThan( VectorXd::Ones(obssynth_obj.x0().size()), 1e-6 ) );

	BOOST_CHECK( (obssynth_obj.Xt().data().at(1)-x1).isMuchSmallerThan( VectorXd::Ones(obssynth_obj.x0().size()), 1e-6 ) );

	BOOST_CHECK( (obssynth_obj.Xt().data().back()-xf).isMuchSmallerThan( VectorXd::Ones(obssynth_obj.x0().size()), 1e-6 ) );

}


BOOST_AUTO_TEST_SUITE_END()
#endif