#ifndef PARAMETER_TEST_HPP
#define PARAMETER_TEST_HPP

#include <boost/test/included/unit_test.hpp>
#include "../src/Parameter.hpp"

using namespace EnVar;

BOOST_AUTO_TEST_SUITE(Parameter_tests)

struct F
{
	Parameter 	para;

	F():
		para()
	{
		para.info();
	}

	~F(){}
};

BOOST_FIXTURE_TEST_CASE(initialization_parameter, F){
	BOOST_CHECK_EQUAL(para.dtobs(), 150);
	BOOST_CHECK_EQUAL(para.nobs(), 3);
	BOOST_CHECK_EQUAL(para.tobs_assim(), 450);
	BOOST_CHECK_EQUAL(para.t_offset(), 6000);
	BOOST_CHECK_EQUAL(para.t_assim_deb().at(0), 6000);
	BOOST_CHECK_EQUAL(para.t_assim_fin().at(0), 6450);
}

BOOST_AUTO_TEST_SUITE_END()

#endif