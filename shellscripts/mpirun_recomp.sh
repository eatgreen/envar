#!/bin/sh
export COMPILER='mpicxx'

export COMPILE_FLAGS='-c -O2 -Wall -Wno-unused-variable -Wno-unused-parameter -std=c++11 -I/usr/local/include/eigen3 -Iinclude/spectra-0.5.0/include -D MPI -D TIME'

make clean && make -j 4 mpitestexe

export NPX=2
export NPY=2

proc_size=$((NPX*NPY))

mpirun -np $proc_size mpitestexe
