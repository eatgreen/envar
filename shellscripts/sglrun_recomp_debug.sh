#!/bin/sh

export COMPILER='g++-6'

export COMPILE_FLAGS='-c -g -Wall -Wno-unused-variable -Wno-unused-parameter -std=c++11 -I/usr/local/Cellar/eigen/3.3.4/include/eigen3 -D DEBUG'
export NPX=1
export NPY=1

make clean && make -j 4 mpitest

./mpitest
