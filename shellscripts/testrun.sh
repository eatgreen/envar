#!/bin/sh
export COMPILER='mpicxx'

export COMPILE_FLAGS='-c -g -Wall -Wno-unused-variable -Wno-unused-parameter -std=c++11 -I/usr/local/include/eigen3 -D _MPI'
export NPX=2
export NPY=2

proc_size=$((NPX*NPY))

make clean && make -j 4 testexe

mpirun -np $proc_size testexe
