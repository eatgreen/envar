#!/bin/sh
export COMPILER='g++-6'

export COMPILE_FLAGS='-c -O2 -Wall -Wno-unused-variable -Wno-unused-parameter -std=c++11 -I/usr/local/include/eigen3 -Iinclude/spectra-0.5.0/include'
export NPX=1
export NPY=1

make clean && make -j 4 mpitestexe
./mpitestexe

# make clean && make -j 4 Btestexe
# ./Btest
