#!/bin/sh

export COMPILE_FLAGS='-c -g -Wall -std=c++11 -I/usr/local/Cellar/eigen/3.3.4/include/eigen3 -D MPI -D DEBUG'
export NPX=2
export NPY=2

proc_size=$((NPX*NPY))

make clean && make mpitest

mpirun -np $proc_size mpitest