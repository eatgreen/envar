#ifndef SHALLOWWATERMODEL
#define SHALLOWWATERMODEL

#include <Eigen/Core>
#include <iostream>
#include <vector>
#include <string>
#include <chrono>
#include <unordered_map>
#include <cstdlib>
#include "MpiDom.hpp"
#include "ModelPara.hpp"

using Clock=std::chrono::high_resolution_clock;

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

class ShallowWaterModel
{
private:
    int                          Obs_type_;
    DimPara::Dim_Type            Dim_type_;
    ModelPara::DynStep_Type      DynStep_type_;
    ModelPara::Flux_Type         Flux_type_;
    ModelPara::BC_Type           BC_type_;
    double                       g_;
    int                          orderT_;
    double                       dt_;
    int                          nState_;
    bool                         LocUncert_flag_;
    int                          LocUncert_combo_;
    bool                         WhiteNoise_flag_;
    bool                         Viscosity_flag_;
    double                       miu_;
    bool                         Topo_flag_;
    bool                         Coriolis_flag_;
    double                       f_cori_;
    bool                         Bottom_friction_flag_;

    double                       Lx_;
    double                       Ly_;

    int                          nx_;
    int                          ny_;
    double                       dx_;
    double                       dy_;
    MatrixXd                     xx_;
    MatrixXd                     yy_;
    int                          nSize_;

    int                          proc_ni_;
    int                          proc_nj_;

public:
	ShallowWaterModel();
	~ShallowWaterModel();

    //Constructor
    ShallowWaterModel(ModelPara&, SWMpiDom&);

	//Method
    VectorXd                TimeIntegration2D(const double*, const int, bool, SWMpiDom&);

    //getters
    const int& Obs_type(void) const { return Obs_type_; }
    const DimPara::Dim_Type& Dim_type(void) const { return Dim_type_; }
    const ModelPara::DynStep_Type& DynStep_type(void) const { return DynStep_type_; }
    const ModelPara::Flux_Type& Flux_type(void) const { return Flux_type_; }
    const ModelPara::BC_Type& BC_type(void) const { return BC_type_; }
    const double&  g(void) const { return g_; }
    const int&  orderT(void) const { return orderT_; }
    const double&  Lx(void) const { return Lx_; }
    const double&  Ly(void) const { return Ly_; }
    const int& nx(void) const { return nx_; }
    const int& ny(void) const { return ny_; }
    const double&  dt(void) const { return dt_; }
    const double&  dx(void) const { return dx_; }
    const double&  dy(void) const { return dy_; }
    const MatrixXd& xx(void) const { return xx_; }
    const MatrixXd& yy(void) const { return yy_; }
    const int& nState(void) const{ return nState_; }
    const int& nSize(void) const{ return nSize_; }
    const bool&  LocUncert_flag(void) const { return LocUncert_flag_; }
    const int&  LocUncert_combo(void) const { return LocUncert_combo_; }
    const bool&  WhiteNoise_flag(void) const { return WhiteNoise_flag_; }
    const bool&  Viscosity_flag(void) const { return Viscosity_flag_; }
    const double&  miu(void) const { return miu_; }
    const bool&  Topo_flag(void) const { return Topo_flag_; }
    const bool&  Coriolis_flag(void) const { return Coriolis_flag_; }
    const double&  f_cori(void) const { return f_cori_; }
    const bool&  Bottom_friction_flag(void) const { return Bottom_friction_flag_; }
    const int& proc_ni(void) const { return proc_ni_; }
    const int& proc_nj(void) const { return proc_nj_; }

private:
    VectorXd                Advection(VectorXd&, bool, SWMpiDom&);
    std::vector<MatrixXd>   RoeFlux(const MatrixXd&, const MatrixXd&, const MatrixXd&, const MatrixXd&, const MatrixXd&, const MatrixXd&, bool&);
    VectorXd                Physical2Movement(const VectorXd&);
    VectorXd                InvPhysical2Movement(const VectorXd&);

    class BC
    {
    public:
        BC(){};
        ~BC(){};

    private:
        MatrixXd WBC_,EBC_,NBC_,SBC_;

    public:
        const MatrixXd WBC(void) const{ return WBC_;}
        const MatrixXd EBC(void) const{ return EBC_;}
        const MatrixXd NBC(void) const{ return NBC_;}
        const MatrixXd SBC(void) const{ return SBC_;}

        void setWBC(MatrixXd foo){ WBC_=foo; };
        void setEBC(MatrixXd foo){ EBC_=foo; };
        void setNBC(MatrixXd foo){ NBC_=foo; };
        void setSBC(MatrixXd foo){ SBC_=foo; };
    };
    BC                      set_BC(const MatrixXd&, std::string, SWMpiDom&);

private:

    bool stob(std::string str){
        std::istringstream is(str);
        bool b;
        is>>std::boolalpha>>b;
        return b;
    }
};

MatrixXd      get_h(const VectorXd&, int&, int&);
MatrixXd      get_u(const VectorXd&, int&, int&);
MatrixXd      get_v(const VectorXd&, int&, int&);
VectorXd      get_ctl_vector(const MatrixXd&, const MatrixXd&, const MatrixXd&);
void          cout_huv(VectorXd&, int&, int&);
MatrixXd      get_h(VectorXd&&, int&, int&);
MatrixXd      get_u(VectorXd&&, int&, int&);
MatrixXd      get_v(VectorXd&&, int&, int&);
VectorXd      get_ctl_vector(MatrixXd&&, MatrixXd&&, MatrixXd&&);
void          cout_huv(VectorXd&&, int&, int&);
    
#endif 