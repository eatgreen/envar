/********************************************************************

    SWMpiDom.cpp

*********************************************************************/

/**
    \file SWMpiDom.cpp
    \brief SWMpiDom stucture implememtation
*/

#include "SWMpiDom.hpp"

SWMpiDom::SWMpiDom(void){
}

SWMpiDom::~SWMpiDom(void){
}

SWMpiDom::SWMpiDom(DimPara& dimpara_obj)
{
    Lx_=dimpara_obj.Lx();
    Ly_=dimpara_obj.Ly();
    nx_glo_=dimpara_obj.nx();
    ny_glo_=dimpara_obj.ny();

    nState_=dimpara_obj.nState();

    nSize_glo_=nx_glo_*ny_glo_;

    proc_ni_=dimpara_obj.proc_ni();
    proc_nj_=dimpara_obj.proc_nj();

    dx_=dimpara_obj.dx();
    dy_=dimpara_obj.dy();

    x_glo_=dimpara_obj.x();
    y_glo_=dimpara_obj.y();

    xx_glo_=dimpara_obj.xx();
    yy_glo_=dimpara_obj.yy();
}

//! A member
/*!
    Initialize local domain index for corresponding decomposed domain
*/

void SWMpiDom::Init_domain(){
#ifdef MPI_simple
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank_);
    MPI_Comm_size(MPI_COMM_WORLD, &size_);    
    root_=0;

    cout<<"my rank is   "<<myrank_<<endl;
    cout<<"root rank is "<<root_<<endl;

    std::fesetround(FE_TONEAREST);
    nx_div_=(int)std::rint(nx_glo_/proc_ni_);
    ny_div_=(int)std::rint(ny_glo_/proc_nj_);

    nx_div_mod_=nx_glo_-nx_div_*(proc_ni_-1);
    ny_div_mod_=ny_glo_-ny_div_*(proc_nj_-1);

    if( (myrank_+1)%proc_nj_==0 && myrank_!=size_-1 ){
        nx_loc_=nx_div_;
        ny_loc_=ny_div_mod_;
        ix_loc_=(myrank_/proc_nj_)*nx_div_;
        iy_loc_=ny_-ny_div_mod_;
    }
    else if( (myrank_/proc_nj_)==proc_ni_-1 && myrank_!=size_-1 ){
        nx_loc_=nx_div_mod_;
        ny_loc_=ny_div_;
        ix_loc_=nx_-nx_div_mod_;    
        iy_loc_=(myrank_-proc_nj_*(myrank_/proc_nj_))*ny_div_;
    }
    else if(myrank_==size_-1){
        nx_loc_=nx_div_mod_;
        ny_loc_=ny_div_mod_;
        ix_loc_=nx_-nx_loc_;
        iy_loc_=ny_-ny_loc_;
    }
    else{
        nx_loc_=nx_div_;
        ny_loc_=ny_div_;
        ix_loc_=(myrank_/proc_nj_)*nx_div_;
        iy_loc_=(myrank_-proc_nj_*(myrank_/proc_nj_))*ny_div_;
    }

    nSize_loc_=nx_loc_*ny_loc_;

    cout<<"Rank "<<myrank_<<" nx_loc "<<nx_loc_<<endl;
    cout<<"Rank "<<myrank_<<" ny_loc "<<ny_loc_<<endl;
    cout<<"Rank "<<myrank_<<" ix_loc "<<ix_loc_<<endl;
    cout<<"Rank "<<myrank_<<" iy_loc "<<iy_loc_<<endl;

    int    *ix_loc_array = nullptr;
    int    *iy_loc_array = nullptr;
    int    *nx_loc_array = nullptr;
    int    *ny_loc_array = nullptr;

    if (myrank_==0){
        ix_loc_array = new int[ size_ ];
        iy_loc_array = new int[ size_ ];
        nx_loc_array = new int[ size_ ];
        ny_loc_array = new int[ size_ ];
    }

    MPI_Gather(&ix_loc_, 1, MPI_INT, ix_loc_array, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather(&iy_loc_, 1, MPI_INT, iy_loc_array, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather(&nx_loc_, 1, MPI_INT, nx_loc_array, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather(&ny_loc_, 1, MPI_INT, ny_loc_array, 1, MPI_INT, 0, MPI_COMM_WORLD);

    if (myrank_==root_){

        ix_loc_vec_.assign(ix_loc_array, ix_loc_array + size_ );
        iy_loc_vec_.assign(iy_loc_array, iy_loc_array + size_ );
        nx_loc_vec_.assign(nx_loc_array, nx_loc_array + size_ );
        ny_loc_vec_.assign(ny_loc_array, ny_loc_array + size_ );

        delete[] ix_loc_array;
        delete[] iy_loc_array;
        delete[] nx_loc_array;
        delete[] ny_loc_array;
    }

#elif MPI
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank_);
    MPI_Comm_size(MPI_COMM_WORLD, &size_);   
    root_=0;
    
    int ndims=2;
    int *dims,*coord,*periods;

    dims    = (int*) malloc(ndims*sizeof(int));
    coord   = (int*) malloc(ndims*sizeof(int));
    periods = (int*) malloc(ndims*sizeof(int));

    //Block over x and y direction
    dims[0]=proc_ni_;
    dims[1]=proc_nj_;

    //no periodic BC
    periods[0]=0;
    periods[1]=0;

    MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, 0, &comm_cart_); 

    MPI_Cart_coords(comm_cart_, myrank_, ndims, coord);

    coord_x_=coord[0];
    coord_y_=coord[1];

    std::fesetround(FE_TONEAREST);
    nx_div_=(int)std::rint((double)(nx_glo_)/(double)(proc_ni_));
    ny_div_=(int)std::rint((double)(ny_glo_)/(double)(proc_nj_));

    nx_div_mod_=nx_glo_-nx_div_*(proc_ni_-1);
    ny_div_mod_=ny_glo_-ny_div_*(proc_nj_-1);

    ix_loc_=coord[0]*nx_div_;
    iy_loc_=coord[1]*ny_div_;

    if ( coord[0] == proc_ni_-1 && coord[1] != proc_nj_-1 )
    {
        nx_loc_=nx_div_mod_;
        ny_loc_=ny_div_;
    }
    else if ( coord[0] != proc_ni_-1 && coord[1] == proc_nj_-1 )
    {
        nx_loc_=nx_div_;
        ny_loc_=ny_div_mod_;
    }
    else if ( coord[0] == proc_ni_-1 && coord[1] == proc_nj_-1 )
    {
        nx_loc_=nx_div_mod_;
        ny_loc_=ny_div_mod_;
    }
    else
    {
        nx_loc_=nx_div_;
        ny_loc_=ny_div_;
    }

    nSize_loc_=nx_loc_*ny_loc_;

    cout<<"my rank: "<<myrank_<<" coord_x: "<<coord_x_<<" coord_y: "<<coord_y_<<endl;
    cout<<"my rank: "<<myrank_<<" ix_loc_: "<<ix_loc_<<" iy_loc_: "<<iy_loc_<<endl;
    cout<<"my rank: "<<myrank_<<" nx_loc_: "<<nx_loc_<<" ny_loc_: "<<ny_loc_<<endl;

    int             *ix_loc_array = nullptr;
    int             *iy_loc_array = nullptr;
    int             *nx_loc_array = nullptr;
    int             *ny_loc_array = nullptr;

    if (myrank_==0){
        ix_loc_array = new int[ size_ ];
        iy_loc_array = new int[ size_ ];
        nx_loc_array = new int[ size_ ];
        ny_loc_array = new int[ size_ ];
    }

    MPI_Gather(&ix_loc_, 1, MPI_INT, ix_loc_array, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather(&iy_loc_, 1, MPI_INT, iy_loc_array, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather(&nx_loc_, 1, MPI_INT, nx_loc_array, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather(&ny_loc_, 1, MPI_INT, ny_loc_array, 1, MPI_INT, 0, MPI_COMM_WORLD);

    if (myrank_==root_){

        ix_loc_vec_.assign(ix_loc_array, ix_loc_array + size_ );
        iy_loc_vec_.assign(iy_loc_array, iy_loc_array + size_ );
        nx_loc_vec_.assign(nx_loc_array, nx_loc_array + size_ );
        ny_loc_vec_.assign(ny_loc_array, ny_loc_array + size_ );

        delete[] ix_loc_array;
        delete[] iy_loc_array;
        delete[] nx_loc_array;
        delete[] ny_loc_array;
    }
    
#else
    myrank_=0;
    size_=1;
    root_=0;
    nx_loc_=nx_glo_;
    ny_loc_=ny_glo_;
    nSize_loc_=nSize_glo_;
    ix_loc_=0;
    iy_loc_=0;
#endif
}
