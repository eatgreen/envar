#include "ShallowWaterModel.hpp"

#ifdef TIME	
	static std::chrono::duration<float> set_BC_time=std::chrono::duration<float>::zero();
	static std::chrono::duration<float> RoeFlux_time=std::chrono::duration<float>::zero();
#endif

ShallowWaterModel::ShallowWaterModel(void){
}

ShallowWaterModel::~ShallowWaterModel(void){
}

//Constructor taking ModelPara as parameter
ShallowWaterModel::ShallowWaterModel(ModelPara& modpar_obj, SWMpiDom& mpi_obj){
	Dim_type_=modpar_obj.dimpara().Dim_type();
    DynStep_type_=modpar_obj.DynStep_type();
    Flux_type_=modpar_obj.Flux_type();
    BC_type_=modpar_obj.BC_type();
    g_=modpar_obj.g();
    orderT_=modpar_obj.orderT();
    nState_=modpar_obj.dimpara().nState();

    dt_=modpar_obj.dt();
    LocUncert_flag_=modpar_obj.LocUncert_flag();
    LocUncert_combo_=modpar_obj.LocUncert_combo();
    WhiteNoise_flag_=modpar_obj.WhiteNoise_flag();
    
    Viscosity_flag_=modpar_obj.Viscosity_flag();
    miu_=modpar_obj.miu();
    Topo_flag_=modpar_obj.Topo_flag();
    Coriolis_flag_=modpar_obj.Coriolis_flag();
    f_cori_=modpar_obj.f_cori();
    Bottom_friction_flag_=modpar_obj.Bottom_friction_flag();

	dx_=mpi_obj.dx();
	dy_=mpi_obj.dy();

#if defined(MPI) || defined(MPI_simple)
	nx_=mpi_obj.nx_loc();
	ny_=mpi_obj.ny_loc();
	nSize_=mpi_obj.nSize_loc();
	xx_=mpi_obj.xx_glo().block(mpi_obj.ix_loc(), mpi_obj.iy_loc(), mpi_obj.nx_loc(), mpi_obj.ny_loc());
	yy_=mpi_obj.yy_glo().block(mpi_obj.ix_loc(), mpi_obj.iy_loc(), mpi_obj.nx_loc(), mpi_obj.ny_loc());
#else
	nx_=mpi_obj.nx_glo();
	ny_=mpi_obj.ny_glo();
	nSize_=mpi_obj.nSize_glo();
	xx_=mpi_obj.xx_glo();
	yy_=mpi_obj.yy_glo();
#endif
}

VectorXd ShallowWaterModel::TimeIntegration2D(const double *px, const int x_size, bool print_time_flag, SWMpiDom& mpi_obj){	
	VectorXd MQy,y,k1,k2,k3,MQx2,MQx3,MQx;

	Eigen::Map<const VectorXd> x(px, x_size);
	MQx=Physical2Movement(x);

    k1=Advection(MQx,  0, mpi_obj);
    MQx2=MQx+0.5*dt_*k1;
    k2=Advection(MQx2, 0, mpi_obj);
    MQx3=MQx-dt_*k1+2.0*dt_*k2;
    k3=Advection(MQx3, print_time_flag, mpi_obj);
    MQy = MQx + dt_*(1.0/6*k1+2.0/3*k2+1.0/6*k3);

    y=InvPhysical2Movement(MQy);

    return y;
}

//Private methods:
VectorXd ShallowWaterModel::Advection(VectorXd& MQx, bool print_time_flag, SWMpiDom& mpi_obj){
	VectorXd Flux,x;
	std::vector<MatrixXd> F,G,Fluxfoo;
	BC hBC,uBC,vBC;
	MatrixXd h,u,v;

	bool flagX=1;
	bool flagY=!flagX;

	x=InvPhysical2Movement(MQx);

	h=get_h(x, nx_, ny_);
	u=get_u(x, nx_, ny_);
	v=get_v(x, nx_, ny_);

#ifdef TIME	
    auto t_0=Clock::now();
#endif
	hBC=set_BC(h, "h", mpi_obj);
	uBC=set_BC(u, "u", mpi_obj);
	vBC=set_BC(v, "v", mpi_obj);

#ifdef TIME	
    auto t_1=Clock::now();

    if (mpi_obj.myrank()==mpi_obj.root()){
    	set_BC_time+=t_1-t_0;
    	if (print_time_flag)
    		cout<<"my rank: "<<mpi_obj.myrank()<<" time elapsed in ShallowWaterModel::Advection set_BC: "<<std::chrono::duration_cast<std::chrono::microseconds>(set_BC_time).count()<<" mius"<<endl;
	}
    
    auto t_2=Clock::now();
#endif

	F=RoeFlux(hBC.WBC(),hBC.EBC(),uBC.WBC(),uBC.EBC(),vBC.WBC(),vBC.EBC(),flagX);

	G=RoeFlux(hBC.NBC(),hBC.SBC(),uBC.NBC(),uBC.SBC(),vBC.NBC(),vBC.SBC(),flagY);

#ifdef TIME	
    auto t_3=Clock::now();

    if (mpi_obj.myrank()==mpi_obj.root()){
    	RoeFlux_time+=t_3-t_2;
    	if (print_time_flag)
    		cout<<"my rank: "<<mpi_obj.myrank()<<" time elapsed in ShallowWaterModel::Advection RoeFlux: "<<std::chrono::duration_cast<std::chrono::microseconds>(RoeFlux_time).count()<<" mius"<<endl;
	}
#endif

	for(std::size_t i=0; i<(unsigned int)nState_; i++){
		Fluxfoo.push_back( -( ( F.at(i).topRightCorner(F.at(i).rows()-1,F.at(i).cols()-1) - F.at(i).topLeftCorner(F.at(i).rows()-1,F.at(i).cols()-1) )/dx_
		  + ( G.at(i).bottomLeftCorner(G.at(i).rows()-1,G.at(i).cols()-1) - G.at(i).topLeftCorner(G.at(i).rows()-1,G.at(i).cols()-1) )/dy_ ) );
	}

	Flux=get_ctl_vector(Fluxfoo.at(0),Fluxfoo.at(1),Fluxfoo.at(2));
	
	return Flux;
}


std::vector<MatrixXd>  ShallowWaterModel::RoeFlux(const MatrixXd& hBC_l, const MatrixXd& hBC_r, const MatrixXd& uBC_l, const MatrixXd& uBC_r, const MatrixXd& vBC_l, const MatrixXd& vBC_r, bool& flag){
	
	std::vector<MatrixXd>  Roe;
	bool flagX, flagY;
	int  rows, cols;

	flagX=flag;
	flagY=!flag;
	rows=hBC_l.rows();
	cols=hBC_l.cols();

	MatrixXd duml,dumr,hhat,uhat,vhat,chat,chatl,chatr,dh,du,dv,uNl,uNr;
	MatrixXd al2,al3,ar2,ar3,a1,a2,a3,da2,da3;
	MatrixXd R21,R22,R23,R31,R32,R33,alpha1,alpha2,alpha3,FL1,FL2,FL3,FR1,FR2,FR3;

	duml=hBC_l.cwiseSqrt();
	dumr=hBC_r.cwiseSqrt();

	hhat=duml.cwiseProduct(dumr);
	uhat=(duml.cwiseProduct(uBC_l)+dumr.cwiseProduct(uBC_r)).cwiseQuotient(duml+dumr);
	vhat=(duml.cwiseProduct(vBC_l)+dumr.cwiseProduct(vBC_r)).cwiseQuotient(duml+dumr);

	chat=(0.5*g_*(hBC_l+hBC_r)).cwiseSqrt();
	chatl=(g_*hBC_l).cwiseSqrt();
	chatr=(g_*hBC_r).cwiseSqrt();

	dh=hBC_r-hBC_l;
	du=uBC_r-uBC_l;
	dv=vBC_r-vBC_l;

	uNl = uBC_l*flagX + vBC_l*flagY;
	uNr = uBC_r*flagX + vBC_r*flagY;

	al2=uNl+chatl;
	al3=uNl-chatl;
	ar2=uNr+chatr;
	ar3=uNr-chatr;

	a1 = (uhat*flagX + vhat*flagY).cwiseAbs() ;
	a2 = (uhat*flagX + vhat*flagY + chat).cwiseAbs();
	a3 = (uhat*flagX + vhat*flagY - chat).cwiseAbs();

	da2=(2*(ar2-al2)).cwiseMax(MatrixXd::Zero(rows,cols));
	da3=(2*(ar3-al3)).cwiseMax(MatrixXd::Zero(rows,cols));

	a2=(a2.array()<da2.array()).select((a2.array()*a2.array()/da2.array()+da2.array())*0.5,a2);

	a3=(a3.array()<da3.array()).select((a3.array()*a3.array()/da3.array()+da3.array())*0.5,a3);

	R21=-flagY*MatrixXd::Ones(rows,cols);
	R22=uhat+flagX*chat;
	R23=uhat-flagX*chat;
	R31=flagX*MatrixXd::Ones(rows,cols);
	R32=vhat+flagY*chat;
	R33=vhat-flagY*chat;

	alpha1 = (hhat.array()*dv.array()*flagX - hhat.array()*du.array()*flagY)*a1.array();
	alpha2 = 0.5 * (dh.array() + (hhat.array()*du.array()*flagX + hhat.array()*dv.array()*flagY)/chat.array()).array()*a2.array();
	alpha3 = 0.5 * (dh.array() - (hhat.array()*du.array()*flagX + hhat.array()*dv.array()*flagY)/chat.array()).array()*a3.array();

	// Left flux
	FL1=uNl.array()*hBC_l.array();
	FL2=uBC_l.array()*uNl.array()*hBC_l.array() + 0.5*g_*hBC_l.array()*hBC_l.array()*flagX;
	FL3=vBC_l.array()*uNl.array()*hBC_l.array() + 0.5*g_*hBC_l.array()*hBC_l.array()*flagY;

	 // Right flux
	FR1=uNr.array()*hBC_r.array();
	FR2=uBC_r.array()*uNr.array()*hBC_r.array() + 0.5*g_*hBC_r.array()*hBC_r.array()*flagX;
	FR3=vBC_r.array()*uNr.array()*hBC_r.array() + 0.5*g_*hBC_r.array()*hBC_r.array()*flagY;

	Roe.push_back(0.5*(FL1+FR1-alpha2-alpha3));
	Roe.push_back(0.5*(FL2.array()+FR2.array()-(R21.array()*alpha1.array())-(R22.array()*alpha2.array())-(R23.array()*alpha3.array())));
	Roe.push_back(0.5*(FL3.array()+FR3.array()-(R31.array()*alpha1.array())-(R32.array()*alpha2.array())-(R33.array()*alpha3.array())));
	
	return Roe;
}

ShallowWaterModel::BC  ShallowWaterModel::set_BC(const MatrixXd& h, std::string flag, SWMpiDom& mpi_obj){

	BC       hBC;
	MatrixXd foo;
	std::string flagh("h"),flagu("u"),flagv("v");
	Eigen::RowVectorXd h_col;
	VectorXd h_row;

	#if defined(MPI) || defined(MPI_simple)
	MPI_Status status;
	#ifdef MPI_simple
	double   h_cor=0.0;
	#endif
	#endif

	#ifdef MPI_simple

	foo.resize(h.rows()+1,h.cols()+1);

	//four rank example
	if(mpi_obj.myrank()==0)
	{
		foo.resize(h.rows()+1,h.cols()+1);
		h_col.resize(h.cols());
		h_row.resize(h.rows());

		//west BC
		foo.topRightCorner(h.rows(),h.cols())=h;

	    MPI_Send(h.rightCols(1).data(), h.rows(), MPI_DOUBLE, 1, 100, MPI_COMM_WORLD); //send the right column to rank 1
		
	    MPI_Recv(h_col.data(), h.cols(), MPI_DOUBLE, 2, 200, MPI_COMM_WORLD, &status); //recv the top row from rank 2
		
	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 2, 300, MPI_COMM_WORLD, &status); //recv the top left corner from rank 2

		if (!flag.compare(flagh)){
			foo.block(0,0,h.rows(),1)=h.leftCols(1);
			foo(h.rows(),0)=h_cor;
			foo.block(h.rows(),1,1,h.cols())=h_col;
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,h.rows(),1)=-h.leftCols(1);
			foo(h.rows(),0)=-h_cor;
			foo.block(h.rows(),1,1,h.cols())=-h_col;
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,h.rows(),1)=h.leftCols(1);
			foo(h.rows(),0)=h_cor;
			foo.block(h.rows(),1,1,h.cols())=h_col;
		}

		hBC.setWBC(foo);
		
		//east BC
		foo.topLeftCorner(h.rows(),h.cols())=h;

	    MPI_Recv(h_row.data(), h.rows(), MPI_DOUBLE, 1, 102, MPI_COMM_WORLD, &status); //recv the left column to rank 1
		
	    MPI_Recv(h_col.data(), h.cols(), MPI_DOUBLE, 2, 202, MPI_COMM_WORLD, &status); //recv the top row to rank 2

	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 3, 303, MPI_COMM_WORLD, &status); //recv the top left corner from rank 3

		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h_cor;
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=-h_cor;
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h_cor;
		}

		hBC.setEBC(foo);
		
		//north BC
		foo.bottomLeftCorner(h.rows(),h.cols())=h;

	    MPI_Recv(h_row.data(), h.rows(), MPI_DOUBLE, 1, 104, MPI_COMM_WORLD, &status); //recv the left column from rank 1

	    h_col=h.bottomRows(1);
	    MPI_Send(h_col.data(), h.cols(), MPI_DOUBLE, 2, 204, MPI_COMM_WORLD); //send the bottom row to rank 2

	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 1, 306, MPI_COMM_WORLD, &status); //recv the top left corner from rank 1

		if (!flag.compare(flagh)){
			foo.block(0,0,1,h.cols())=h.block(0,0,1,h.cols());
			foo(0,h.cols())=h_cor;
			foo.block(1,h.cols(),h.rows(),1)=h_row;
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,1,h.cols())=h.block(0,0,1,h.cols());
			foo(0,h.cols())=h_cor;
			foo.block(1,h.cols(),h.rows(),1)=h_row;
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,1,h.cols())=-h.block(0,0,1,h.cols());
			foo(0,h.cols())=-h_cor;
			foo.block(1,h.cols(),h.rows(),1)=h_row;
		}

		hBC.setNBC(foo);
		
		//south BC
		foo.topLeftCorner(h.rows(),h.cols())=h;

	    MPI_Recv(h_row.data(), h.rows(), MPI_DOUBLE, 1, 106, MPI_COMM_WORLD, &status); //recv the left column to rank 1
		
	    MPI_Recv(h_col.data(), h.cols(), MPI_DOUBLE, 2, 206, MPI_COMM_WORLD, &status); //recv the top row to rank 2

	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 3, 309, MPI_COMM_WORLD, &status); //recv the top left corner from rank 3

		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h_cor;
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h_cor;
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=-h_col;
			foo(h.rows(),h.cols())=-h_cor;
		}

		hBC.setSBC(foo);
	}
	else if(mpi_obj.myrank()==1)
	{
		foo.resize(h.rows()+1,h.cols()+1);
		h_col.resize(h.cols());
		h_row.resize(h.rows());
		//west BC
		foo.topRightCorner(h.rows(),h.cols())=h;
	    
	    MPI_Recv(h_row.data(), h.rows(), MPI_DOUBLE, 0, 100, MPI_COMM_WORLD, &status); //recv the right column from rank 0
		
	    MPI_Recv(h_col.data(), h.cols(), MPI_DOUBLE, 3, 201, MPI_COMM_WORLD, &status); //recv the top row from rank 3

	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 2, 301, MPI_COMM_WORLD, &status); //recv the top right corner from rank 2

		if (!flag.compare(flagh)){
			foo.block(0,0,h.rows(),1)=h_row;
			foo(h.rows(),0)=h_cor;
			foo.block(h.rows(),1,1,h.cols())=h_col;
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,h.rows(),1)=-h_row;
			foo(h.rows(),0)=-h_cor;
			foo.block(h.rows(),1,1,h.cols())=-h_col;
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,h.rows(),1)=h_row;
			foo(h.rows(),0)=h_cor;
			foo.block(h.rows(),1,1,h.cols())=h_col;
		}

		hBC.setWBC(foo);

		//east BC
		foo.topLeftCorner(h.rows(),h.cols())=h;
		foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);

	    MPI_Send(h.leftCols(1).data(), h.rows(), MPI_DOUBLE, 0, 102, MPI_COMM_WORLD); //send the left column to rank 1
		
	    MPI_Recv(h_col.data(), h.cols(), MPI_DOUBLE, 3, 203, MPI_COMM_WORLD, &status); //recv the top row from rank 3

	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 3, 304, MPI_COMM_WORLD, &status); //recv the top right corber from rank 3

		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h_cor;
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=-h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=-h_col;
			foo(h.rows(),h.cols())=-h_cor;
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h_cor;
		}

		hBC.setEBC(foo);

		//north BC
		foo.bottomLeftCorner(h.rows(),h.cols())=h;

	    MPI_Send(h.leftCols(1).data(), h.rows(), MPI_DOUBLE, 0, 104, MPI_COMM_WORLD); //send the left column to rank 0

	    h_col=h.bottomRows(1);
	    MPI_Send(h_col.data(), h.cols(), MPI_DOUBLE, 3, 205, MPI_COMM_WORLD); //send the bottom row to rank 3

	    MPI_Send(&(h(0,0)), 1, MPI_DOUBLE, 0, 306, MPI_COMM_WORLD); //send the top left corner to rank 0

	    MPI_Send(&(h(h.rows()-1,0)), 1, MPI_DOUBLE, 2, 307, MPI_COMM_WORLD); //send the bottom left corner to rank 2

	    MPI_Send(&(h(h.rows()-1,h.cols()-1)), 1, MPI_DOUBLE, 3, 308, MPI_COMM_WORLD); //send the bottom right corner to rank 3

		if (!flag.compare(flagh)){
			foo.block(0,0,1,h.cols())=h.block(0,0,1,h.cols());
			foo(0,h.cols())=h(0,h.cols()-1);
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,1,h.cols())=h.block(0,0,1,h.cols());
			foo(0,h.cols())=h(0,h.cols()-1);
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,1,h.cols())=-h.block(0,0,1,h.cols());
			foo(0,h.cols())=-h(0,h.cols()-1);
			foo.block(1,h.cols(),h.rows(),1)=-h.rightCols(1);
		}

		hBC.setNBC(foo);

		//south BC
		foo.topLeftCorner(h.rows(),h.cols())=h;

	    MPI_Send(h.leftCols(1).data(), h.rows(), MPI_DOUBLE, 0, 106, MPI_COMM_WORLD); //send the left column to rank 1
		
	    MPI_Recv(h_col.data(), h.cols(), MPI_DOUBLE, 3, 207, MPI_COMM_WORLD, &status); //recv the top row from rank 3

	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 3, 310, MPI_COMM_WORLD, &status); //recv the top right corber from rank 3

		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h_cor;
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h_cor;
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=-h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=-h_col;
			foo(h.rows(),h.cols())=-h_cor;
		}

		hBC.setSBC(foo);
	}
	else if(mpi_obj.myrank()==2)
	{
		foo.resize(h.rows()+1,h.cols()+1);
		h_col.resize(h.cols());
		h_row.resize(h.rows());

		//west BC
		foo.topRightCorner(h.rows(),h.cols())=h;

	    MPI_Send(h.rightCols(1).data(), h.rows(), MPI_DOUBLE, 3, 101, MPI_COMM_WORLD); //send the right column to rank 3			

		h_col = h.topRows(1);
	    MPI_Send(h_col.data(), h.cols(), MPI_DOUBLE, 0, 200, MPI_COMM_WORLD); //send the top row to rank 0

	    MPI_Send(&(h(0,0)), 1, MPI_DOUBLE, 0, 300, MPI_COMM_WORLD); //send the top left corner to rank 0

	    MPI_Send(&(h(0,h.cols()-1)), 1, MPI_DOUBLE, 1, 301, MPI_COMM_WORLD); //send the top right corner to rank 1

	    MPI_Send(&(h(h.rows()-1,h.cols()-1)), 1, MPI_DOUBLE, 3, 302, MPI_COMM_WORLD); //send the bottom right corner to rank 3

		if (!flag.compare(flagh)){
			foo.block(0,0,h.rows(),1)=h.leftCols(1);
			foo(h.rows(),0)=h(h.rows()-1,0);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,h.rows(),1)=-h.leftCols(1);
			foo(h.rows(),0)=-h(h.rows()-1,0);
			foo.block(h.rows(),1,1,h.cols())=-h.bottomRows(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,h.rows(),1)=h.leftCols(1);
			foo(h.rows(),0)=h(h.rows()-1,0);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}

		hBC.setWBC(foo);

		//east BC
		foo.topLeftCorner(h.rows(),h.cols())=h;

	    MPI_Recv(h_row.data(), h.rows(), MPI_DOUBLE, 3, 103, MPI_COMM_WORLD, &status); //recv the left column from rank 3

		h_col=h.topRows(1);;
	    MPI_Send(h_col.data(), h.cols(), MPI_DOUBLE, 0, 202, MPI_COMM_WORLD); //send the top row to rank 0

	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 3, 305, MPI_COMM_WORLD, &status); //recv the bottom left corner from rank 3

		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_cor;
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=-h_row;
			foo.block(h.rows(),0,1,h.cols())=-h.bottomRows(1);
			foo(h.rows(),h.cols())=-h_cor;
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_cor;
		}

		hBC.setEBC(foo);

		//north BC
		foo.bottomLeftCorner(h.rows(),h.cols())=h;

	    MPI_Recv(h_row.data(), h.rows(), MPI_DOUBLE, 3, 105, MPI_COMM_WORLD, &status); //recv the left column from rank 3

	    MPI_Recv(h_col.data(), h.cols(), MPI_DOUBLE, 0, 204, MPI_COMM_WORLD, &status); //recv the top row from rank 0

	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 1, 307, MPI_COMM_WORLD, &status); //recv the bottom right corner from rank 1

		if (!flag.compare(flagh)){
			foo.block(0,0,1,h.cols())=h_col;
			foo(0,h.cols())=h_cor;
			foo.block(1,h.cols(),h.rows(),1)=h_row;
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,1,h.cols())=h_col;
			foo(0,h.cols())=h_cor;
			foo.block(1,h.cols(),h.rows(),1)=h_row;
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,1,h.cols())=-h_col;
			foo(0,h.cols())=-h_cor;
			foo.block(1,h.cols(),h.rows(),1)=-h_row;
		}
		
		hBC.setNBC(foo);

		//south BC
		foo.topLeftCorner(h.rows(),h.cols())=h;

	    MPI_Recv(h_row.data(), h.rows(), MPI_DOUBLE, 3, 107, MPI_COMM_WORLD, &status); //recv the left column from rank 3

	    h_col=h.topRows(1);
	    MPI_Send(h_col.data(), h.cols(), MPI_DOUBLE, 0, 206, MPI_COMM_WORLD); //send the top row to rank 0

	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 3, 311, MPI_COMM_WORLD, &status); //recv the bottom left corner from rank 3

		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_cor;
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_cor;
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=-h_row;
			foo.block(h.rows(),0,1,h.cols())=-h.bottomRows(1);
			foo(h.rows(),h.cols())=-h_cor;
		}

		hBC.setSBC(foo);
	}
	else if(mpi_obj.myrank()==3)
	{
		foo.resize(h.rows()+1,h.cols()+1);
		h_col.resize(h.cols());
		h_row.resize(h.rows());
		//west BC
		foo.topRightCorner(h.rows(),h.cols())=h;

		MPI_Recv(h_row.data(), h.rows(), MPI_DOUBLE, 2, 101, MPI_COMM_WORLD, &status); //recv the right column from rank 2			

		h_col=h.topRows(1);
	    MPI_Send(h_col.data(), h.cols(), MPI_DOUBLE, 1, 201, MPI_COMM_WORLD); //send the top row to rank 1

	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 2, 302, MPI_COMM_WORLD, &status); //recv the bottom right corner from rank 2

		if (!flag.compare(flagh)){
			foo.block(0,0,h.rows(),1)=h_row;
			foo(h.rows(),0)=h_cor;
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,h.rows(),1)=h_row;
			foo(h.rows(),0)=h_cor;
			foo.block(h.rows(),1,1,h.cols())=-h.bottomRows(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,h.rows(),1)=h_row;
			foo(h.rows(),0)=h_cor;
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}

		hBC.setWBC(foo);

		//east BC
		foo.topLeftCorner(h.rows(),h.cols())=h;

	    MPI_Send(h.leftCols(1).data(), h.rows(), MPI_DOUBLE, 2, 103, MPI_COMM_WORLD); //send the left column to rank 2

		h_col=h.topRows(1);
	    MPI_Send(h_col.data(), h.cols(), MPI_DOUBLE, 1, 203, MPI_COMM_WORLD); //send the top row to rank 1

	    MPI_Send(&(h(0,0)), 1, MPI_DOUBLE, 0, 303, MPI_COMM_WORLD); //send the top left corner to rank 0

	    MPI_Send(&(h(0,h.cols()-1)), 1, MPI_DOUBLE, 1, 304, MPI_COMM_WORLD); //send the top right corner to rank 1

	    MPI_Send(&(h(h.rows()-1,0)), 1, MPI_DOUBLE, 2, 305, MPI_COMM_WORLD); //send the bottom left corner to rank 2

		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=-h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=-h.bottomRows(1);
			foo(h.rows(),h.cols())=-h(h.rows()-1,h.cols()-1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
		}

		hBC.setEBC(foo);

		//north BC
		foo.bottomLeftCorner(h.rows(),h.cols())=h;

	    MPI_Send(h.leftCols(1).data(), h.rows(), MPI_DOUBLE, 2, 105, MPI_COMM_WORLD); //send the left column to rank 2

	    MPI_Recv(h_col.data(), h.cols(), MPI_DOUBLE, 1, 205, MPI_COMM_WORLD, &status); //recv the bottom row from rank 1

	    MPI_Recv(&h_cor, 1, MPI_DOUBLE, 1, 308, MPI_COMM_WORLD, &status); //recv the bottom right corner from rank 1

		if (!flag.compare(flagh)){
			foo.block(0,0,1,h.cols())=h_col;
			foo(0,h.cols())=h_cor;
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,1,h.cols())=h_col;
			foo(0,h.cols())=h_cor;
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,1,h.cols())=h_col;
			foo(0,h.cols())=h_cor;
			foo.block(1,h.cols(),h.rows(),1)=-h.rightCols(1);
		}

		hBC.setNBC(foo);

		//south BC
		foo.topLeftCorner(h.rows(),h.cols())=h;

	    MPI_Send(h.leftCols(1).data(), h.rows(), MPI_DOUBLE, 2, 107, MPI_COMM_WORLD); //send the left column to rank 2

		h_col=h.topRows(1);
	    MPI_Send(h_col.data(), h.cols(), MPI_DOUBLE, 1, 207, MPI_COMM_WORLD); //send the top row to rank 1

	    MPI_Send(&(h(0,0)), 1, MPI_DOUBLE, 0, 309, MPI_COMM_WORLD); //send the top left corner to rank 0

	    MPI_Send(&(h(0,h.cols()-1)), 1, MPI_DOUBLE, 1, 310, MPI_COMM_WORLD); //send the top right corner to rank 1

	    MPI_Send(&(h(h.rows()-1,0)), 1, MPI_DOUBLE, 2, 311, MPI_COMM_WORLD); //send the bottom left corner to rank 2

		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=-h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=-h.bottomRows(1);
			foo(h.rows(),h.cols())=-h(h.rows()-1,h.cols()-1);
		}

		hBC.setSBC(foo);
	}
	else
		cout<<"Only 4 rank is allowed with key MPI_simple"<<endl;

	#elif  MPI

	//  ---->y
	//	|	-----------------------------------------------------------------
	//	|	*	*	*	* | *	*	*	* |	*	*	*	* |	*	*	*	*	*
	//	|	*	* 0	*	* |	*	* 1	*	* |	*	* 2	*	* |	*	* 3	*	*	*
	//	x	*	*(0,0)*	* |	*	*(0,1)*	* |	*	*(0,2)*	* |	*	*(0,3)*	*	*
	//  	-----------------------------------------------------------------
	//		*	*	*	* |	*	*	*	* |	*	*	*	* |	*	*	*	*	*
	//		*	* 4	*	* |	*	* 5	*	* |	*	* 6	*	* |	*	* 7	*	*	*
	//		*	*(1,0)*	* |	*	*(1,1)*	* |	*	*(1,2)*	* |	*	*(1,3)*	*	*
	//  	-----------------------------------------------------------------
	//		*	*	*	* |	*	*	*	* |	*	*	*	* |	*	*	*	*	*
	//		*	* 8	*	* |	*	* 9	*	* |	*	* 10*	* |	*	* 11*	*	*
	//		*	*(2,0)*	* |	*	*(2,1)*	* |	*	*(2,2)*	* |	*	*(2,3)*	*	*
	//		-----------------------------------------------------------------
	//		*	*	*	* |	*	*	*	* |	*	*	*	* |	*	*	*	*	*
	//		*	* 12*	* |	*	* 13*	* |	*	* 14*	* |	*	* 15*	*	*
	//		*	*(3,0)*	* |	*	*(3,1)*	* |	*	*(3,2)*	* |	*	*(3,3)*	*	*
	//		-----------------------------------------------------------------

	int disp,sendtag,direction_x=0,direction_y=1;
	int source, dest;

	foo.resize(h.rows()+1,h.cols()+1);
	h_col.resize(h.cols());
	h_row.resize(h.rows());

	//west BC

	//	#	*	*	*	* 
	//	#	*   * 	*	* 
	//	#	*	*	*	* 
	//	#	#	#	#	#

	foo.topRightCorner(h.rows(),h.cols())=h;

	disp=1;

	MPI_Cart_shift(mpi_obj.comm_cart(), direction_y, disp, &source, &dest);

	sendtag=mpi_obj.myrank()+100;

	//   	cout<<"my rank at west: "<<mpi_obj.myrank()<<"("<<mpi_obj.coord_x()<<","<<mpi_obj.coord_y()<<")"<<" source: "<<source<<" dest: "<<dest<<endl;

	if ( mpi_obj.coord_y() == 0 && dest != -2 )
	{
		//west BC
	    MPI_Send(h.rightCols(1).data(), h.rows(), MPI_DOUBLE, dest, sendtag, mpi_obj.comm_cart());

		if (!flag.compare(flagh)){
			foo.block(0,0,h.rows(),1)=h.leftCols(1);
			foo(h.rows(),0)=h(h.rows()-1,0);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,h.rows(),1)=-h.leftCols(1);
			foo(h.rows(),0)=-h(h.rows()-1,0);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,h.rows(),1)=h.leftCols(1);
			foo(h.rows(),0)=h(h.rows()-1,0);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
	}
	else if ( mpi_obj.coord_y() == mpi_obj.proc_nj()-1 && source != -2 )
	{
		//west BC
		MPI_Recv(h_row.data(), h.rows(), MPI_DOUBLE, source, MPI_ANY_TAG, mpi_obj.comm_cart(), &status);

		if (!flag.compare(flagh)){
			foo.block(0,0,h.rows(),1)=h_row;
			foo(h.rows(),0)=h_row(h.rows()-1);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,h.rows(),1)=h_row;
			foo(h.rows(),0)=h_row(h.rows()-1);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,h.rows(),1)=h_row;
			foo(h.rows(),0)=h_row(h.rows()-1);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
	}
	else if ( source != -2 && dest != -2 )
	{
		//west BC
		MPI_Sendrecv(h.rightCols(1).data(), h.rows(), MPI_DOUBLE, dest, sendtag, h_row.data(), h.rows(), MPI_DOUBLE, source, MPI_ANY_TAG, mpi_obj.comm_cart(), &status);

		if (!flag.compare(flagh)){
			foo.block(0,0,h.rows(),1)=h_row;
			foo(h.rows(),0)=h_row(h.rows()-1);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,h.rows(),1)=h_row;
			foo(h.rows(),0)=h_row(h.rows()-1);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,h.rows(),1)=h_row;
			foo(h.rows(),0)=h_row(h.rows()-1);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
	}
	else if ( source == -2 && dest == -2 )
	{
		if (!flag.compare(flagh)){
			foo.block(0,0,h.rows(),1)=h.leftCols(1);
			foo(h.rows(),0)=h(h.rows()-1,0);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,h.rows(),1)=-h.leftCols(1);
			foo(h.rows(),0)=-h(h.rows()-1,0);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,h.rows(),1)=h.leftCols(1);
			foo(h.rows(),0)=h(h.rows()-1,0);
			foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
		}
	}
	else
	{
	    std::cerr << "Bad rank not supported!!!" << endl;
	}

	hBC.setWBC(foo);

	//east BC

	//	*	*	*	* 	#
	//	*   * 	*	* 	#
	//	*	*	*	* 	#
	//	#	#	#	#	#

	foo.topLeftCorner(h.rows(),h.cols())=h;

	// MPI_Request request;

	disp=-1;
	sendtag=mpi_obj.myrank()+200;

	MPI_Cart_shift(mpi_obj.comm_cart(), direction_y, disp, &source, &dest);

	// cout<<"my rank at east: "<<mpi_obj.myrank()<<"("<<mpi_obj.coord_x()<<","<<mpi_obj.coord_y()<<")"<<" source: "<<source<<" dest: "<<dest<<endl;

	if ( mpi_obj.coord_y() == mpi_obj.proc_nj()-1 && dest != -2 )
	{
		//east BC
	    MPI_Send(h.leftCols(1).data(), h.rows(), MPI_DOUBLE, dest, sendtag, mpi_obj.comm_cart());
	    // MPI_Isend(h.leftCols(1).data(), h.rows(), MPI_DOUBLE, dest, sendtag, mpi_obj.comm_cart(), &request);

		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);;
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=-h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=-h(h.rows()-1,h.cols()-1);;
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);;
		}
	}
	else if ( mpi_obj.coord_y() == 0 && source != -2 )
	{
		//east BC
		// MPI_Probe(source, recvtag, mpi_obj.comm_cart(), &status);
	    MPI_Recv(h_row.data(), h.rows(), MPI_DOUBLE, source, MPI_ANY_TAG, mpi_obj.comm_cart(), &status);
	    // MPI_Irecv(h_row.data(), h.rows(), MPI_DOUBLE, source, recvtag, mpi_obj.comm_cart(), &request);

		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_row(h.rows()-1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_row(h.rows()-1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_row(h.rows()-1);
		}
	}
	else if ( source != -2 && dest != -2 )
	{
		//east BC
		MPI_Sendrecv(h.leftCols(1).data(), h.rows(), MPI_DOUBLE, dest, sendtag, h_row.data(), h.rows(), MPI_DOUBLE, source, MPI_ANY_TAG, mpi_obj.comm_cart(), &status);
		
		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_row(h.rows()-1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_row(h.rows()-1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h_row;
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_row(h.rows()-1);
		}
	}
	else if ( source == -2 && dest == -2 )
	{
		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=-h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=-h(h.rows()-1,h.cols()-1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
		}
	}
	else
	{
	    std::cerr << "Bad rank not supported!!!" << endl;
	}

	// MPI_Wait(&request, &status);

	hBC.setEBC(foo);

	//north BC

	//	#	#	#	#	#
	//	*	*	*	* 	#
	//	*   * 	*	* 	#
	//	*	*	*	* 	#

	Eigen::RowVectorXd h_col2;
	h_col2.resize(h.cols());

	foo.bottomLeftCorner(h.rows(),h.cols())=h;

	disp=1;
	sendtag=mpi_obj.myrank()+300;

	MPI_Cart_shift(mpi_obj.comm_cart(), direction_x, disp, &source, &dest);

	// cout<<"my rank at north: "<<mpi_obj.myrank()<<" ("<<mpi_obj.coord_x()<<","<<mpi_obj.coord_y()<<")"<<" source: "<<source<<" dest: "<<dest<<endl;

	if ( mpi_obj.coord_x() == 0 && dest != -2 )
	{
		//north BC
		h_col=h.bottomRows(1);
	    MPI_Send(h_col.data(), h.cols(), MPI_DOUBLE, dest, sendtag, mpi_obj.comm_cart());

		if (!flag.compare(flagh)){
			foo.block(0,0,1,h.cols())=h.block(0,0,1,h.cols());
			foo(0,h.cols())=h(0,h.cols()-1);
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,1,h.cols())=h.block(0,0,1,h.cols());
			foo(0,h.cols())=h(0,h.cols()-1);
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,1,h.cols())=-h.block(0,0,1,h.cols());
			foo(0,h.cols())=-h(0,h.cols()-1);
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
	}
	else if ( mpi_obj.coord_x() == mpi_obj.proc_ni()-1 && source != -2 )
	{
		//north BC
	    MPI_Recv(h_col.data(), h.cols(), MPI_DOUBLE, source, MPI_ANY_TAG, mpi_obj.comm_cart(), &status);

		if (!flag.compare(flagh)){
			foo.block(0,0,1,h.cols())=h_col;
			foo(0,h.cols())=h_col(h.cols()-1);;
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,1,h.cols())=h_col;
			foo(0,h.cols())=h_col(h.cols()-1);;
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,1,h.cols())=h_col;
			foo(0,h.cols())=h_col(h.cols()-1);;
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
	}
	else if ( source != -2 && dest != -2 )
	{
		//north BC
		h_col2=h.bottomRows(1);
	    MPI_Sendrecv(h_col2.data(), h.cols(), MPI_DOUBLE, dest, sendtag, h_col.data(), h.cols(), MPI_DOUBLE, source, MPI_ANY_TAG, mpi_obj.comm_cart(), &status);
				
		if (!flag.compare(flagh)){
			foo.block(0,0,1,h.cols())=h_col;
			foo(0,h.cols())=h_col(h.cols()-1);;
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,1,h.cols())=h_col;
			foo(0,h.cols())=h_col(h.cols()-1);;
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,1,h.cols())=h_col;
			foo(0,h.cols())=h_col(h.cols()-1);;
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
	}
	else if ( source == -2 && dest == -2 )
	{
		if (!flag.compare(flagh)){
			foo.block(0,0,1,h.cols())=h.block(0,0,1,h.cols());
			foo(0,h.cols())=h(0,h.cols()-1);
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,0,1,h.cols())=h.block(0,0,1,h.cols());
			foo(0,h.cols())=h(0,h.cols()-1);
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,0,1,h.cols())=-h.block(0,0,1,h.cols());
			foo(0,h.cols())=-h(0,h.cols()-1);
			foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
		}
	}
	else
	{
	    std::cerr << "Bad rank not supported!!!" << endl;
	}

	hBC.setNBC(foo);

	//south BC

	//	*	*	*	* 	#
	//	*   * 	*	* 	#
	//	*	*	*	* 	#
	//	#	#	#	#	#

	foo.topLeftCorner(h.rows(),h.cols())=h;

	disp=-1;
	sendtag=mpi_obj.myrank()+400;

	MPI_Cart_shift(mpi_obj.comm_cart(), direction_x, disp, &source, &dest);

	// cout<<"my rank at south: "<<mpi_obj.myrank()<<" ("<<mpi_obj.coord_x()<<","<<mpi_obj.coord_y()<<")"<<" source: "<<source<<" dest: "<<dest<<endl;

	if ( mpi_obj.coord_x() == 0 && source != -2 )
	{
		//south BC
	    MPI_Recv(h_col.data(), h.cols(), MPI_DOUBLE, source, MPI_ANY_TAG, mpi_obj.comm_cart(), &status);
		
		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
		}
	}
	else if ( mpi_obj.coord_x() == mpi_obj.proc_ni()-1 && dest != -2 )
	{
		//south BC
		h_col=h.topRows(1);
	    MPI_Send(h_col.data(), h.cols(), MPI_DOUBLE, dest, sendtag, mpi_obj.comm_cart());
		
		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_col(h.cols()-1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h_col(h.cols()-1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=-h.bottomRows(1);
			foo(h.rows(),h.cols())=-h_col(h.cols()-1);
		}
	}
	else if ( source != -2 && dest != -2 )
	{
		//south BC
		h_col2=h.topRows(1);
	    MPI_Sendrecv(h_col2.data(), h.cols(), MPI_DOUBLE, dest, sendtag, h_col.data(), h.cols(), MPI_DOUBLE, source, MPI_ANY_TAG, mpi_obj.comm_cart(), &status); 

		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h_col(h.cols()-1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h_col(h.cols()-1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h_col;
			foo(h.rows(),h.cols())=h_col(h.cols()-1);
		}
	}
	else if ( source == -2 && dest == -2 )
	{
		if (!flag.compare(flagh)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
		}
		else if(!flag.compare(flagu)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
			foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
		}
		else if(!flag.compare(flagv)){
			foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
			foo.block(h.rows(),0,1,h.cols())=-h.bottomRows(1);
			foo(h.rows(),h.cols())=-h(h.rows()-1,h.cols()-1);
		}
	}
	else
	{
	    std::cerr << "Bad rank not supported!!!" << endl;
	}

	hBC.setSBC(foo);

	#else
	foo.resize(h.rows()+1,h.cols()+1);

	//west BC
	foo.topRightCorner(h.rows(),h.cols())=h;
	if (!flag.compare(flagh)){
		foo.block(0,0,h.rows(),1)=h.leftCols(1);
		foo(h.rows(),0)=h(h.rows()-1,0);
		foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
	}
	else if(!flag.compare(flagu)){
		foo.block(0,0,h.rows(),1)=-h.leftCols(1);
		foo(h.rows(),0)=-h(h.rows()-1,0);
		foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
	}
	else if(!flag.compare(flagv)){
		foo.block(0,0,h.rows(),1)=h.leftCols(1);
		foo(h.rows(),0)=h(h.rows()-1,0);
		foo.block(h.rows(),1,1,h.cols())=h.bottomRows(1);
	}

	hBC.setWBC(foo);

	//east BC
	foo.topLeftCorner(h.rows(),h.cols())=h;
	if (!flag.compare(flagh)){
		foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
		foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
		foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
	}
	else if(!flag.compare(flagu)){
		foo.block(0,h.cols(),h.rows(),1)=-h.rightCols(1);
		foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
		foo(h.rows(),h.cols())=-h(h.rows()-1,h.cols()-1);
	}
	else if(!flag.compare(flagv)){
		foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
		foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
		foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
	}

	hBC.setEBC(foo);

	//north BC
	foo.bottomLeftCorner(h.rows(),h.cols())=h;
	if (!flag.compare(flagh)){
		foo.block(0,0,1,h.cols())=h.block(0,0,1,h.cols());
		foo(0,h.cols())=h(0,h.cols()-1);
		foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
	}
	else if(!flag.compare(flagu)){
		foo.block(0,0,1,h.cols())=h.block(0,0,1,h.cols());
		foo(0,h.cols())=h(0,h.cols()-1);
		foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
	}
	else if(!flag.compare(flagv)){
		foo.block(0,0,1,h.cols())=-h.block(0,0,1,h.cols());
		foo(0,h.cols())=-h(0,h.cols()-1);
		foo.block(1,h.cols(),h.rows(),1)=h.rightCols(1);
	}

	hBC.setNBC(foo);

	//south BC
	foo.topLeftCorner(h.rows(),h.cols())=h;
	if (!flag.compare(flagh)){
		foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
		foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
		foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
	}
	else if(!flag.compare(flagu)){
		foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
		foo.block(h.rows(),0,1,h.cols())=h.bottomRows(1);
		foo(h.rows(),h.cols())=h(h.rows()-1,h.cols()-1);
	}
	else if(!flag.compare(flagv)){
		foo.block(0,h.cols(),h.rows(),1)=h.rightCols(1);
		foo.block(h.rows(),0,1,h.cols())=-h.bottomRows(1);
		foo(h.rows(),h.cols())=-h(h.rows()-1,h.cols()-1);
	}

	hBC.setSBC(foo);		
	#endif
	return hBC;
}

VectorXd ShallowWaterModel::Physical2Movement(const VectorXd& x){
	VectorXd y;
	y.resize(nx_*ny_*nState_);

	y.head(nx_*ny_)=x.head(nx_*ny_);
	y.segment(nx_*ny_,nx_*ny_)=x.segment(nx_*ny_,nx_*ny_).array()*x.head(nx_*ny_).array();
	y.tail(nx_*ny_)=x.tail(nx_*ny_).array()*x.head(nx_*ny_).array();

	return y;
}

VectorXd ShallowWaterModel::InvPhysical2Movement(const VectorXd& y){
	VectorXd x;
	x.resize(nx_*ny_*nState_);

	x.head(nx_*ny_)=y.head(nx_*ny_);
	x.segment(nx_*ny_,nx_*ny_)=y.segment(nx_*ny_,nx_*ny_).array()/y.head(nx_*ny_).array();
	x.tail(nx_*ny_)=y.tail(nx_*ny_).array()/y.head(nx_*ny_).array();

	return x;
}

MatrixXd get_h(const VectorXd& x, int& nx, int& ny){
	VectorXd xx;
	MatrixXd h;

	h.resize(nx, ny);
	xx=x.head(nx*ny);
	h=Eigen::Map<MatrixXd>(xx.data(), nx, ny);
	return h;
}

MatrixXd get_h(VectorXd&& x, int& nx, int& ny){
	MatrixXd h;
	h=get_h(x, nx, ny);
	return h;
}

MatrixXd get_u(const VectorXd& x, int& nx, int& ny){
	VectorXd xx;
	MatrixXd u;

	u.resize(nx, ny);
	xx=x.segment(nx*ny, nx*ny);
	u=Eigen::Map<MatrixXd>(xx.data(), nx, ny);
	return u;
}

MatrixXd get_u(VectorXd&& x, int& nx, int& ny){
	MatrixXd u;
	u=get_u(x, nx, ny);
	return u;
}

MatrixXd get_v(const VectorXd& x, int& nx, int& ny){
	VectorXd xx;
	MatrixXd v;

	v.resize(nx, ny);
	xx=x.tail(nx*ny);
	v=Eigen::Map<MatrixXd>(xx.data(), nx, ny);
	return v;
}

MatrixXd get_v(VectorXd&& x, int& nx, int& ny){
	MatrixXd v;
	v=get_v(x, nx, ny);
	return v;
}

VectorXd get_ctl_vector(const MatrixXd& h, const MatrixXd& u, const MatrixXd& v){
	VectorXd x;
	MatrixXd hh,uu,vv;

	hh=h;
	uu=u;
	vv=v;

	x.resize(h.rows()*h.cols()*3);
    x.head(h.rows()*h.cols())=Eigen::Map<VectorXd>(hh.data(), h.rows()*h.cols());
	x.segment(h.rows()*h.cols(), h.rows()*h.cols())=Eigen::Map<VectorXd>(uu.data(), h.rows()*h.cols());
	x.tail(h.rows()*h.cols())=Eigen::Map<VectorXd>(vv.data(), h.rows()*h.cols());
	return x;
}

VectorXd get_ctl_vector(MatrixXd&& h, MatrixXd&& u, MatrixXd&& v){
	VectorXd x;
	x=get_ctl_vector(h,u,v);
	return x;
}

void cout_huv(VectorXd& x, int& nx, int& ny){
	cout<<"h\n"<<get_h(x, nx, ny)<<"\n"<<endl;
	cout<<"u\n"<<get_u(x, nx, ny)<<"\n"<<endl;
	cout<<"v\n"<<get_v(x, nx, ny)<<"\n"<<endl;
}

void cout_huv(VectorXd&& x, int& nx, int& ny){
	cout_huv(x, nx, ny);
}