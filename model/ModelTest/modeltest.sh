#!/bin/sh

cd ..

g++-6 -c -I/usr/local/include/eigen3 SWMpiDom.cpp -o ModelTest/SWMpiDom.o

g++-6 -c -I/usr/local/include/eigen3 ShallowWaterModel.cpp -o ModelTest/ShallowWaterModel.o

g++-6 -c -I/usr/local/include/eigen3 ModelTest/ModelTest.cpp -o ModelTest/ModelTest.o

g++-6 ModelTest/ShallowWaterModel.o ModelTest/ModelTest.o ModelTest/SWMpiDom.o -o ModelTest/modeltest

cd ModelTest