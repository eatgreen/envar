#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Eigen/Core>
#include <chrono>
#include "../ShallowWaterModel.hpp"
#include "../ModelPara.hpp"
#include "../SWMpiDom.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;
 
using Clock=std::chrono::high_resolution_clock;

int main(int argc, char *argv[])
{
	int myrank=0,size=1;
#if defined(MPI) || defined(MPI_simple)
	MPI_Init(&argc, &argv);                   /* Initialize MPI       */
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);   /* Get my rank          */
    MPI_Comm_size(MPI_COMM_WORLD, &size);     /* Get the total number of processors */
#endif

	int proc_ni=0, proc_nj=0;
    if( argc == 3 ) {
   		proc_ni=std::stoi(argv[1]);
   		proc_nj=std::stoi(argv[2]);
    }
    else if( argc ==1 ) {
        proc_ni=std::stoi(std::getenv("NPX"));
        proc_nj=std::stoi(std::getenv("NPY"));
    }
    else {
		if(myrank==0)
	      	std::cerr<<"Provide arguments for processors: 1 1 for single run, 2 2 for mpi run, \b"
      			 		" no arguments using system variables: NPX and NPY"<<endl;
      	std::abort();
    }

    if( size!=proc_ni*proc_nj ){
		if(myrank==0)
	    	std::cerr<<"Command line input processor number not equal to this run's processor configuration"<<endl;
    	std::abort();
    }

    ModelPara modpara_obj;
    modpara_obj.ReadModelPara();
    modpara_obj.setModelDimPara();

    SWMpiDom mpi_obj(modpara_obj);
    mpi_obj.Init_domain();

	ShallowWaterModel mod_obj(modpara_obj, mpi_obj);

	MatrixXd hb_glo,ub_glo,vb_glo;
	VectorXd sb_glo;
    hb_glo=5000.0*MatrixXd::Ones(mpi_obj.nx_glo(),mpi_obj.ny_glo())-4.0e-4/mod_obj.g()*mpi_obj.xx_glo();
    ub_glo.setZero(mpi_obj.nx_glo(),mpi_obj.ny_glo());
    vb_glo.setZero(mpi_obj.nx_glo(),mpi_obj.ny_glo());
	sb_glo=get_ctl_vector(hb_glo, ub_glo, vb_glo);

	VectorXd sb_glo_plus_dt=mod_obj.TimeIntegration2D(sb_glo, 0, mpi_obj);
}