#ifndef MODELPARA
#define MODELPARA

#include <Eigen/Core>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <cstdlib>

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

class DimPara
{
public:
    enum Dim_Type                {D1,D2};

private:
    Dim_Type                     Dim_type_;
    double                       Lx_;
    double                       Ly_;
    int                          nx_;
    int                          ny_;
    int                          nState_;

    int                          proc_ni_;
    int                          proc_nj_;

    double                       dx_;
    double                       dy_;
    VectorXd                     x_;
    VectorXd                     y_;
    MatrixXd                     xx_;
    MatrixXd                     yy_;
    int                          nSize_;

public:
    DimPara() {}
    ~DimPara() {}

    DimPara(std::unordered_map<std::string, std::string> modelpara_map){

        Lx_=std::stod(modelpara_map["Lx_"]); 
        Ly_=std::stod(modelpara_map["Ly_"]); 
        nx_=std::stoi(modelpara_map["nx_"]);
        ny_=std::stoi(modelpara_map["ny_"]);
        nState_=std::stod(modelpara_map["nState_"]);   
    }

    void setDim_type(Dim_Type Dim_type) { Dim_type_=Dim_type; }
    void setnx(int nx) { nx_=nx; }
    void setny(int ny) { ny_=ny; }
    void setproc_ni(int proc_ni) { proc_ni_=proc_ni; }
    void setproc_nj(int proc_nj) { proc_nj_=proc_nj; }

    void setDimPara(void){
        dx_=Lx_/(ny_-1);
        dy_=Ly_/(nx_-1);

        x_.setLinSpaced(ny_,0,Lx_);
        y_.setLinSpaced(nx_,0,Ly_);

        xx_=x_.transpose().replicate(nx_,1);
        yy_=y_.replicate(1,ny_);

        nSize_=nx_*ny_;
    }

    const Dim_Type&     Dim_type(void) const { return Dim_type_; }
    const double&       Lx(void) const { return Lx_; }
    const double&       Ly(void) const { return Ly_; }
    const int&          nx(void) const { return nx_; }
    const int&          ny(void) const { return ny_; }
    const int&          nState(void) const { return nState_; }

    const double&       dx(void) const { return dx_; }
    const double&       dy(void) const { return dy_; }
    const VectorXd&     x(void) const { return x_; }
    const VectorXd&     y(void) const { return y_; }
    const MatrixXd&     xx(void) const { return xx_; }
    const MatrixXd&     yy(void) const { return yy_; }
    const int&          nSize(void) const { return nSize_; }

    const int&          proc_ni(void) const { return proc_ni_; }
    const int&          proc_nj(void) const { return proc_nj_; }

};

class ModelPara
{
public:
    enum DynStep_Type             {dyn,tan,adj};
    enum Flux_Type                {Roe};
    enum BC_Type                  {Reflective,Channel,Dirichlet,Newmon};

private:
    std::unordered_map<std::string, std::string> modelpara_map_;
    DynStep_Type                 DynStep_type_;
    Flux_Type                    Flux_type_;
    BC_Type                      BC_type_;
    double                       g_;

    DimPara                      dimpara_;

    double                       dt_;
    int                          orderT_;
    bool                         LocUncert_flag_;
    int                          LocUncert_combo_;
    bool                         WhiteNoise_flag_;
    bool                         Data_LocUncert_flag_;
    bool                         Viscosity_flag_;
    double                       miu_;
    bool                         Topo_flag_;

    bool                         Coriolis_flag_;
    double                       f_cori_;
    bool                         Bottom_friction_flag_;

public:
    ModelPara() {}
    ~ModelPara() {}

    ModelPara(bool readflag)
    {
        std::string     src_path(std::getenv("ENV_PATH"));
        std::ifstream   modelparafile_config(src_path+"model/ModelParameter_config");
        std::string     line,name,value;

        if(modelparafile_config.is_open())
        {
            while(std::getline(modelparafile_config,line)){
                std::string::size_type nc = line.find("//");
                if(nc != std::string::npos)
                    line.erase(nc);

                std::istringstream is(line);
                if(is >> name >> value)
                    modelpara_map_.insert({name, value});
            }
        }
        else
            std::cerr<<"Error opening file modelparameter_config"<<endl;

        std::ifstream modelparafile_exp(src_path+"model/ModelParameter_exp");

        if(modelparafile_exp.is_open())
        {
            while(std::getline(modelparafile_exp,line)){
                std::string::size_type ne = line.find("//");
                if(ne != std::string::npos)
                    line.erase(ne);

                std::istringstream is(line);
                if(is >> name >> value)
                    modelpara_map_[name]=value;
            }
        }
        else
            std::cerr<<"Error opening file modelparameter_exp"<<endl;

        DynStep_type_=static_cast<DynStep_Type>(std::stoi(modelpara_map_["DynStep_type_"])); 
        BC_type_=static_cast<BC_Type>(std::stoi(modelpara_map_["BC_type_"]));  
        Flux_type_=static_cast<Flux_Type>(std::stoi(modelpara_map_["Flux_type_"])); 
        g_=std::stod(modelpara_map_["g_"]);     

        dimpara_=DimPara(modelpara_map_);

        dt_=std::stod(modelpara_map_["dt_"]); 
        orderT_=std::stoi(modelpara_map_["orderT_"]); 
        LocUncert_flag_=stob(modelpara_map_["LocUncert_flag_"]);  
        LocUncert_combo_=std::stoi(modelpara_map_["LocUncert_combo_"]);  
        WhiteNoise_flag_=stob(modelpara_map_["WhiteNoise_flag_"]); 
        Viscosity_flag_=stob(modelpara_map_["Viscosity_flag_"]);  
        miu_=std::stod(modelpara_map_["miu_"]);            
        Topo_flag_=stob(modelpara_map_["Topo_flag_"]);       
        Coriolis_flag_=stob(modelpara_map_["Coriolis_flag_"]); 
        f_cori_=std::stod(modelpara_map_["f_cori_"]);    
        Bottom_friction_flag_=stob(modelpara_map_["Bottom_friction_flag_"]);

    }

    void setdt(double dt) { dt_=dt; }
    void setLocUncert_combo(int LocUncert_combo) { LocUncert_combo_=LocUncert_combo; }

    void setSynthobs_ModelPara(void) {
        LocUncert_flag_=0;
        Viscosity_flag_=0;
        Bottom_friction_flag_=0;
    }

    const DynStep_Type& DynStep_type(void) const { return DynStep_type_; }
    const Flux_Type&    Flux_type(void) const { return Flux_type_; }
    const BC_Type&      BC_type(void) const { return BC_type_; }
    const double&       g(void) const { return g_; }

          DimPara&      dimpara(void) { return dimpara_; }
 
    const double&       dt(void) const { return dt_; }
    const int&          orderT(void) const { return orderT_; }
    const bool&         LocUncert_flag(void) const { return LocUncert_flag_; }
    const int&          LocUncert_combo(void) const { return LocUncert_combo_; }
    const bool&         WhiteNoise_flag(void) const { return WhiteNoise_flag_; }
    const bool&         Data_LocUncert_flag(void) const { return Data_LocUncert_flag_; }
    const bool&         Viscosity_flag(void) const { return Viscosity_flag_; }
    const double&       miu(void) const { return miu_; }
    const bool&         Topo_flag(void) const { return Topo_flag_; }
    const bool&         Coriolis_flag(void) const { return Coriolis_flag_; }
    const double&       f_cori(void) const { return f_cori_; }
    const bool&         Bottom_friction_flag(void) const { return Bottom_friction_flag_; }

    private:

    bool stob(std::string str){
        std::istringstream is(str);
        bool b;
        is>>std::boolalpha>>b;
        return b;
    }
};

#endif