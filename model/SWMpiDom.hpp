#ifndef SWMPIDOM
#define SWMPIDOM

#include <Eigen/Core>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cfenv>
#include <iterator>
#include <unordered_map>

#if defined(MPI) || defined(MPI_simple)
#include <mpi.h> 
#endif
 
#include "ModelPara.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

class SWMpiDom
{
public:
    // Constructor and destructor.
    SWMpiDom();
    ~SWMpiDom();

private:
    std::unordered_map<std::string, std::string> dimpara_map_;
    int             nx_glo_;
    int             ny_glo_;
    double          Lx_;
    double          Ly_;
    int             nState_;

    int             proc_ni_;
    int             proc_nj_;

    double          dx_;
    double          dy_;
    int             nSize_glo_;

    VectorXd        x_glo_;
    VectorXd        y_glo_;

    MatrixXd        xx_glo_;
    MatrixXd        yy_glo_;

    int             myrank_;
    int             size_;
    int             root_;

    int             coord_x_;
    int             coord_y_;

#if defined(MPI) || defined(MPI_simple)
    MPI_Comm        comm_cart_;
    int             nx_div_;
    int             ny_div_;
    int             nx_div_mod_;
    int             ny_div_mod_;   
#endif

    int             nx_loc_;
    int             ny_loc_;
    int             nSize_loc_;
    int             ix_loc_;
    int             iy_loc_;
    std::vector<int>    ix_loc_vec_;
    std::vector<int>    iy_loc_vec_;
    std::vector<int>    nx_loc_vec_;
    std::vector<int>    ny_loc_vec_;

public:

    //constructors
    SWMpiDom(DimPara&);

    // Methods
    void Init_domain(void);

    //getters
    int& nx_glo(void) { return nx_glo_; }
    int& ny_glo(void) { return ny_glo_; }
    int& nState(void) { return nState_; }
    int& nSize_glo(void) { return nSize_glo_; }
    double& dx(void) { return dx_; }
    double& dy(void) { return dy_; }

    MatrixXd& xx_glo(void) { return xx_glo_; }
    MatrixXd& yy_glo(void) { return yy_glo_; }

    int& myrank(void) { return myrank_; }
    int& mysize(void) { return size_; }
    int& root(void) { return root_; }
    int& coord_x(void) { return coord_x_; }
    int& coord_y(void) { return coord_y_; }
    int& proc_ni(void) { return proc_ni_; }
    int& proc_nj(void) { return proc_nj_; }
#ifdef MPI
    MPI_Comm& comm_cart(void) { return comm_cart_; }
#endif
    int& nx_loc(void) { return nx_loc_; }
    int& ny_loc(void) { return ny_loc_; }
    int& nSize_loc(void) { return nSize_loc_; }

    int& ix_loc(void) { return ix_loc_; }
    int& iy_loc(void) { return iy_loc_; }

    const std::vector<int> & ix_loc_vec(void) const { return ix_loc_vec_; }
    const std::vector<int> & iy_loc_vec(void) const { return iy_loc_vec_; }
    const std::vector<int> & nx_loc_vec(void) const { return nx_loc_vec_; }
    const std::vector<int> & ny_loc_vec(void) const { return ny_loc_vec_; }
};

typedef SWMpiDom MpiDom;

#endif
